# jiax2

Java implementation of IAX2 (Inter-Asterisk eXchange Version 2) communications protocol.

## Overview

**jiax2** currently implements next IAX2 functionality based on [RFC 5456](https://tools.ietf.org/html/rfc5456):
* Call Link Management
* Mid-Call Behavior
* Call Tear Down
* Network Monitoring *(partial)*
* Miscellaneous
* Media Messages
* Trunking

**jiax2** implementation inspired by [YATE](http://www.yate.ro/opensource.php?page=yate) and [Asterisk](http://www.asterisk.org) implementations.

## Structure

**jiax2** uses standard [Maven](https://maven.apache.org) project layout.

## Build

**jiax2** could be compiled, installed and deployed with regular [Maven commands](https://maven.apache.org/guides/introduction/introduction-to-the-lifecycle.html).

## Usage examples

```java
import com.flamesgroup.jiax2.AudioMediaFormat;
import com.flamesgroup.jiax2.AudioMediaFormatOption;
import com.flamesgroup.jiax2.AudioOptions;
import com.flamesgroup.jiax2.CallDropReason;
import com.flamesgroup.jiax2.IIaxPeer;
import com.flamesgroup.jiax2.IMediaStream;
import com.flamesgroup.jiax2.IMediaStreamHandler;
import com.flamesgroup.jiax2.INewTerminatingCallHandler;
import com.flamesgroup.jiax2.IOriginatingCall;
import com.flamesgroup.jiax2.IOriginatingCallHandler;
import com.flamesgroup.jiax2.ITerminatingCall;
import com.flamesgroup.jiax2.ITerminatingCallHandler;
import com.flamesgroup.jiax2.IaxPeer;
import com.flamesgroup.jiax2.helper.AdaptiveDelay;
import com.flamesgroup.media.codec.audio.ElementaryAudioCodecFactory;
import com.flamesgroup.media.codec.audio.IAudioCodecFactory;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.util.Arrays;
import java.util.Collections;

public class Jiax2Example {

  public static void main(String[] args) throws Exception {
    FileOutputStream outputStream = new FileOutputStream("output.pcm");
    FileInputStream inputStream = new FileInputStream("input.pcm");

    SocketAddress bind = new InetSocketAddress("0.0.0.0", 4569);
    AudioOptions audioOptions = new AudioOptions(Arrays.asList(new AudioMediaFormatOption(AudioMediaFormat.ALAW, 2), new AudioMediaFormatOption(AudioMediaFormat.ULAW, 2)));
    IAudioCodecFactory audioCodecFactory = new ElementaryAudioCodecFactory();

    IIaxPeer iaxPeer = new IaxPeer(bind, audioOptions, audioCodecFactory, Collections.emptyMap());

    iaxPeer.startup(new INewTerminatingCallHandler() {
      @Override
      public ITerminatingCallHandler handleNewCall(final ITerminatingCall terminationCall) {
        // TODO implement processing of incoming call by returning correct handler
        return new ITerminatingCallHandler() {
          @Override
          public void handleHangup(final CallDropReason callDropReason) {
          }

          @Override
          public void handleDtmf(final char dtmf) {
          }
        };
      }
    });

    SocketAddress target = new InetSocketAddress("iax2.peer.address", 4569);
    String called = "123";
    String caller = "321";
    String callerName = "jiax2";
    // TODO implement execution of outgoing call by passing correct handler
    IOriginatingCall originatingCall = iaxPeer.newCall(target, called, caller, callerName, new IOriginatingCallHandler() {
      @Override
      public void handleRinging() {
      }

      @Override
      public void handleAnswer() {
      }

      @Override
      public void handleAccept() {
      }

      @Override
      public void handleHangup(final CallDropReason callDropReason) {
      }

      @Override
      public void handleDtmf(final char dtmf) {
      }
    });

    IMediaStream mediaStream = originatingCall.getAudioMediaStream();

    mediaStream.open(new IMediaStreamHandler() {
      @Override
      public void handleMedia(final long timestamp, final byte[] data, final int offset, final int length) {
        try {
          outputStream.write(data, offset, length);
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    });

    Thread thread = new Thread(new Runnable() {
      @Override
      public void run() {
        AdaptiveDelay ad = new AdaptiveDelay();
        byte[] data = new byte[320]; // 20ms of PCM
        while (true) {
          try {
            inputStream.read(data);
          } catch (IOException e) {
            e.printStackTrace();
          }
          mediaStream.write(0, data, 0, data.length);
          try {
            ad.delay(20);
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
        }
      }
    });
    thread.start();
    thread.join();

    mediaStream.close();

    originatingCall.hangup(CallDropReason.NORMAL_CLEARING);

    iaxPeer.shutdown();

    outputStream.close();
    inputStream.close();
  }

}
```

## Questions

If you have any questions about **jiax2**, feel free to create issue with it.
