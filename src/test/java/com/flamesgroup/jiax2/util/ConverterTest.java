/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.flamesgroup.jiax2.util;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ConverterTest {


  @Test public void testByteArrayToHexString() throws Exception {
    final byte[] b = {0x00, 0x11, 0x22, 0x33};

    assertEquals("00112233", Converter.byteArrayToHexString(b));
    assertEquals("1122", Converter.byteArrayToHexString(b, 1, 2));
  }

  @Test public void testByteToHex() throws Exception {
    assertEquals("0x00", Converter.byteToHex((byte) 0x00));
    assertEquals("0x01", Converter.byteToHex((byte) 0x01));
    assertEquals("0xff", Converter.byteToHex((byte) 0xff));
    assertEquals("0x5a", Converter.byteToHex((byte) 0x5a));
  }

  @Test public void testLongToHex() throws Exception {
    assertEquals("0x0000000000000000", Converter.longToHex(0x0000000000000000L));
    assertEquals("0x0000000000000001", Converter.longToHex(0x0000000000000001L));
    assertEquals("0xffffffffffffffff", Converter.longToHex(0xffffffffffffffffL));
    assertEquals("0x1234567890abcdef", Converter.longToHex(0x1234567890abcdefL));
  }

}
