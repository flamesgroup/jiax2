/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.flamesgroup.jiax2.helper;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class CircularBufferTest {

  @Test public void testCircularity() throws Exception {
    CircularBuffer circularBuffer = new CircularBuffer(4);

    assertTrue(circularBuffer.isEmpty());
    assertFalse(circularBuffer.isFull());
    {
      byte[] b = new byte[] {1, 2, 3};
      assertEquals(3, circularBuffer.put(b));
    }
    assertFalse(circularBuffer.isEmpty());
    assertFalse(circularBuffer.isFull());
    {
      byte[] b = new byte[2];
      assertEquals(2, circularBuffer.get(b));
      assertArrayEquals(new byte[] {1, 2}, b);
    }
    assertFalse(circularBuffer.isEmpty());
    assertFalse(circularBuffer.isFull());
    {
      byte[] b = new byte[] {4, 5};
      assertEquals(2, circularBuffer.put(b));
    }
    assertFalse(circularBuffer.isEmpty());
    assertFalse(circularBuffer.isFull());
    {
      byte[] b = new byte[3];
      assertEquals(3, circularBuffer.get(b));
      assertArrayEquals(new byte[] {3, 4, 5}, b);
    }
    assertTrue(circularBuffer.isEmpty());
    assertFalse(circularBuffer.isFull());
    {
      byte[] b = new byte[] {6, 7, 8, 9};
      assertEquals(4, circularBuffer.put(b));
    }
    assertFalse(circularBuffer.isEmpty());
    assertTrue(circularBuffer.isFull());
    {
      byte[] b = new byte[4];
      assertEquals(4, circularBuffer.get(b));
      assertArrayEquals(new byte[] {6, 7, 8, 9}, b);
    }
    assertTrue(circularBuffer.isEmpty());
    assertFalse(circularBuffer.isFull());
  }

  @Test public void testPutAndGetOverflow() throws Exception {
    CircularBuffer circularBuffer = new CircularBuffer(5);

    assertTrue(circularBuffer.isEmpty());
    assertFalse(circularBuffer.isFull());
    {
      byte[] b = new byte[] {1, 2, 3, 4, 5, 6, 7, 8, 9};
      assertEquals(5, circularBuffer.put(b));
    }
    assertFalse(circularBuffer.isEmpty());
    assertTrue(circularBuffer.isFull());
    {
      byte[] b = new byte[10];
      assertEquals(5, circularBuffer.get(b));
      assertArrayEquals(new byte[] {1, 2, 3, 4, 5, 0, 0, 0, 0, 0}, b);
    }
    assertTrue(circularBuffer.isEmpty());
    assertFalse(circularBuffer.isFull());
  }

  @Test public void testFillLackData() throws Exception {
    CircularBuffer circularBuffer = new CircularBuffer(5, true, (byte) 0xFF);

    assertTrue(circularBuffer.isEmpty());
    assertFalse(circularBuffer.isFull());
    {
      byte[] b = new byte[10];
      assertEquals(10, circularBuffer.get(b));
      assertArrayEquals(new byte[] {(byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF}, b);
    }
    assertTrue(circularBuffer.isEmpty());
    assertFalse(circularBuffer.isFull());
    {
      byte[] b = new byte[] {1, 2, 3};
      assertEquals(3, circularBuffer.put(b));
    }
    assertFalse(circularBuffer.isEmpty());
    assertFalse(circularBuffer.isFull());
    {
      byte[] b = new byte[5];
      assertEquals(5, circularBuffer.get(b));
      assertArrayEquals(new byte[] {(byte) 0xFF, (byte) 0xFF, 1, 2, 3}, b);
    }
    assertTrue(circularBuffer.isEmpty());
    assertFalse(circularBuffer.isFull());
  }

}
