/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.flamesgroup.jiax2.frame.mini;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import com.flamesgroup.jiax2.util.DataUtils;
import org.junit.Before;
import org.junit.Test;

import java.nio.ByteBuffer;

public class MiniFrameTest {

  private MiniFrame miniFrame;

  @Before public void setUp() throws Exception {
    miniFrame = new MiniFrame();
  }

  @Test public void testEncodeTo() throws Exception {
    miniFrame.setSourceCallNumber_uint16((short) 7);
    miniFrame.setTimestamp_uint16((short) 1000);
    miniFrame.setData(new byte[] {1, 2, 3, 4, 5});

    ByteBuffer actualByteBuffer = ByteBuffer.allocate(64);
    miniFrame.encodeTo(actualByteBuffer);
    actualByteBuffer.flip();
    byte[] actualByteArray = DataUtils.remaining2array(actualByteBuffer);


    byte[] expectedByteArray = {
        (byte) 0x00, (byte) 0x07, (byte) 0x03, (byte) 0xe8, (byte) 0x01, (byte) 0x02, (byte) 0x03, (byte) 0x04, (byte) 0x05
    };

    assertArrayEquals(expectedByteArray, actualByteArray);
  }

  @Test public void testDecodeFrom() throws Exception {
    byte[] actualByteArray = {
      (byte) 0x59, (byte) 0xc5, (byte) 0x00, (byte) 0x38, (byte) 0x1c, (byte) 0x25, (byte) 0xff, (byte) 0xa4, (byte) 0x9b, (byte) 0x9f, (byte) 0xc0, (byte) 0x29, (byte) 0x1d, (byte) 0x1e,
      (byte) 0x33, (byte) 0xb2, (byte) 0x9d, (byte) 0x9c, (byte) 0xa9, (byte) 0x41, (byte) 0x20, (byte) 0x1c, (byte) 0x25, (byte) 0xff, (byte) 0xa4, (byte) 0x9b, (byte) 0x9f, (byte) 0xc0,
      (byte) 0x29, (byte) 0x1d, (byte) 0x1e, (byte) 0x33, (byte) 0xb2, (byte) 0x9d, (byte) 0x9c, (byte) 0xa9, (byte) 0x41, (byte) 0x20, (byte) 0x1c, (byte) 0x25, (byte) 0xff, (byte) 0xa4,
      (byte) 0x9b, (byte) 0x9f, (byte) 0xc0, (byte) 0x29, (byte) 0x1d, (byte) 0x1e, (byte) 0x33, (byte) 0xb2, (byte) 0x9d, (byte) 0x9c, (byte) 0xa9, (byte) 0x41, (byte) 0x20, (byte) 0x1c,
      (byte) 0x25, (byte) 0xff, (byte) 0xa4, (byte) 0x9b, (byte) 0x9f, (byte) 0xc0, (byte) 0x29, (byte) 0x1d, (byte) 0x1e, (byte) 0x33, (byte) 0xb2, (byte) 0x9d, (byte) 0x9c, (byte) 0xa9,
      (byte) 0x41, (byte) 0x20, (byte) 0x1c, (byte) 0x25, (byte) 0xff, (byte) 0xa4, (byte) 0x9b, (byte) 0x9f, (byte) 0xc0, (byte) 0x29, (byte) 0x1d, (byte) 0x1e, (byte) 0x33, (byte) 0xb2,
      (byte) 0x9d, (byte) 0x9c, (byte) 0xa9, (byte) 0x41, (byte) 0x20, (byte) 0x1c, (byte) 0x25, (byte) 0xff, (byte) 0xa4, (byte) 0x9b, (byte) 0x9f, (byte) 0xc0, (byte) 0x29, (byte) 0x1d,
      (byte) 0x1e, (byte) 0x33, (byte) 0xb2, (byte) 0x9d, (byte) 0x9c, (byte) 0xa9, (byte) 0x41, (byte) 0x20, (byte) 0x1c, (byte) 0x25, (byte) 0xff, (byte) 0xa4, (byte) 0x9b, (byte) 0x9f,
      (byte) 0xc0, (byte) 0x29, (byte) 0x1d, (byte) 0x1e, (byte) 0x33, (byte) 0xb2, (byte) 0x9d, (byte) 0x9c, (byte) 0xa9, (byte) 0x41, (byte) 0x20, (byte) 0x1c, (byte) 0x25, (byte) 0xff,
      (byte) 0xa4, (byte) 0x9b, (byte) 0x9f, (byte) 0xc0, (byte) 0x29, (byte) 0x1d, (byte) 0x1e, (byte) 0x33, (byte) 0xb2, (byte) 0x9d, (byte) 0x9c, (byte) 0xa9, (byte) 0x41, (byte) 0x20,
      (byte) 0x1c, (byte) 0x25, (byte) 0xff, (byte) 0xa4, (byte) 0x9b, (byte) 0x9f, (byte) 0xc0, (byte) 0x29, (byte) 0x1d, (byte) 0x1e, (byte) 0x33, (byte) 0xb2, (byte) 0x9d, (byte) 0x9c,
      (byte) 0xa9, (byte) 0x41, (byte) 0x20, (byte) 0x1c, (byte) 0x25, (byte) 0xff, (byte) 0xa4, (byte) 0x9b, (byte) 0x9f, (byte) 0xc0
    };
    ByteBuffer actualByteBuffer = ByteBuffer.wrap(actualByteArray);
    miniFrame.decodeFrom(actualByteBuffer);

    assertThat(actualByteBuffer.remaining(), is(0));

    assertEquals(22981, miniFrame.getSourceCallNumber_uint16());
    assertEquals(56, miniFrame.getTimestamp_uint16());
    assertThat(miniFrame.getData().length, is(160));
  }

}
