/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.flamesgroup.jiax2.frame.full;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;

import java.nio.ByteBuffer;

public class VoiceFullFrameTest {

  private FullFrame voiceFullFrame;

  @Before public void setUp() throws Exception {
    voiceFullFrame = new FullFrame();
  }

  @Test public void testDecodeFrom() throws Exception {
    byte[] actualByteArray = {
      (byte) 0xfb, (byte) 0x8c, (byte) 0x00, (byte) 0x02, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x1e, (byte) 0x02, (byte) 0x01, (byte) 0x02, (byte) 0x08, (byte) 0x1e, (byte) 0x99, (byte) 0xb4, (byte) 0xb6,
      (byte) 0x83, (byte) 0x69, (byte) 0x0b, (byte) 0x37, (byte) 0x0c, (byte) 0xd5, (byte) 0x8f, (byte) 0xb6, (byte) 0x8a, (byte) 0xe8, (byte) 0x00, (byte) 0x37, (byte) 0x35, (byte) 0x1e, (byte) 0x99, (byte) 0xb4,
      (byte) 0xb6, (byte) 0x83, (byte) 0x69, (byte) 0x0b, (byte) 0x37, (byte) 0x0c, (byte) 0xd5, (byte) 0x8f, (byte) 0xb6, (byte) 0x8a, (byte) 0xe8, (byte) 0x00, (byte) 0x37, (byte) 0x35, (byte) 0x1e, (byte) 0x99,
      (byte) 0xb4, (byte) 0xb6, (byte) 0x83, (byte) 0x69, (byte) 0x0b, (byte) 0x37, (byte) 0x0c, (byte) 0xd5, (byte) 0x8f, (byte) 0xb6, (byte) 0x8a, (byte) 0xe8, (byte) 0x00, (byte) 0x37, (byte) 0x35, (byte) 0x1e,
      (byte) 0x99, (byte) 0xb4, (byte) 0xb6, (byte) 0x83, (byte) 0x69, (byte) 0x0b, (byte) 0x37, (byte) 0x0c, (byte) 0xd5, (byte) 0x8f, (byte) 0xb6, (byte) 0x8a, (byte) 0xe8, (byte) 0x00, (byte) 0x37, (byte) 0x35,
      (byte) 0x1e, (byte) 0x99, (byte) 0xb4, (byte) 0xb6, (byte) 0x83, (byte) 0x69, (byte) 0x0b, (byte) 0x37, (byte) 0x0c, (byte) 0xd5, (byte) 0x8f, (byte) 0xb6, (byte) 0x8a, (byte) 0xe8, (byte) 0x00, (byte) 0x37,
      (byte) 0x35, (byte) 0x1e, (byte) 0x99, (byte) 0xb4, (byte) 0xb6, (byte) 0x83, (byte) 0x69, (byte) 0x0b, (byte) 0x37, (byte) 0x0c, (byte) 0xd5, (byte) 0x8f, (byte) 0xb6, (byte) 0x8a, (byte) 0xe8, (byte) 0x00,
      (byte) 0x37, (byte) 0x35, (byte) 0x1e, (byte) 0x99, (byte) 0xb4, (byte) 0xb6, (byte) 0x83, (byte) 0x69, (byte) 0x0b, (byte) 0x37, (byte) 0x0c, (byte) 0xd5, (byte) 0x8f, (byte) 0xb6, (byte) 0x8a, (byte) 0xe8,
      (byte) 0x00, (byte) 0x37, (byte) 0x35, (byte) 0x1e, (byte) 0x99, (byte) 0xb4, (byte) 0xb6, (byte) 0x83, (byte) 0x69, (byte) 0x0b, (byte) 0x37, (byte) 0x0c, (byte) 0xd5, (byte) 0x8f, (byte) 0xb6, (byte) 0x8a,
      (byte) 0xe8, (byte) 0x00, (byte) 0x37, (byte) 0x35, (byte) 0x1e, (byte) 0x99, (byte) 0xb4, (byte) 0xb6, (byte) 0x83, (byte) 0x69, (byte) 0x0b, (byte) 0x37, (byte) 0x0c, (byte) 0xd5, (byte) 0x8f, (byte) 0xb6,
      (byte) 0x8a, (byte) 0xe8, (byte) 0x00, (byte) 0x37, (byte) 0x35, (byte) 0x1e, (byte) 0x99, (byte) 0xb4, (byte) 0xb6, (byte) 0x83, (byte) 0x69, (byte) 0x0b
    };
    ByteBuffer actualByteBuffer = ByteBuffer.wrap(actualByteArray);
    voiceFullFrame.decodeFrom(actualByteBuffer);

    assertThat(actualByteBuffer.remaining(), is(0));

    assertEquals(31628, voiceFullFrame.getSourceCallNumber_uint16());
    assertEquals(2, voiceFullFrame.getDestinationCallNumber_uint16());
    assertEquals(false, voiceFullFrame.isRetransmitted());
    assertEquals(30, voiceFullFrame.getTimestamp_uint32());
    assertEquals(2, voiceFullFrame.getOSeqno_uint8());
    assertEquals(1, voiceFullFrame.getISeqno_uint8());
    assertEquals(FullFrameType.VOICE, voiceFullFrame.getFrameType_uint8());
    assertEquals(MediaFormat.ALAW, FullFrameUtils.uncompressSubclass_uint64(voiceFullFrame.getSubclass_uint8()));
    assertEquals(160, voiceFullFrame.getData().length);
  }

}
