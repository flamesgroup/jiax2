/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.flamesgroup.jiax2.frame.full;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;

import java.nio.ByteBuffer;

public class ControlFullFrameTest {

  private FullFrame controlFullFrame;

  @Before public void setUp() throws Exception {
    controlFullFrame = new FullFrame();
  }

  @Test public void testDecodeFrom() throws Exception {
    byte[] actualByteArray = {
      (byte) 0xfb, (byte) 0x8c, (byte) 0x00, (byte) 0x02, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x0a, (byte) 0x01, (byte) 0x01, (byte) 0x04, (byte) 0x04
    };
    ByteBuffer actualByteBuffer = ByteBuffer.wrap(actualByteArray);
    controlFullFrame.decodeFrom(actualByteBuffer);

    assertThat(actualByteBuffer.remaining(), is(0));

    assertEquals(31628, controlFullFrame.getSourceCallNumber_uint16());
    assertEquals(2, controlFullFrame.getDestinationCallNumber_uint16());
    assertEquals(false, controlFullFrame.isRetransmitted());
    assertEquals(10, controlFullFrame.getTimestamp_uint32());
    assertEquals(1, controlFullFrame.getOSeqno_uint8());
    assertEquals(1, controlFullFrame.getISeqno_uint8());
    assertEquals(FullFrameType.CONTROL, controlFullFrame.getFrameType_uint8());
    assertEquals(ControlFullFrameSubclass.ANSWER, controlFullFrame.getSubclass_uint8());
    assertEquals(0, controlFullFrame.getData().length);
  }

}
