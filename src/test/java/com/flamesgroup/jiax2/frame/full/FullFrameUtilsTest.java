/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.flamesgroup.jiax2.frame.full;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class FullFrameUtilsTest {

  @Test public void testCompressSubclass_uint8() throws Exception {
    assertEquals(0L, FullFrameUtils.uncompressSubclass_uint64((byte) 0));
    assertEquals(1L, FullFrameUtils.uncompressSubclass_uint64((byte) 1));
    assertEquals(127L, FullFrameUtils.uncompressSubclass_uint64((byte) 127));
    assertEquals(0x00000000_00000001L, FullFrameUtils.uncompressSubclass_uint64((byte) 128));
    assertEquals(0x80000000_00000000L, FullFrameUtils.uncompressSubclass_uint64((byte) (0x80 | 63)));
    assertEquals(0x00000000_00000400L, FullFrameUtils.uncompressSubclass_uint64((byte) 138));

    assertEquals(-1L, FullFrameUtils.uncompressSubclass_uint64((byte) 0xFF));

    assertEquals(0, FullFrameUtils.uncompressSubclass_uint64((byte) (0x80 | 64)));
  }

  @Test public void testUncompressSubclass_uint64() throws Exception {
    assertEquals((byte) 0, FullFrameUtils.compressSubclass_uint8(0L));
    assertEquals((byte) 1, FullFrameUtils.compressSubclass_uint8(1L));
    assertEquals((byte) 127, FullFrameUtils.compressSubclass_uint8(127L));
    assertEquals((byte) 135, FullFrameUtils.compressSubclass_uint8(128L));

    assertEquals((byte) 0, FullFrameUtils.compressSubclass_uint8(0x10000000_00000100L));
  }

}
