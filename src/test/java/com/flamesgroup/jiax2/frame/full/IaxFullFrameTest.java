/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.flamesgroup.jiax2.frame.full;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;

import java.nio.ByteBuffer;

public class IaxFullFrameTest {

  private FullFrame iaxFullFrame;

  @Before public void setUp() throws Exception {
    iaxFullFrame = new FullFrame();
  }

  @Test public void testDecodeFrom1() throws Exception {
    byte[] actualByteArray = {
      (byte) 0x80, (byte) 0x02, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x02, (byte) 0x00, (byte) 0x00, (byte) 0x06, (byte) 0x01, (byte) 0x0b,
      (byte) 0x02, (byte) 0x00, (byte) 0x02, (byte) 0x02, (byte) 0x00, (byte) 0x04, (byte) 0x06, (byte) 0x6f, (byte) 0x73, (byte) 0x78, (byte) 0x69, (byte) 0x61, (byte) 0x78, (byte) 0x26,
      (byte) 0x01, (byte) 0x01, (byte) 0x27, (byte) 0x01, (byte) 0x00, (byte) 0x28, (byte) 0x02, (byte) 0x00, (byte) 0x00, (byte) 0x09, (byte) 0x04, (byte) 0x00, (byte) 0x00, (byte) 0x00,
      (byte) 0x08, (byte) 0x08, (byte) 0x04, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x0c, (byte) 0x06, (byte) 0x07, (byte) 0x69, (byte) 0x61, (byte) 0x78, (byte) 0x74, (byte) 0x65,
      (byte) 0x73, (byte) 0x74, (byte) 0x01, (byte) 0x08, (byte) 0x39, (byte) 0x39, (byte) 0x39, (byte) 0x39, (byte) 0x31, (byte) 0x30, (byte) 0x30, (byte) 0x38, (byte) 0x0d, (byte) 0x08,
      (byte) 0x39, (byte) 0x39, (byte) 0x39, (byte) 0x39, (byte) 0x31, (byte) 0x30, (byte) 0x30, (byte) 0x38, (byte) 0x0c, (byte) 0x02, (byte) 0x00, (byte) 0x00, (byte) 0x36, (byte) 0x00,
      (byte) 0x24, (byte) 0x12, (byte) 0x5a, (byte) 0x20, (byte) 0x33, (byte) 0x2e, (byte) 0x33, (byte) 0x2e, (byte) 0x32, (byte) 0x31, (byte) 0x39, (byte) 0x33, (byte) 0x33, (byte) 0x20,
      (byte) 0x72, (byte) 0x32, (byte) 0x31, (byte) 0x39, (byte) 0x30, (byte) 0x33
    };
    ByteBuffer actualByteBuffer = ByteBuffer.wrap(actualByteArray);
    iaxFullFrame.decodeFrom(actualByteBuffer);

    assertThat(actualByteBuffer.remaining(), is(0));

    assertEquals(2, iaxFullFrame.getSourceCallNumber_uint16());
    assertEquals(0, iaxFullFrame.getDestinationCallNumber_uint16());
    assertEquals(false, iaxFullFrame.isRetransmitted());
    assertEquals(2, iaxFullFrame.getTimestamp_uint32());
    assertEquals(0, iaxFullFrame.getOSeqno_uint8());
    assertEquals(0, iaxFullFrame.getISeqno_uint8());
    assertEquals(FullFrameType.IAX, iaxFullFrame.getFrameType_uint8());
    assertEquals(IaxFullFrameSubclass.NEW, iaxFullFrame.getSubclass_uint8());
    assertEquals(91, iaxFullFrame.getData().length);
  }

  @Test public void testDecodeFrom2() throws Exception {
    byte[] actualByteArray = {
      (byte) 0xfb, (byte) 0x8c, (byte) 0x00, (byte) 0x02, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x02, (byte) 0x00, (byte) 0x01, (byte) 0x06, (byte) 0x04
    };
    ByteBuffer actualByteBuffer = ByteBuffer.wrap(actualByteArray);
    iaxFullFrame.decodeFrom(actualByteBuffer);

    assertThat(actualByteBuffer.remaining(), is(0));

    assertEquals(31628, iaxFullFrame.getSourceCallNumber_uint16());
    assertEquals(2, iaxFullFrame.getDestinationCallNumber_uint16());
    assertEquals(false, iaxFullFrame.isRetransmitted());
    assertEquals(2, iaxFullFrame.getTimestamp_uint32());
    assertEquals(0, iaxFullFrame.getOSeqno_uint8());
    assertEquals(1, iaxFullFrame.getISeqno_uint8());
    assertEquals(FullFrameType.IAX, iaxFullFrame.getFrameType_uint8());
    assertEquals(IaxFullFrameSubclass.ACK, iaxFullFrame.getSubclass_uint8());
    assertEquals(0, iaxFullFrame.getData().length);
  }

  @Test public void testDecodeFrom3() throws Exception {
    byte[] actualByteArray = {
      (byte) 0xfb, (byte) 0x8c, (byte) 0x00, (byte) 0x02, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x09, (byte) 0x00, (byte) 0x01, (byte) 0x06, (byte) 0x07, (byte) 0x09, (byte) 0x04, (byte) 0x00, (byte) 0x00,
      (byte) 0x00, (byte) 0x08, (byte) 0x08, (byte) 0x04, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x0c
    };
    ByteBuffer actualByteBuffer = ByteBuffer.wrap(actualByteArray);
    iaxFullFrame.decodeFrom(actualByteBuffer);

    assertThat(actualByteBuffer.remaining(), is(0));

    assertEquals(31628, iaxFullFrame.getSourceCallNumber_uint16());
    assertEquals(2, iaxFullFrame.getDestinationCallNumber_uint16());
    assertEquals(false, iaxFullFrame.isRetransmitted());
    assertEquals(9, iaxFullFrame.getTimestamp_uint32());
    assertEquals(0, iaxFullFrame.getOSeqno_uint8());
    assertEquals(1, iaxFullFrame.getISeqno_uint8());
    assertEquals(FullFrameType.IAX, iaxFullFrame.getFrameType_uint8());
    assertEquals(IaxFullFrameSubclass.ACCEPT, iaxFullFrame.getSubclass_uint8());
    assertEquals(12, iaxFullFrame.getData().length);
  }

}
