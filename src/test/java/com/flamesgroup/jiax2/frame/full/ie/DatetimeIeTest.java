/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.flamesgroup.jiax2.frame.full.ie;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import java.time.LocalDateTime;

public class DatetimeIeTest {

  private DatetimeIe datetimeIe;

  @Before public void setUp() throws Exception {
    datetimeIe = new DatetimeIe();
  }

  @Test public void testEncode() throws Exception {
    LocalDateTime datetimeV = LocalDateTime.of(2015, 8, 25, 15, 38, 44);

    datetimeIe.setV(datetimeV);
    byte[] actualByteArray = datetimeIe.encode();

    byte[] expectedByteArray = {(byte) 0x1f, (byte) 0x19, (byte) 0x7c, (byte) 0xd6};

    assertArrayEquals(expectedByteArray, actualByteArray);
  }

  @Test public void testDecode1() throws Exception {
    byte[] actualByteArray = {
        (byte) 0x1f, (byte) 0x1a, (byte) 0x66, (byte) 0x53
    };

    datetimeIe.decode(actualByteArray);
    LocalDateTime datetimeV = datetimeIe.getV();

    assertEquals(2015, datetimeV.getYear());
    assertEquals(8, datetimeV.getMonthValue());
    assertEquals(26, datetimeV.getDayOfMonth());
    assertEquals(12, datetimeV.getHour());
    assertEquals(50, datetimeV.getMinute());
    assertEquals(38, datetimeV.getSecond());
  }

}
