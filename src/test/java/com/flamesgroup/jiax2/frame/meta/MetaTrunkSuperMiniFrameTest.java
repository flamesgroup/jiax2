/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.flamesgroup.jiax2.frame.meta;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertThat;

import com.flamesgroup.jiax2.util.DataUtils;
import org.junit.Before;
import org.junit.Test;

import java.nio.ByteBuffer;

public class MetaTrunkSuperMiniFrameTest {

  private MetaTrunkSuperMiniFrame metaTrunkSuperMiniFrame;

  @Before public void setUp() throws Exception {
    metaTrunkSuperMiniFrame = new MetaTrunkSuperMiniFrame();
  }

  @Test public void testEncodeTo() throws Exception {
    metaTrunkSuperMiniFrame.setTimestamp_uint32(100);
    {
      MetaTrunkSuperMiniFrame.MediaItem mediaItem = new MetaTrunkSuperMiniFrame.MediaItem();
      mediaItem.setSourceCallNumber_uint16((short) 10);
      mediaItem.setData(new byte[] {(byte) 0x55});
      metaTrunkSuperMiniFrame.getMediaItems().add(mediaItem);
    }
    {
      MetaTrunkSuperMiniFrame.MediaItem mediaItem = new MetaTrunkSuperMiniFrame.MediaItem();
      mediaItem.setSourceCallNumber_uint16((short) 30);
      mediaItem.setData(new byte[] {(byte) 0x03, (byte) 0x04, (byte) 0x05});
      metaTrunkSuperMiniFrame.getMediaItems().add(mediaItem);
    }

    ByteBuffer actualByteBuffer = ByteBuffer.allocate(64);
    metaTrunkSuperMiniFrame.encodeTo(actualByteBuffer);
    actualByteBuffer.flip();
    byte[] actualByteArray = DataUtils.remaining2array(actualByteBuffer);

    byte[] expectedByteArray = {
      (byte) 0x00, (byte) 0x00, (byte) 0x01, (byte) 0x00,
      (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x64,
      (byte) 0x00, (byte) 0x0A, (byte) 0x00, (byte) 0x01,
      (byte) 0x55, (byte) 0x00, (byte) 0x1E, (byte) 0x00,
      (byte) 0x03, (byte) 0x03, (byte) 0x04, (byte) 0x05,
    };

    assertArrayEquals(expectedByteArray, actualByteArray);
  }

  @Test public void testDecodeFrom() throws Exception {
    byte[] actualByteArray = {
      (byte) 0x00, (byte) 0x00, (byte) 0x01, (byte) 0x00,
      (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x0F,
      (byte) 0x00, (byte) 0x01, (byte) 0x00, (byte) 0x02,
      (byte) 0x01, (byte) 0x02, (byte) 0x01, (byte) 0x00,
      (byte) 0x00, (byte) 0x05, (byte) 0x0A, (byte) 0x0B,
      (byte) 0x0C, (byte) 0x0D, (byte) 0x0E,
    };
    ByteBuffer actualByteBuffer = ByteBuffer.wrap(actualByteArray);
    metaTrunkSuperMiniFrame.decodeFrom(actualByteBuffer);

    assertThat(actualByteBuffer.remaining(), is(0));

    assertThat(metaTrunkSuperMiniFrame.getTimestamp_uint32(), is(15));
    assertThat(metaTrunkSuperMiniFrame.getMediaItems().size(), is(2));
    assertThat(metaTrunkSuperMiniFrame.getLength(), is(23));

    {
      MetaTrunkSuperMiniFrame.MediaItem mediaItem = metaTrunkSuperMiniFrame.getMediaItems().get(0);
      assertThat(mediaItem.getSourceCallNumber_uint16(), is((short) 1));
      assertArrayEquals(new byte[] {(byte) 0x01, (byte) 0x02}, mediaItem.getData());
      assertThat(mediaItem.getLength(), is(6));
    }
    {
      MetaTrunkSuperMiniFrame.MediaItem mediaItem = metaTrunkSuperMiniFrame.getMediaItems().get(1);
      assertThat(mediaItem.getSourceCallNumber_uint16(), is((short) 256));
      assertArrayEquals(new byte[] {(byte) 0x0A, (byte) 0x0B, (byte) 0x0C, (byte) 0x0D, (byte) 0x0E}, mediaItem.getData());
      assertThat(mediaItem.getLength(), is(9));
    }
  }

}
