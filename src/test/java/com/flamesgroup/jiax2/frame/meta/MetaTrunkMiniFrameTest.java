/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.flamesgroup.jiax2.frame.meta;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertThat;

import com.flamesgroup.jiax2.util.DataUtils;
import org.junit.Before;
import org.junit.Test;

import java.nio.ByteBuffer;

public class MetaTrunkMiniFrameTest {

  private MetaTrunkMiniFrame metaTrunkMiniFrame;

  @Before public void setUp() throws Exception {
    metaTrunkMiniFrame = new MetaTrunkMiniFrame();
  }

  @Test public void testEncodeTo() throws Exception {
    metaTrunkMiniFrame.setTimestamp_uint32(1000);
    {
      MetaTrunkMiniFrame.MediaItem mediaItem = new MetaTrunkMiniFrame.MediaItem();
      mediaItem.setSourceCallNumber_uint16((short) 1);
      mediaItem.setTimestamp_uint16((short) 2);
      mediaItem.setData(new byte[] {(byte) 0x01, (byte) 0x02});
      metaTrunkMiniFrame.getMediaItems().add(mediaItem);
    }
    {
      MetaTrunkMiniFrame.MediaItem mediaItem = new MetaTrunkMiniFrame.MediaItem();
      mediaItem.setSourceCallNumber_uint16((short) 3);
      mediaItem.setTimestamp_uint16((short) 4);
      mediaItem.setData(new byte[] {(byte) 0x03, (byte) 0x04, (byte) 0x05});
      metaTrunkMiniFrame.getMediaItems().add(mediaItem);
    }
    {
      MetaTrunkMiniFrame.MediaItem mediaItem = new MetaTrunkMiniFrame.MediaItem();
      mediaItem.setSourceCallNumber_uint16((short) 5);
      mediaItem.setTimestamp_uint16((short) 6);
      mediaItem.setData(new byte[] {(byte) 0x06, (byte) 0x07, (byte) 0x08, (byte) 0x09});
      metaTrunkMiniFrame.getMediaItems().add(mediaItem);
    }

    ByteBuffer actualByteBuffer = ByteBuffer.allocate(64);
    metaTrunkMiniFrame.encodeTo(actualByteBuffer);
    actualByteBuffer.flip();
    byte[] actualByteArray = DataUtils.remaining2array(actualByteBuffer);

    byte[] expectedByteArray = {
      (byte) 0x00, (byte) 0x00, (byte) 0x01, (byte) 0x01,
      (byte) 0x00, (byte) 0x00, (byte) 0x03, (byte) 0xE8,
      (byte) 0x00, (byte) 0x02, (byte) 0x00, (byte) 0x01,
      (byte) 0x00, (byte) 0x02, (byte) 0x01, (byte) 0x02,
      (byte) 0x00, (byte) 0x03, (byte) 0x00, (byte) 0x03,
      (byte) 0x00, (byte) 0x04, (byte) 0x03, (byte) 0x04,
      (byte) 0x05, (byte) 0x00, (byte) 0x04, (byte) 0x00,
      (byte) 0x05, (byte) 0x00, (byte) 0x06, (byte) 0x06,
      (byte) 0x07, (byte) 0x08, (byte) 0x09,
    };

    assertArrayEquals(expectedByteArray, actualByteArray);
  }

  @Test public void testDecodeFrom() throws Exception {
    byte[] actualByteArray = {
      (byte) 0x00, (byte) 0x00, (byte) 0x01, (byte) 0x01,
      (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00,
      (byte) 0x00, (byte) 0x02, (byte) 0x00, (byte) 0x01,
      (byte) 0x00, (byte) 0x00, (byte) 0x01, (byte) 0x02,
      (byte) 0x00, (byte) 0x05, (byte) 0x01, (byte) 0x00,
      (byte) 0x80, (byte) 0x01, (byte) 0x0A, (byte) 0x0B,
      (byte) 0x0C, (byte) 0x0D, (byte) 0x0E,
    };
    ByteBuffer actualByteBuffer = ByteBuffer.wrap(actualByteArray);
    metaTrunkMiniFrame.decodeFrom(actualByteBuffer);

    assertThat(actualByteBuffer.remaining(), is(0));

    assertThat(metaTrunkMiniFrame.getTimestamp_uint32(), is(0));
    assertThat(metaTrunkMiniFrame.getMediaItems().size(), is(2));
    assertThat(metaTrunkMiniFrame.getLength(), is(27));

    {
      MetaTrunkMiniFrame.MediaItem mediaItem = metaTrunkMiniFrame.getMediaItems().get(0);
      assertThat(mediaItem.getSourceCallNumber_uint16(), is((short) 1));
      assertThat(mediaItem.getTimestamp_uint16(), is((short) 0));
      assertArrayEquals(new byte[] {(byte) 0x01, (byte) 0x02}, mediaItem.getData());
      assertThat(mediaItem.getLength(), is(8));
    }
    {
      MetaTrunkMiniFrame.MediaItem mediaItem = metaTrunkMiniFrame.getMediaItems().get(1);
      assertThat(mediaItem.getSourceCallNumber_uint16(), is((short) 256));
      assertThat(mediaItem.getTimestamp_uint16(), is((short) 32769));
      assertArrayEquals(new byte[] {(byte) 0x0A, (byte) 0x0B, (byte) 0x0C, (byte) 0x0D, (byte) 0x0E}, mediaItem.getData());
      assertThat(mediaItem.getLength(), is(11));
    }
  }

}
