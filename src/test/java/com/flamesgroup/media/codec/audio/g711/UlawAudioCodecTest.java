/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.flamesgroup.media.codec.audio.g711;

import static org.junit.Assert.assertArrayEquals;

import com.flamesgroup.media.codec.audio.AudioCodecException;
import org.junit.Test;

public class UlawAudioCodecTest {

  private static final byte[] PCM = {0x22, 0x11, 0x33, 0x44};
  private static final byte[] ULAW = {0xffffff84, 0xffffffa4};
  private final UlawAudioCodec ulawAudioCodec = new UlawAudioCodec();

  @Test public void encode() throws AudioCodecException {
    byte[] expectedUlaw = {0xffffffae, 0xffffff8e};
    byte[] actualUlaw = ulawAudioCodec.encode(PCM);
    assertArrayEquals(expectedUlaw, actualUlaw);
  }

  @Test public void decode() {
    byte[] expectedPcm = {0x7c, 0x6d, 0xfffffffc, 0x1a};
    byte[] actualPcm = ulawAudioCodec.decode(ULAW);
    assertArrayEquals(expectedPcm, actualPcm);
  }

  @Test(expected = AudioCodecException.class) public void encodeException() throws AudioCodecException {
    byte[] badPcm = {0xffffff84, 0xffffffa4, 0xffffffa4};
    ulawAudioCodec.encode(badPcm);
  }

}
