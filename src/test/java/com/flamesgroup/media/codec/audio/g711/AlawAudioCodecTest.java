/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.flamesgroup.media.codec.audio.g711;

import static org.junit.Assert.assertArrayEquals;

import com.flamesgroup.media.codec.audio.AudioCodecException;
import org.junit.Test;

public class AlawAudioCodecTest {

  private static final byte[] PCM = {0x22, 0x11, 0x33, 0x44};
  private static final byte[] ALAW = {0xffffff84, 0xffffffa4};
  private final AlawAudioCodec alawAudioCodec = new AlawAudioCodec();

  @Test public void encode() throws AudioCodecException {
    byte[] expectedAlaw = {0xffffff84, 0xffffffa4};
    byte[] actualAlaw = alawAudioCodec.encode(PCM);
    assertArrayEquals(expectedAlaw, actualAlaw);
  }

  @Test public void decode() {
    byte[] expectedPcm = {0xffffff80, 0x11, 0x0, 0x46};
    byte[] actualPcm = alawAudioCodec.decode(ALAW);
    assertArrayEquals(expectedPcm, actualPcm);
  }

  @Test(expected = AudioCodecException.class) public void encodeException() throws AudioCodecException {
    byte[] badPcm = {0xffffff84, 0xffffffa4, 0xffffffa4};
    alawAudioCodec.encode(badPcm);
  }

}
