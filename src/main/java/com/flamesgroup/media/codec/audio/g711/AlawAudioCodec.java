/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.flamesgroup.media.codec.audio.g711;

public class AlawAudioCodec extends G711AudioCodec {

  @Override
  protected int g711ToPcm(final int val) {
    return alawToPcm(val);
  }

  @Override
  protected int pcmToG711(final int pcm) {
    return pcmToAlaw(pcm);
  }

  /**
   * Converts a 16-bit linear PCM value to 8-bit A-law.
   * <p>
   * It accepts an 16-bit integer and encodes it as A-law data.
   * <p>
   * Linear Input Code    Compressed Code
   * -----------------    ---------------
   * 0000000wxyza         000wxyz
   * 0000001wxyza         001wxyz
   * 000001wxyzab         010wxyz
   * 00001wxyzabc         011wxyz
   * 0001wxyzabcd         100wxyz
   * 001wxyzabcde         101wxyz
   * 01wxyzabcdef         110wxyz
   * 1wxyzabcdefg         111wxyz
   * <p>
   * For further information see John C. Bellamy's Digital Telephony, 1982,
   * John Wiley & Sons, pps 98-111 and 472-476.
   */
  private int pcmToAlaw(int pcm) // 2's complement (16-bit range)
  {
    int mask;
    int seg;
    //unsigned char aval;
    int aval;

    if (pcm >= 0) {
      mask = 0xD5; // sign (7th) bit = 1
    } else {
      mask = 0x55; // sign bit = 0
      pcm = -pcm - 8;
    }

    // Convert the scaled magnitude to segment number.
    seg = search(pcm, segEnd);

    // Combine the sign, segment, and quantization bits.
    if (seg >= 8) // out of range, return maximum value.
      return (0x7F ^ mask);
    else {
      aval = seg << SEG_SHIFT;
      if (seg < 2)
        aval |= (pcm >> 4) & QUANT_MASK;
      else
        aval |= (pcm >> (seg + 3)) & QUANT_MASK;
      return (aval ^ mask);
    }
  }

  /**
   * Converts an A-law value to 16-bit linear PCM
   */
  private int alawToPcm(int val) {
    int t;
    int seg;
    val ^= 0x55;
    t = (val & QUANT_MASK) << 4;
    //seg=((unsigned)a_val&SEG_MASK)>>SEG_SHIFT;
    seg = (val & SEG_MASK) >> SEG_SHIFT;
    switch (seg) {
      case 0:
        t += 8;
        break;
      case 1:
        t += 0x108;
        break;
      default:
        t += 0x108;
        t <<= seg - 1;
    }
    return ((val & SIGN_BIT) != 0) ? t : -t;
  }

}
