/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.flamesgroup.media.codec.audio;

public final class AudioCodecUtils {

  private AudioCodecUtils() {
  }

  public static int getPcmSample(final byte[] data, final int offset) {
    return (data[offset] & 0xFF) | (data[offset + 1] << 8);
  }

  public static void putPcmSample(final int pcm, final byte[] data, final int offset) {
    data[offset] = (byte) pcm;
    data[offset + 1] = (byte) (pcm >> 8);
  }

  public static int durationToPcmSize(final int duration) {
    return 16 * duration;
  }

}
