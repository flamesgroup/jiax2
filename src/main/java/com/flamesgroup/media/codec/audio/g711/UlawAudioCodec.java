/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.flamesgroup.media.codec.audio.g711;

public class UlawAudioCodec extends G711AudioCodec {

  @Override
  protected int g711ToPcm(final int val) {
    return ulawToPcm(val);
  }

  @Override
  protected int pcmToG711(final int pcm) {
    return pcmToUlaw(pcm);
  }

  /**
   * Converts a linear PCM value to u-law
   * <p>
   * In order to simplify the encoding process, the original linear magnitude
   * is biased by adding 33 which shifts the encoding range from (0 - 8158) to
   * (33 - 8191). The result can be seen in the following encoding table:
   * <p>
   * Biased Linear Input Code   Compressed Code
   * ------------------------   ---------------
   * 00000001wxyza              000wxyz
   * 0000001wxyzab              001wxyz
   * 000001wxyzabc              010wxyz
   * 00001wxyzabcd              011wxyz
   * 0001wxyzabcde              100wxyz
   * 001wxyzabcdef              101wxyz
   * 01wxyzabcdefg              110wxyz
   * 1wxyzabcdefgh              111wxyz
   * <p>
   * Each biased linear code has a leading 1 which identifies the segment
   * number. The value of the segment number is equal to 7 minus the number
   * of leading 0's. The quantization interval is directly available as the
   * four bits wxyz.  The trailing bits (a - h) are ignored.
   * <p>
   * Ordinarily the complement of the resulting code word is used for
   * transmission, and so the code word is complemented before it is returned.
   * <p>
   * For further information see John C. Bellamy's Digital Telephony, 1982,
   * John Wiley & Sons, pps 98-111 and 472-476.
   */
  private int pcmToUlaw(int pcm)  // 2's complement (16-bit range)
  {
    int mask;
    int seg;
    //unsigned char uval;
    int uval;

    // Get the sign and the magnitude of the value.
    if (pcm < 0) {
      pcm = BIAS - pcm;
      mask = 0x7F;
    } else {
      pcm += BIAS;
      mask = 0xFF;
    }
    // Convert the scaled magnitude to segment number.
    seg = search(pcm, segEnd);

    // Combine the sign, segment, quantization bits; and complement the code word.

    if (seg >= 8)
      return (0x7F ^ mask); // out of range, return maximum value.
    else {
      uval = (seg << 4) | ((pcm >> (seg + 3)) & 0xF);
      return (uval ^ mask);
    }
  }

  /**
   * ConvertS a u-law value to 16-bit linear PCM.
   * <p>
   * First, a biased linear code is derived from the code word. An unbiased
   * output can then be obtained by subtracting 33 from the biased code.
   * <p>
   * Note that this function expects to be passed the complement of the
   * original code word. This is in keeping with ISDN conventions.
   */
  private int ulawToPcm(int u_val) {
    int t;
    // Complement to obtain normal u-law value.
    u_val = ~u_val;
    // Extract and bias the quantization bits. Then shift up by the segment number and subtract out the bias.
    t = ((u_val & QUANT_MASK) << 3) + BIAS;
    //t<<=((unsigned)u_val&SEG_MASK)>>SEG_SHIFT;
    t <<= (u_val & SEG_MASK) >> SEG_SHIFT;

    return ((u_val & SIGN_BIT) != 0) ? (BIAS - t) : (t - BIAS);
  }

}
