/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.flamesgroup.media.codec.audio.g711;

import com.flamesgroup.media.codec.audio.AudioCodecException;
import com.flamesgroup.media.codec.audio.AudioCodecUtils;
import com.flamesgroup.media.codec.audio.IAudioCodec;

abstract class G711AudioCodec implements IAudioCodec {

  protected static final int SIGN_BIT = 0x80;    // Sign bit for a A-law byte.
  protected static final int QUANT_MASK = 0xf;   // Quantization field mask.
  protected static final int SEG_SHIFT = 4;      // Left shift for segment number.
  protected static final int SEG_MASK = 0x70;    // Segment field mask.
  protected static final int BIAS = 0x84;        //Bias for pcm code.

  protected static final int[] segEnd = {0xFF, 0x1FF, 0x3FF, 0x7FF, 0xFFF, 0x1FFF, 0x3FFF, 0x7FFF};

  protected abstract int g711ToPcm(final int val);

  protected abstract int pcmToG711(final int pcm);

  @Override
  public byte[] decode(final byte[] data) {
    byte[] pcmData = new byte[data.length * 2];
    for (int i = 0; i < data.length; i++) {
      int pcm = g711ToPcm(data[i]);
      AudioCodecUtils.putPcmSample(pcm, pcmData, i * 2);
    }
    return pcmData;
  }

  @Override
  public byte[] encode(final byte[] pcm) throws AudioCodecException {
    if (pcm.length % 2 != 0) {
      throw new AudioCodecException("can't process pcm, pcmFrameSize must be divisible by 2");
    }
    byte[] g711Data = new byte[pcm.length / 2];
    for (int i = 0; i < pcm.length; i = i + 2) {
      int g711 = pcmToG711(AudioCodecUtils.getPcmSample(pcm, i));
      g711Data[i / 2] = (byte) g711;
    }
    return g711Data;
  }

  protected int search(final int val, final int[] table) {
    for (int i = 0; i < table.length; i++)
      if (val <= table[i])
        return i;
    return table.length;
  }

  @Override
  public int getFrameDuration() {
    return 1;
  }

}
