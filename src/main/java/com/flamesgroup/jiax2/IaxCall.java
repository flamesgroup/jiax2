/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.flamesgroup.jiax2;

import com.flamesgroup.jiax2.engine.SessionId;
import com.flamesgroup.media.codec.audio.AudioCodecException;
import com.flamesgroup.media.codec.audio.AudioCodecUtils;
import com.flamesgroup.media.codec.audio.IAudioCodec;
import com.flamesgroup.media.codec.audio.IAudioCodecFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;
import java.util.Objects;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public final class IaxCall implements ICall, ITerminatingCall, IOriginatingCall, IMediaStream {

  final static Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

  private static final EncodedMediaData closeEncodedMediaData = new EncodedMediaData(0, new byte[0]);
  private static final DecodedMediaData closeDecodedMediaData = new DecodedMediaData(0, new byte[0]);

  private final IaxPeer iaxPeer;
  private final SessionId sessionId;
  private final String caller;
  private final String called;
  private final String calledContext;
  private final IAudioCodecFactory audioCodecFactory;

  private final Lock controlLock = new ReentrantLock();
  private CallState state = CallState.INITIAL;
  private ICallHandler callHandler;
  private ITerminatingCallHandler terminatingCallHandler;
  private IOriginatingCallHandler originatingCallHandler;
  private long mediaFormat;
  private int writtenPcmDataSize;
  private IAudioCodec audioCodec;

  private final Lock mediaLock = new ReentrantLock();
  private IMediaStreamHandler mediaStreamHandler;

  private int lastTimestamp;
  private byte[] writtenPcmData;
  private int writtenPcmDataFillSize;

  private final BlockingQueue<EncodedMediaData> decodeMediaDataQueue = new LinkedBlockingDeque<>();
  private final BlockingQueue<DecodedMediaData> encodeMediaDataQueue = new LinkedBlockingDeque<>();

  public IaxCall(final IaxPeer iaxPeer, final SessionId sessionId, final String caller, final String called,
      final String calledContext, final IAudioCodecFactory audioCodecFactory, final long mediaFormat, final int audioFrames) {
    this.iaxPeer = iaxPeer;
    this.sessionId = sessionId;
    this.caller = caller;
    this.called = called;
    this.calledContext = calledContext;
    this.audioCodecFactory = audioCodecFactory;
    this.mediaFormat = mediaFormat;
    this.audioCodec = audioCodecFactory.createAudioCodec(mediaFormat);
    this.writtenPcmDataSize = audioFrames * AudioCodecUtils.durationToPcmSize(audioCodec.getFrameDuration());
  }

  protected IaxPeer getIaxPeer() {
    return iaxPeer;
  }

  protected SessionId getSessionId() {
    return sessionId;
  }

  private boolean checkCurrentStateWithExpectedAndWarnOnFail(final CallState expectedState) {
    int r = state.compareTo(expectedState);
    if (r > 0) {
      logger.warn("[{}] has illegal state: {}, when expected state before: {}", this, state, expectedState);
    } else if (r == 0) {
      logger.warn("[{}] already in state: {}", this, state);
    } else {
      return true;
    }
    return false;
  }

  protected void setState(final CallState state) {
    this.state = state;
  }

  // ICall
  @Override
  public CallState getState() {
    return state;
  }

  @Override
  public String getCaller() {
    return caller;
  }

  @Override
  public String getCalled() {
    return called;
  }

  @Override
  public String getCalledContext() {
    return calledContext;
  }

  @Override
  public IMediaStream getAudioMediaStream() {
    return this;
  }

  @Override
  public long getMediaFormat() {
    return mediaFormat;
  }

  @Override
  public IAudioCodec getAudioCodec() {
    return audioCodec;
  }

  @Override
  public void hangup(final CallDropReason callDropReason) {
    controlLock.lock();
    try {
      if (!checkCurrentStateWithExpectedAndWarnOnFail(CallState.HANGUP)) {
        return;
      } else {
        terminatingCallHandler = null;
        setState(CallState.HANGUP);
      }
    } finally {
      controlLock.unlock();
    }
    logger.debug("[{}] drop", this);

    getIaxPeer().processHangup(this, callDropReason.getCauseCode());
  }

  @Override
  public void sendDtmf(final char dtmf) {
    controlLock.lock();
    try {
      if (!checkCurrentStateWithExpectedAndWarnOnFail(CallState.HANGUP)) {
        return;
      }
    } finally {
      controlLock.unlock();
    }
    logger.debug("[{}] send dtmf {}", this, dtmf);

    getIaxPeer().processDtmf(this, dtmf);
  }

  // ITerminatingCall
  @Override
  public void ringing() {
    controlLock.lock();
    try {
      if (!checkCurrentStateWithExpectedAndWarnOnFail(CallState.RINGING)) {
        return;
      } else {
        setState(CallState.RINGING);
      }
    } finally {
      controlLock.unlock();
    }
    logger.debug("[{}] ringing", this);

    getIaxPeer().processRinging(this);
  }

  @Override
  public void answer() {
    controlLock.lock();
    try {
      if (!checkCurrentStateWithExpectedAndWarnOnFail(CallState.ANSWERED)) {
        return;
      } else {
        setState(CallState.ANSWERED);
      }
    } finally {
      controlLock.unlock();
    }
    logger.debug("[{}] answer", this);

    getIaxPeer().processAnswer(this);
  }

  // inits
  void initAsNewTerminationCall(final INewTerminatingCallHandler newTerminatingCallHandler) {
    controlLock.lock();
    try {
      ITerminatingCallHandler terminatingCallHandler = newTerminatingCallHandler.handleNewCall(this);
      if (terminatingCallHandler != null && checkCurrentStateWithExpectedAndWarnOnFail(CallState.ACCEPTED)) {
        this.callHandler = this.terminatingCallHandler = terminatingCallHandler;
        setState(CallState.ACCEPTED);
        getIaxPeer().processAccept(this);
        logger.debug("[{}] accept", this);
      } else {
        hangup(CallDropReason.TEMPORARY_FAILURE);
        logger.debug("[{}] accept temporary failure - call dropped", this);
      }
    } finally {
      controlLock.unlock();
    }
  }

  void initAsNewOriginationCall(final IOriginatingCallHandler originatingCallHandler) {
    controlLock.lock();
    try {
      if (originatingCallHandler != null && checkCurrentStateWithExpectedAndWarnOnFail(CallState.DIALED)) {
        this.callHandler = this.originatingCallHandler = originatingCallHandler;
        setState(CallState.DIALED);
        logger.debug("[{}] dial", this);
      } else {
        hangup(CallDropReason.TEMPORARY_FAILURE);
        logger.debug("[{}] dial temporary failure - call dropped", this);
      }
    } finally {
      controlLock.unlock();
    }
  }

  // control handlers
  public void handleHangup(final Byte causeCode) {
    ICallHandler localCallHandler;
    controlLock.lock();
    try {
      if (callHandler == null || !checkCurrentStateWithExpectedAndWarnOnFail(CallState.HANGUP)) {
        return;
      } else {
        localCallHandler = callHandler;
        callHandler = null;
        terminatingCallHandler = null;
        originatingCallHandler = null;
        setState(CallState.HANGUP);
      }
    } finally {
      controlLock.unlock();
    }

    CallDropReason callDropReason;
    if (causeCode != null) {
      callDropReason = CallDropReason.fromCauseCode(causeCode);
    } else {
      callDropReason = CallDropReason.UNDEFINED;
    }
    logger.debug("[{}] handleDrop: {}", this, callDropReason);
    localCallHandler.handleHangup(callDropReason);
  }

  void handleDtmf(final char dtmf) {
    ICallHandler localCallHandler;
    controlLock.lock();
    try {
      if (callHandler == null || !checkCurrentStateWithExpectedAndWarnOnFail(CallState.HANGUP)) {
        return;
      } else {
        localCallHandler = callHandler;
      }
    } finally {
      controlLock.unlock();
    }
    logger.debug("[{}] handleDtmf: {}", this, dtmf);
    localCallHandler.handleDtmf(dtmf);
  }

  void handleAccept(final long mediaFormat, final int audioFrames) {
    IOriginatingCallHandler localOriginatingCallHandler;
    controlLock.lock();
    try {
      if (originatingCallHandler == null || !checkCurrentStateWithExpectedAndWarnOnFail(CallState.ACCEPTED)) {
        return;
      } else {
        localOriginatingCallHandler = originatingCallHandler;
        setState(CallState.ACCEPTED);
        this.mediaFormat = mediaFormat;
        this.audioCodec = audioCodecFactory.createAudioCodec(mediaFormat);
        this.writtenPcmDataSize = audioFrames * AudioCodecUtils.durationToPcmSize(audioCodec.getFrameDuration());
      }
    } finally {
      controlLock.unlock();
    }
    logger.debug("[{}] handleAccept", this);
    localOriginatingCallHandler.handleAccept();
  }

  void handleRinging() {
    IOriginatingCallHandler localOriginatingCallHandler;
    controlLock.lock();
    try {
      if (originatingCallHandler == null || !checkCurrentStateWithExpectedAndWarnOnFail(CallState.RINGING)) {
        return;
      } else {
        localOriginatingCallHandler = originatingCallHandler;
        setState(CallState.RINGING);
      }
    } finally {
      controlLock.unlock();
    }
    logger.debug("[{}] handleRinging", this);
    localOriginatingCallHandler.handleRinging();
  }

  void handleAnswer() {
    IOriginatingCallHandler localOriginatingCallHandler;
    controlLock.lock();
    try {
      if (originatingCallHandler == null || !checkCurrentStateWithExpectedAndWarnOnFail(CallState.ANSWERED)) {
        return;
      } else {
        localOriginatingCallHandler = originatingCallHandler;
        setState(CallState.ANSWERED);
      }
    } finally {
      controlLock.unlock();
    }
    logger.debug("[{}] handleAnswer", this);
    localOriginatingCallHandler.handleAnswer();
  }

  // IMediaStream
  @Override
  public void open(final IMediaStreamHandler mediaStreamHandler) {
    Objects.requireNonNull(mediaStreamHandler, "mediaStreamHandler parameter mustn't be null");

    mediaLock.lock();
    try {
      this.mediaStreamHandler = mediaStreamHandler;
    } finally {
      mediaLock.unlock();
    }

    new Thread(new MediaDecoder(), "IaxCallMediaDecoder[" + sessionId + "]").start();
    new Thread(new MediaEncoder(), "IaxCallMediaEncoder[" + sessionId + "]").start();
  }

  @Override
  public void close() {
    try {
      decodeMediaDataQueue.put(closeEncodedMediaData);
    } catch (InterruptedException e) {
      logger.error("[{}] - unexpected InterruptedException when closing media decoder", this);
    }
    try {
      encodeMediaDataQueue.put(closeDecodedMediaData);
    } catch (InterruptedException e) {
      logger.error("[{}] - unexpected InterruptedException when closing media encoder", this);
    }

    mediaLock.lock();
    try {
      mediaStreamHandler = null;
    } finally {
      mediaLock.unlock();
    }
  }

  @Override
  public boolean isOpen() {
    return isOpenLocal(getMediaStreamHandlerLocal());
  }

  @Override
  public void write(final int timestamp, final byte[] data, final int offset, final int length) {
    if (writtenPcmDataFillSize == 0) {
      writtenPcmData = new byte[writtenPcmDataSize];
      lastTimestamp = timestamp;
    }

    int availablePcmDataLength = writtenPcmData.length - writtenPcmDataFillSize;
    int remainPcmDataToWriteLength = 0;
    if (availablePcmDataLength >= length) {
      availablePcmDataLength = length;
    } else {
      remainPcmDataToWriteLength = length - availablePcmDataLength;
    }

    System.arraycopy(data, offset, writtenPcmData, writtenPcmDataFillSize, availablePcmDataLength);
    writtenPcmDataFillSize += availablePcmDataLength;

    if (writtenPcmData.length == writtenPcmDataFillSize) {
      encodeMediaDataQueue.offer(new DecodedMediaData(lastTimestamp, writtenPcmData));
      writtenPcmDataFillSize = 0;
    }

    if (remainPcmDataToWriteLength > 0) {
      writtenPcmData = new byte[writtenPcmDataSize];
      writtenPcmDataFillSize = 0;
      lastTimestamp = timestamp + availablePcmDataLength / (AudioCodecUtils.durationToPcmSize(audioCodec.getFrameDuration()) / audioCodec.getFrameDuration());

      System.arraycopy(data, offset + availablePcmDataLength, writtenPcmData, writtenPcmDataFillSize, remainPcmDataToWriteLength);
      writtenPcmDataFillSize += remainPcmDataToWriteLength;
    }
  }

  void handleMedia(final int timestamp, final byte[] data) {
    decodeMediaDataQueue.offer(new EncodedMediaData(timestamp, data));
  }

  private IMediaStreamHandler getMediaStreamHandlerLocal() {
    mediaLock.lock();
    try {
      return mediaStreamHandler;
    } finally {
      mediaLock.unlock();
    }
  }

  private boolean isOpenLocal(final IMediaStreamHandler mediaStreamHandlerLocal) {
    return mediaStreamHandlerLocal != null;
  }

  private final class MediaDecoder implements Runnable {

    @Override
    public void run() {
      while (true) {
        try {
          EncodedMediaData encodedMediaData = decodeMediaDataQueue.take();
          if (encodedMediaData == closeEncodedMediaData) {
            break;
          }

          byte[] decodedData = audioCodec.decode(encodedMediaData.getData());

          IMediaStreamHandler mediaStreamHandlerLocal = getMediaStreamHandlerLocal();
          if (isOpenLocal(mediaStreamHandlerLocal)) {
            mediaStreamHandlerLocal.handleMedia(encodedMediaData.getTimestamp(), decodedData, 0, decodedData.length);
          }
        } catch (InterruptedException e) {
          logger.error("[{}] - unexpected InterruptedException - end executing", this, e);
          break;
        } catch (AudioCodecException e) {
          logger.error("[{}] - can't decode data to pcm", this, e);
        }
      }
    }

    @Override
    public String toString() {
      return getClass().getSimpleName() + "[" + sessionId + "]";
    }

  }

  private final class MediaEncoder implements Runnable {

    @Override
    public void run() {
      while (true) {
        try {
          DecodedMediaData decodedMediaData = encodeMediaDataQueue.take();
          if (decodedMediaData == closeDecodedMediaData) {
            break;
          }

          byte[] encodedData = audioCodec.encode(decodedMediaData.getData());

          getIaxPeer().processMedia(IaxCall.this, decodedMediaData.getTimestamp(), encodedData);
        } catch (InterruptedException e) {
          logger.error("[{}] - unexpected InterruptedException - end executing", this, e);
          break;
        } catch (AudioCodecException e) {
          logger.error("[{}] - can't encode data to codec", this, e);
        }
      }
    }

    @Override
    public String toString() {
      return getClass().getSimpleName() + "[" + sessionId + "]";
    }

  }

  private abstract static class MediaData {

    private final int timestamp;
    private final byte[] data;

    public MediaData(final int timestamp, final byte[] data) {
      this.timestamp = timestamp;
      this.data = data;
    }

    public int getTimestamp() {
      return timestamp;
    }

    public byte[] getData() {
      return data;
    }

  }

  private static final class DecodedMediaData extends MediaData {

    public DecodedMediaData(final int timestamp, final byte[] data) {
      super(timestamp, data);
    }

  }

  private static final class EncodedMediaData extends MediaData {

    public EncodedMediaData(final int timestamp, final byte[] data) {
      super(timestamp, data);
    }

  }

}
