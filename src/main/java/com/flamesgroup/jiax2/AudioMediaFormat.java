/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.flamesgroup.jiax2;

import com.flamesgroup.jiax2.frame.full.MediaFormat;

public enum AudioMediaFormat {

  G723_1(MediaFormat.G723_1),
  GSM(MediaFormat.GSM),
  ULAW(MediaFormat.ULAW),
  ALAW(MediaFormat.ALAW),
  G726(MediaFormat.G726),
  ADPCM(MediaFormat.ADPCM),
  SLIN(MediaFormat.SLIN),
  LPC10(MediaFormat.LPC10),
  G729(MediaFormat.G729),
  SPEEX(MediaFormat.SPEEX),
  ILBC(MediaFormat.ILBC),
  G726AAL2(MediaFormat.G726AAL2),
  G722(MediaFormat.G722),
  AMR(MediaFormat.AMR),
  GSM_HR(MediaFormat.GSM_HR),
  OPUS(MediaFormat.OPUS);

  private final long v_uint64;

  AudioMediaFormat(final long v_uint64) {
    this.v_uint64 = v_uint64;
  }

  public long getV_uint64() {
    return v_uint64;
  }

}
