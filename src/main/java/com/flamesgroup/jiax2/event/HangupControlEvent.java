/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.flamesgroup.jiax2.event;

import com.flamesgroup.jiax2.engine.SessionId;

public class HangupControlEvent extends ControlEvent {

  private final String cause;
  private final Byte causeCode;

  public HangupControlEvent(final SessionId sessionId, final String cause, final Byte causeCode) {
    super(sessionId);
    this.cause = cause;
    this.causeCode = causeCode;
  }

  public HangupControlEvent(final SessionId sessionId, final Byte causeCode) {
    this(sessionId, null, causeCode);
  }

  public String getCause() {
    return cause;
  }

  public Byte getCauseCode() {
    return causeCode;
  }

}
