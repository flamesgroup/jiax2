/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.flamesgroup.jiax2.event;

import com.flamesgroup.jiax2.engine.SessionId;

import java.net.SocketAddress;

public class NewControlEvent extends ControlEvent {

  private final SocketAddress address;
  private final String calledNumber;
  private final String callingNumber;
  private final String calledContext;
  private final Long format;
  private final Long capability;

  public NewControlEvent(final SessionId sessionId, final SocketAddress address, final String calledNumber, final String callingNumber, final String calledContext, final Long format,
      final Long capability) {
    super(sessionId);
    this.address = address;
    this.calledNumber = calledNumber;
    this.callingNumber = callingNumber;
    this.calledContext = calledContext;
    this.format = format;
    this.capability = capability;
  }

  public SocketAddress getAddress() {
    return address;
  }

  public String getCalledNumber() {
    return calledNumber;
  }

  public String getCallingNumber() {
    return callingNumber;
  }

  public String getCalledContext() {
    return calledContext;
  }

  public Long getFormat() {
    return format;
  }

  public Long getCapability() {
    return capability;
  }

}
