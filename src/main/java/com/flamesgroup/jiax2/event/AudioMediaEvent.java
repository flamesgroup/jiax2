/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.flamesgroup.jiax2.event;

import com.flamesgroup.jiax2.engine.SessionId;

public class AudioMediaEvent extends MediaEvent {

  private final long audioMediaFormat;
  private final int timestamp;
  private final byte[] data;

  public AudioMediaEvent(final SessionId sessionId, final long audioMediaFormat, final int timestamp, final byte[] data) {
    super(sessionId);
    this.audioMediaFormat = audioMediaFormat;
    this.timestamp = timestamp;
    this.data = data;
  }

  public long getAudioMediaFormat_uint64() {
    return audioMediaFormat;
  }

  public int getTimestamp() {
    return timestamp;
  }

  public byte[] getData() {
    return data;
  }

}
