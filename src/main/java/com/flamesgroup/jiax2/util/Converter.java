/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.flamesgroup.jiax2.util;

public final class Converter {

  private Converter() {
  }

  public static String byteArrayToHexString(final byte[] b, final int off, final int len) {
    StringBuilder hexString = new StringBuilder();
    for (int i = off; i < (off + len); i++) {
      appendByteToHex(hexString, b[i]);
    }
    return hexString.toString();
  }

  public static String byteArrayToHexString(final byte[] b) {
    return byteArrayToHexString(b, 0, b.length);
  }

  public static String longToHex(final long l) {
    StringBuilder sb = new StringBuilder();
    sb.append("0x");
    for (int bit = Long.SIZE - 8; bit >= 0; bit -= 8) {
      appendByteToHex(sb, (byte) (l >> bit));
    }
    return sb.toString();
  }

  public static String byteToHex(final byte b) {
    StringBuilder sb = new StringBuilder();
    sb.append("0x");
    appendByteToHex(sb, b);
    return sb.toString();
  }

  private static void appendByteToHex(final StringBuilder sb, final byte b) {
    for (int bit = Byte.SIZE - 4; bit >= 0; bit -= 4) {
      sb.append(nybbleToHex((b >> bit) & 0xF));
    }
  }

  private static char nybbleToHex(final int nybble) {
    if (nybble >= 0x0 && nybble <= 0x9) {
      return (char) ('0' + nybble);
    } else if (nybble >= 0xA && nybble <= 0xF) {
      return (char) ('a' + nybble - 0xA);
    } else {
      throw new AssertionError();
    }
  }

}
