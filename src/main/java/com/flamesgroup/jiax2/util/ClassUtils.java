/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.flamesgroup.jiax2.util;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Map;

public final class ClassUtils {

  private ClassUtils() {
  }

  public static Map<Byte, String> collectByteFieldNames(final Class c) {
    final Map<Byte, String> fieldNames = new HashMap<>();
    Field[] fields = c.getFields();
    for (Field field : fields) {
      if (field.getType() == byte.class) {
        if (isModifierPublicStaticFinal(field.getModifiers())) {
          byte v;
          try {
            v = field.getByte(c);
          } catch (IllegalAccessException e) {
            throw new AssertionError(e);
          }
          String n = field.getName();

          String pn = fieldNames.put(v, n);
          if (pn != null) {
            throw new AssertionError("fields " + pn + " and " + n + " with duplicate value " + v);
          }
        }
      }
    }
    return fieldNames;
  }

  public static Map<Short, String> collectShortFieldNames(final Class c) {
    final Map<Short, String> fieldNames = new HashMap<>();
    Field[] fields = c.getFields();
    for (Field field : fields) {
      if (field.getType() == short.class) {
        if (isModifierPublicStaticFinal(field.getModifiers())) {
          short v;
          try {
            v = field.getShort(c);
          } catch (IllegalAccessException e) {
            throw new AssertionError(e);
          }
          String n = field.getName();

          String pn = fieldNames.put(v, n);
          if (pn != null) {
            throw new AssertionError("fields " + pn + " and " + n + " with duplicate value " + v);
          }
        }
      }
    }
    return fieldNames;
  }

  public static Map<Long, String> collectLongFieldNames(final Class c) {
    final Map<Long, String> fieldNames = new HashMap<>();
    Field[] fields = c.getFields();
    for (Field field : fields) {
      if (field.getType() == long.class) {
        if (isModifierPublicStaticFinal(field.getModifiers())) {
          long v;
          try {
            v = field.getLong(c);
          } catch (IllegalAccessException e) {
            throw new AssertionError(e);
          }
          String n = field.getName();

          String pn = fieldNames.put(v, n);
          if (pn != null) {
            throw new AssertionError("fields " + pn + " and " + n + " with duplicate value " + v);
          }
        }
      }
    }
    return fieldNames;
  }

  private static boolean isModifierPublicStaticFinal(final int mod) {
    return (mod & (Modifier.PUBLIC | Modifier.STATIC | Modifier.FINAL)) == (Modifier.PUBLIC | Modifier.STATIC | Modifier.FINAL);
  }

}
