/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.flamesgroup.jiax2.frame.mini;

import com.flamesgroup.jiax2.frame.SingularFrame;
import com.flamesgroup.jiax2.util.DataUtils;

import java.nio.ByteBuffer;
import java.util.Objects;

public class MiniFrame extends SingularFrame {

  private short timestamp_uint16;
  private byte[] data = DataUtils.EMPTY_DATA;

  public short getTimestamp_uint16() {
    return timestamp_uint16;
  }

  public void setTimestamp_uint16(final short timestamp_uint16) {
    this.timestamp_uint16 = timestamp_uint16;
  }

  public byte[] getData() {
    return data;
  }

  public void setData(final byte[] data) {
    Objects.requireNonNull(data);
    this.data = data;
  }

  @Override
  public void encodeTo(final ByteBuffer bb) {
    bb.putShort((short) (getSourceCallNumber_uint16() | 0x0000));
    bb.putShort(getTimestamp_uint16());
    bb.put(getData());
  }

  @Override
  public void decodeFrom(final ByteBuffer bb) {
    short sourceCallNumber_uint16;
    short timestamp_uint16;
    byte[] data;

    {
      short uint16 = bb.getShort();
      boolean fullFrame = (uint16 & 0x8000) != 0;
      if (fullFrame) {
        throw new IllegalArgumentException("Full Frame bit is not set to 0");
      }
      sourceCallNumber_uint16 = (short) (uint16 & 0x7FFF);
    }
    timestamp_uint16 = bb.getShort();
    data = DataUtils.remaining2array(bb);

    setSourceCallNumber_uint16(sourceCallNumber_uint16);
    setTimestamp_uint16(timestamp_uint16);
    setData(data);
  }

  @Override
  public String toString() {
    return getClass().getSimpleName() + "@" + Integer.toHexString(hashCode());
  }

}
