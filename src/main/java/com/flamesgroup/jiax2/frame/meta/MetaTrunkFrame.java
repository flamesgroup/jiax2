/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.flamesgroup.jiax2.frame.meta;

import com.flamesgroup.jiax2.frame.Frame;
import com.flamesgroup.jiax2.frame.PluralFrame;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

public abstract class MetaTrunkFrame<TMediaItem extends MetaTrunkFrame.MediaItem> extends PluralFrame {

  public static final int HEADER_LENGTH = 8;

  private int timestamp_uint32;

  private final List<TMediaItem> mediaItems = new ArrayList<>();

  public int getTimestamp_uint32() {
    return timestamp_uint32;
  }

  public void setTimestamp_uint32(final int timestamp_uint32) {
    this.timestamp_uint32 = timestamp_uint32;
  }

  public List<TMediaItem> getMediaItems() {
    return mediaItems;
  }

  public int getLength() {
    int length = HEADER_LENGTH;
    for (TMediaItem mediaItem : getMediaItems()) {
      length += mediaItem.getLength();
    }
    return length;
  }

  @Override
  public void encodeTo(final ByteBuffer bb) {
    bb.putShort((short) (0x0000));
    bb.put((byte) 0x01);
    bb.put(getMetaCommandData_uint8());
    bb.putInt(getTimestamp_uint32());
    for (TMediaItem mediaItem : getMediaItems()) {
      mediaItem.encodeTo(bb);
    }
  }

  @Override
  public void decodeFrom(final ByteBuffer bb) {
    int timestamp_uint32;

    {
      short uint16 = bb.getShort(); // Meta Indicator
      boolean fullFrame = (uint16 & 0x8000) != 0;
      if (fullFrame) {
        throw new IllegalArgumentException("Full Frame bit is not set to 0");
      }
      if (uint16 != 0) {
        throw new IllegalArgumentException("Meta Indicator bits is not set to 0");
      }
    }
    {
      byte uint8 = bb.get(); // Meta Command
      boolean metaVideoFrame = (uint8 & 0x80) != 0;
      if (metaVideoFrame) {
        throw new IllegalArgumentException("Meta VIDEO Frame bit is set to 1");
      }
      if (uint8 != 1) {
        throw new IllegalArgumentException("Meta Command is not set to 1");
      }
    }
    {
      byte uint8 = bb.get(); // Command Data
      if (uint8 != getMetaCommandData_uint8()) {
        throw new IllegalArgumentException("Meta Command Data is incorrect - expected [" + getMetaCommandData_uint8() + "] but it is [" + uint8 + "]");
      }
    }
    timestamp_uint32 = bb.getInt();
    try {
      while (bb.hasRemaining()) {
        TMediaItem mediaItem = createMetaItem();
        mediaItem.decodeFrom(bb);
        getMediaItems().add(mediaItem);
      }
    } catch (Exception e) {
      getMediaItems().clear(); // clear previously added items if any Exception during decode occur
      throw e;
    }

    setTimestamp_uint32(timestamp_uint32);
    // mediaItems already filled with items
  }

  protected abstract byte getMetaCommandData_uint8();

  protected abstract TMediaItem createMetaItem();

  @Override
  public String toString() {
    return getClass().getSimpleName() + "@" + Integer.toHexString(hashCode());
  }

  public abstract static class MediaItem extends Frame {

    public abstract int getLength();

    @Override
    public String toString() {
      return getClass().getEnclosingClass().getSimpleName() + "$" + getClass().getSimpleName() + "@" + Integer.toHexString(hashCode());
    }

  }

}
