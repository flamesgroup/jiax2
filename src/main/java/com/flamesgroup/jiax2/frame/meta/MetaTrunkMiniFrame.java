/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.flamesgroup.jiax2.frame.meta;

import com.flamesgroup.jiax2.util.DataUtils;

import java.nio.ByteBuffer;
import java.util.Objects;

public final class MetaTrunkMiniFrame extends MetaTrunkFrame<MetaTrunkMiniFrame.MediaItem> {

  @Override
  protected byte getMetaCommandData_uint8() {
    return 0x01;
  }

  @Override
  protected MediaItem createMetaItem() {
    return new MediaItem();
  }

  public static class MediaItem extends MetaTrunkFrame.MediaItem {

    public static final int HEADER_LENGTH = 6;

    private short sourceCallNumber_uint16;
    private short timestamp_uint16;
    private byte[] data = DataUtils.EMPTY_DATA;

    public short getSourceCallNumber_uint16() {
      return sourceCallNumber_uint16;
    }

    public void setSourceCallNumber_uint16(final short sourceCallNumber_uint16) {
      if (Integer.compareUnsigned(sourceCallNumber_uint16, 0x7FFF) > 0) {
        throw new IllegalArgumentException("Source Call Number is overflow 15-bit value");
      }
      this.sourceCallNumber_uint16 = sourceCallNumber_uint16;
    }

    public short getTimestamp_uint16() {
      return timestamp_uint16;
    }

    public void setTimestamp_uint16(final short timestamp_uint16) {
      this.timestamp_uint16 = timestamp_uint16;
    }

    public byte[] getData() {
      return data;
    }

    public void setData(final byte[] data) {
      Objects.requireNonNull(data);
      this.data = data;
    }

    @Override
    public int getLength() {
      return HEADER_LENGTH + data.length;
    }

    @Override
    public void encodeTo(final ByteBuffer bb) {
      bb.putShort((short) getData().length);
      bb.putShort(getSourceCallNumber_uint16());
      bb.putShort(getTimestamp_uint16());
      bb.put(getData());
    }

    @Override
    public void decodeFrom(final ByteBuffer bb) {
      int dataLength;
      short sourceCallNumber_uint16;
      short timestamp_uint16;
      byte[] data;

      dataLength = Short.toUnsignedInt(bb.getShort());
      sourceCallNumber_uint16 = (short) (bb.getShort() & 0x7FFF);
      timestamp_uint16 = bb.getShort();
      data = DataUtils.length2array(bb, dataLength);

      setSourceCallNumber_uint16(sourceCallNumber_uint16);
      setTimestamp_uint16(timestamp_uint16);
      setData(data);
    }

  }

}
