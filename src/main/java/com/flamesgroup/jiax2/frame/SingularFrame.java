/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.flamesgroup.jiax2.frame;

public abstract class SingularFrame extends Frame {

  private short sourceCallNumber_uint16;

  public short getSourceCallNumber_uint16() {
    return sourceCallNumber_uint16;
  }

  public void setSourceCallNumber_uint16(final short sourceCallNumber_uint16) {
    if (Integer.compareUnsigned(sourceCallNumber_uint16, 0x7FFF) > 0) {
      throw new IllegalArgumentException("Source Call Number is overflow 15-bit value");
    }
    this.sourceCallNumber_uint16 = sourceCallNumber_uint16;
  }

}
