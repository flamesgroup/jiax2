/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.flamesgroup.jiax2.frame.full.ie;

import java.util.Objects;

public class IeItem {

  private final byte ie_uint8;
  private final byte[] data;

  public IeItem(final byte ie_uint8, final byte[] data) {
    Objects.requireNonNull(data);
    if (data.length > 255) {
      throw new IllegalArgumentException("IE Data Length is overflow 8-bit value");
    }

    this.ie_uint8 = ie_uint8;
    this.data = data;
  }

  public byte getIe_uint8() {
    return ie_uint8;
  }

  public byte[] getData() {
    return data;
  }

}
