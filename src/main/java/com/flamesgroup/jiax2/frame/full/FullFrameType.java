/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.flamesgroup.jiax2.frame.full;

import com.flamesgroup.jiax2.util.ClassUtils;

import java.util.Map;

public final class FullFrameType {

  public static final byte DTMF = (byte) 0x01;
  public static final byte VOICE = (byte) 0x02;
  public static final byte VIDEO = (byte) 0x03;
  public static final byte CONTROL = (byte) 0x04;
  public static final byte NULL = (byte) 0x05;
  public static final byte IAX = (byte) 0x06;
  public static final byte TEXT = (byte) 0x07;
  public static final byte IMAGE = (byte) 0x08;
  public static final byte HTML = (byte) 0x09;
  public static final byte COMFORT_NOISE = (byte) 0x0A;

  private static final Map<Byte, String> typeNames;

  private FullFrameType() {
  }

  public static String asString(final byte type) {
    String name = typeNames.get(type);
    if (name == null) {
      name = "<Unknown Full Frame Type " + type + ">";
    }
    return name;
  }

  static {
    typeNames = ClassUtils.collectByteFieldNames(FullFrameType.class);
  }

}
