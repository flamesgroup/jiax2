/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.flamesgroup.jiax2.frame.full.ie;

public enum IeType {

  CALLED_NUMBER((byte) 0x01, CalledNumberIe.class),
  CALLING_NUMBER((byte) 0x02, CallingNumberIe.class),
  CALLING_ANI((byte) 0x03, CallingAniIe.class),
  CALLING_NAME((byte) 0x04, CallingNameIe.class),
  CALLED_CONTEXT((byte) 0x05, CalledContextIe.class),
  USERNAME((byte) 0x06, UsernameIe.class),
  PASSWORD((byte) 0x07, PasswordIe.class),
  CAPABILITY((byte) 0x08, CapabilityIe.class),
  FORMAT((byte) 0x09, FormatIe.class),
  LANGUAGE((byte) 0x0a, LanguageIe.class),
  VERSION((byte) 0x0b, VersionIe.class),
  ADSICPE((byte) 0x0c, AdsicpeIe.class),
  DNID((byte) 0x0d, DnidIe.class),
  AUTHMETHODS((byte) 0x0e, AuthmethodsIe.class),
  CHALLENGE((byte) 0x0f, ChallengeIe.class),
  MD5_RESULT((byte) 0x10, Md5ResultIe.class),
  RSA_RESULT((byte) 0x11, RsaResultIe.class),
  APPARENT_ADDR((byte) 0x12, ApparentAddrIe.class),
  REFRESH((byte) 0x13, RefreshIe.class),
  DPSTATUS((byte) 0x14, DpstatusIe.class),
  CALLNO((byte) 0x15, CallnoIe.class),
  CAUSE((byte) 0x16, CauseIe.class),
  IAX_UNKNOWN((byte) 0x17, IaxUnknownIe.class),
  MSGCOUNT((byte) 0x18, MsgcountIe.class),
  AUTOANSWER((byte) 0x19, AutoanswerIe.class),
  MUSICONHOLD((byte) 0x1a, MusiconholdIe.class),
  TRANSFERID((byte) 0x1b, TransferidIe.class),
  RDNIS((byte) 0x1c, RdnisIe.class),
  PROVISIONING((byte) 0x1d, ProvisioningIe.class),
  AESPROVISIONING((byte) 0x1e, AesprovisioningIe.class),
  DATETIME((byte) 0x1f, DatetimeIe.class),
  DEVICETYPE((byte) 0x20, DevicetypeIe.class),
  SERVICEIDENT((byte) 0x21, ServiceidentIe.class),
  FIRMWAREVER((byte) 0x22, FirmwareverIe.class),
  FWBLOCKDESC((byte) 0x23, FwblockdescIe.class),
  FWBLOCKDATA((byte) 0x24, FwblockdataIe.class),
  PROVVER((byte) 0x25, ProvverIe.class),
  CALLINGPRES((byte) 0x26, CallingpresIe.class),
  CALLINGTON((byte) 0x27, CallingtonIe.class),
  CALLINGTNS((byte) 0x28, CallingtnsIe.class),
  SAMPLINGRATE((byte) 0x29, SamplingrateIe.class),
  CAUSECODE((byte) 0x2a, CausecodeIe.class),
  ENCRYPTION((byte) 0x2b, EncryptionIe.class),
  ENCKEY((byte) 0x2c, EnckeyIe.class),
  CODEC_PREFS((byte) 0x2d, CodecPrefsIe.class),
  RR_JITTER((byte) 0x2e, RrJitterIe.class),
  RR_LOSS((byte) 0x2f, RrLossIe.class),
  RR_PKTS((byte) 0x30, RrPktsIe.class),
  RR_DELAY((byte) 0x31, RrDelayIe.class),
  RR_DROPPED((byte) 0x32, RrDroppedIe.class),
  RR_OOO((byte) 0x33, RrOooIe.class),
  CALLTOKEN((byte) 0x36, CalltokenIe.class),
  CAPABILITY2((byte) 0x37, Capability2Ie.class),
  FORMAT2((byte) 0x38, Format2Ie.class);

  private final byte v_uint8;
  private final Class<? extends Ie> c;

  IeType(final byte v_uint8, final Class<? extends Ie> c) {
    this.v_uint8 = v_uint8;
    this.c = c;
  }

  public byte getV_uint8() {
    return v_uint8;
  }

  public Class<? extends Ie> getC() {
    return c;
  }
}
