/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.flamesgroup.jiax2.frame.full.ie;

import java.lang.reflect.Constructor;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class IeMap {

  private final Map<Class, Ie> map = new HashMap<>();

  public void clear() {
    map.clear();
  }

  public <T extends Ie> T get(final Class<T> type) {
    return type.cast(map.get(type));
  }

  public <T extends Ie> T put(final Class<T> type, final T value) {
    return type.cast(map.put(type, value));
  }

  public Ie getRaw(final Class<? extends Ie> type) {
    return map.get(type);
  }

  public Ie putRaw(final Class<? extends Ie> type, final Ie value) {
    return map.put(type, value);
  }

  public int size() {
    return map.size();
  }

  public Collection<Ie> values() {
    return map.values();
  }

  public <T extends Ie> T add(final T value) {
    Class<? extends Ie> type = value.getClass();
    Ie ie = map.put(type, value);
    return (T) type.cast(ie);
  }

  public void setUtf8(final Class<? extends BaseUtf8Ie> type, final String value) {
    if (value != null) {
      BaseUtf8Ie utf8Ie = instantiateBaseUtf8Ie(type, value);
      put((Class<BaseUtf8Ie>) type, utf8Ie);
    }
  }

  public void setUint8(final Class<? extends BaseUint8Ie> type, final Byte value) {
    if (value != null) {
      BaseUint8Ie uint8Ie = instantiateBaseUint8Ie(type, value);
      put((Class<BaseUint8Ie>) type, uint8Ie);
    }
  }

  public void setUint16(final Class<? extends BaseUint16Ie> type, final Short value) {
    if (value != null) {
      BaseUint16Ie uint16Ie = instantiateBaseUint16Ie(type, value);
      put((Class<BaseUint16Ie>) type, uint16Ie);
    }
  }

  public void setUint32(final Class<? extends BaseUint32Ie> type, final Integer value) {
    if (value != null) {
      BaseUint32Ie uint8Ie = instantiateBaseUint32Ie(type, value);
      put((Class<BaseUint32Ie>) type, uint8Ie);
    }
  }

  public String getUtf8(final Class<? extends BaseUtf8Ie> type) {
    BaseUtf8Ie utf8Ie = get(type);
    if (utf8Ie != null) {
      return utf8Ie.getV();
    } else {
      return null;
    }
  }

  public Byte getUint8(final Class<? extends BaseUint8Ie> type) {
    BaseUint8Ie uint8Ie = get(type);
    if (uint8Ie != null) {
      return uint8Ie.getV_uint8();
    } else {
      return null;
    }
  }

  public Short getUint16(final Class<? extends BaseUint16Ie> type) {
    BaseUint16Ie uint16Ie = get(type);
    if (uint16Ie != null) {
      return uint16Ie.getV_uint16();
    } else {
      return null;
    }
  }

  public Integer getUint32(final Class<? extends BaseUint32Ie> type) {
    BaseUint32Ie uint32Ie = get(type);
    if (uint32Ie != null) {
      return uint32Ie.getV_uint32();
    } else {
      return null;
    }
  }

  private BaseUtf8Ie instantiateBaseUtf8Ie(final Class<? extends BaseUtf8Ie> type, final String value) {
    try {
      Constructor<? extends BaseUtf8Ie> constructor = type.getConstructor(String.class);
      return constructor.newInstance(value);
    } catch (ReflectiveOperationException e) {
      throw new AssertionError(e);
    }
  }

  private BaseUint8Ie instantiateBaseUint8Ie(final Class<? extends BaseUint8Ie> type, final byte value) {
    try {
      Constructor<? extends BaseUint8Ie> constructor = type.getConstructor(byte.class);
      return constructor.newInstance(value);
    } catch (ReflectiveOperationException e) {
      throw new AssertionError(e);
    }
  }

  private BaseUint16Ie instantiateBaseUint16Ie(final Class<? extends BaseUint16Ie> type, final short value) {
    try {
      Constructor<? extends BaseUint16Ie> constructor = type.getConstructor(short.class);
      return constructor.newInstance(value);
    } catch (ReflectiveOperationException e) {
      throw new AssertionError(e);
    }
  }

  private BaseUint32Ie instantiateBaseUint32Ie(final Class<? extends BaseUint32Ie> type, final int value) {
    try {
      Constructor<? extends BaseUint32Ie> constructor = type.getConstructor(int.class);
      return constructor.newInstance(value);
    } catch (ReflectiveOperationException e) {
      throw new AssertionError(e);
    }
  }


}
