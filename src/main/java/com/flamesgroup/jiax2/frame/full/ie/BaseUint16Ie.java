/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.flamesgroup.jiax2.frame.full.ie;

public abstract class BaseUint16Ie implements Ie {

  private short v_uint16;

  public BaseUint16Ie(final short v_uint16) {
    this.v_uint16 = v_uint16;
  }

  public BaseUint16Ie() {
  }

  public short getV_uint16() {
    return v_uint16;
  }

  public void setV_uint16(final short v_uint16) {
    this.v_uint16 = v_uint16;
  }

  @Override
  public byte[] encode() {
    return new byte[] {(byte) (v_uint16 >> 8), (byte) v_uint16};
  }

  @Override
  public void decode(final byte[] ieData) {
    if (ieData.length != 2) {
      throw new IllegalArgumentException("IE Data Length must be 2");
    }

    v_uint16 = (short) ((ieData[0] & 0xFF) << 8 | (ieData[1] & 0xFF));
  }

}
