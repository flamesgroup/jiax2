/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.flamesgroup.jiax2.frame.full;

import com.flamesgroup.jiax2.util.ClassUtils;

import java.util.Map;

public final class ControlFullFrameSubclass {

  public static final byte HANGUP = (byte) 0x01;
  // public static final byte Reserved = (byte) 0x02;
  public static final byte RINGING = (byte) 0x03;
  public static final byte ANSWER = (byte) 0x04;
  public static final byte BUSY = (byte) 0x05;
  // public static final byte Reserved = (byte) 0x06;
  // public static final byte Reserved = (byte) 0x07;
  public static final byte CONGESTION = (byte) 0x08;
  public static final byte FLASH_HOOK = (byte) 0x09;
  public static final byte OPTION = (byte) 0x0B;
  public static final byte KEY_RADIO = (byte) 0x0C;
  public static final byte UNKEY_RADIO = (byte) 0x0D;
  public static final byte CALL_PROGRESSING = (byte) 0x0E;
  public static final byte CALL_PROCEEDING = (byte) 0x0F;
  public static final byte HOLD = (byte) 0x10;
  public static final byte UNHOLD = (byte) 0x11;
  public static final byte STOP_SOUNDS = (byte) 0xFF;

  private static final Map<Byte, String> subclassNames;

  private ControlFullFrameSubclass() {
  }

  public static String asString(final byte subclass) {
    String name = subclassNames.get(subclass);
    if (name == null) {
      name = "<Unknown Control Subclass " + subclass + ">";
    }
    return name;
  }

  static {
    subclassNames = ClassUtils.collectByteFieldNames(ControlFullFrameSubclass.class);
  }

}
