/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.flamesgroup.jiax2.frame.full;

import com.flamesgroup.jiax2.util.ClassUtils;

import java.util.Map;

public final class AuthenticationMethods {

  // public static final short Reserved = (short) 0x0001; // was Plaintext
  public static final short MD5 = (short) 0x0002;
  public static final short RSA = (short) 0x0003;

  private static final Map<Short, String> methodNames;

  private AuthenticationMethods() {
  }

  public static String asString(final short method) {
    String name = methodNames.get(method);
    if (name == null) {
      name = "<Unknown Authentication Method " + method + ">";
    }
    return name;
  }

  static {
    methodNames = ClassUtils.collectShortFieldNames(AuthenticationMethods.class);
  }

}
