/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.flamesgroup.jiax2.frame.full;

import com.flamesgroup.jiax2.util.ClassUtils;

import java.util.Map;

public final class MediaFormat {

  public static final long G723_1 = 1L << 0;
  public static final long GSM = 1L << 1;
  public static final long ULAW = 1L << 2;
  public static final long ALAW = 1L << 3;
  public static final long G726 = 1L << 4;
  public static final long ADPCM = 1L << 5;
  public static final long SLIN = 1L << 6;
  public static final long LPC10 = 1L << 7;
  public static final long G729 = 1L << 8;
  public static final long SPEEX = 1L << 9;
  public static final long ILBC = 1L << 10;
  public static final long G726AAL2 = 1L << 11;
  public static final long G722 = 1L << 12;
  public static final long AMR = 1L << 13;
  public static final long GSM_HR = 1L << 31; // NOTE: GSM Half Rate is not defined in RFC5456 - took from yate
  public static final long OPUS = 1L << 34; // NOTE: OPUS is not defined in RFC5456 - took from asterisk
  public static final long JPEG = 1L << 16;
  public static final long PNG = 1L << 17;
  public static final long H261 = 1L << 18;
  public static final long H263 = 1L << 19;
  public static final long H263p = 1L << 20;
  public static final long H264 = 1L << 21;

  private static final Map<Long, String> formatNames;

  private MediaFormat() {
  }

  public static String asString(final long format) {
    String name = formatNames.get(format);
    if (name == null) {
      name = "<Unknown Media Format " + format + ">";
    }
    return name;
  }

  static {
    formatNames = ClassUtils.collectLongFieldNames(MediaFormat.class);
  }

}
