/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.flamesgroup.jiax2.frame.full;

import com.flamesgroup.jiax2.frame.full.ie.Ie;
import com.flamesgroup.jiax2.frame.full.ie.IeItem;
import com.flamesgroup.jiax2.frame.full.ie.IeMap;
import com.flamesgroup.jiax2.frame.full.ie.IeType;
import com.flamesgroup.jiax2.util.Converter;
import com.flamesgroup.jiax2.util.DataUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;
import java.lang.reflect.Constructor;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class IeUtils {

  final static Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

  private static final Map<Byte, Class<? extends Ie>> ieType2Class = new HashMap<>();

  public static byte[] encode(final IeMap ieMap) {
    LinkedList<IeItem> ieItems = new LinkedList<>();

    for (Ie ie : ieMap.values()) {
      IeItem item = new IeItem(ie.getType().getV_uint8(), ie.encode());

      /*
      RFC 5456 - 6.2.2.
      A NEW message MUST include the 'version' IE, and it MUST be the first
      IE; the order of other IEs is unspecified.

      So make 'version' IE always be the first :)
       */
      if (item.getIe_uint8() == IeType.VERSION.getV_uint8()) {
        ieItems.addFirst(item);
      } else {
        ieItems.addLast(item);
      }
    }

    int encodedDataLength = 0;
    for (IeItem ie : ieItems) {
      encodedDataLength += 1 + 1 + ie.getData().length;
    }

    byte[] data = new byte[encodedDataLength];

    ByteBuffer bb = ByteBuffer.wrap(data);
    for (IeItem ie : ieItems) {
      bb.put(ie.getIe_uint8());
      bb.put((byte) ie.getData().length);
      bb.put(ie.getData());
    }
    return bb.array();
  }

  public static IeMap decode(final byte[] data) {
    ByteBuffer bb = ByteBuffer.wrap(data);

    LinkedList<IeItem> ieItems = new LinkedList<>();

    while (bb.hasRemaining()) {
      byte ieIndex = bb.get();
      int ieDataLength = Byte.toUnsignedInt(bb.get());
      byte[] ieData = DataUtils.length2array(bb, ieDataLength);

      ieItems.add(new IeItem(ieIndex, ieData));
    }

    IeMap ieMap = new IeMap();
    for (IeItem item : ieItems) {
      Class<? extends Ie> ieClass = ieType2Class.get(item.getIe_uint8());
      if (ieClass == null) {
        logger.warn("Unknown IE Type [{}] - IE ignored", Converter.byteToHex(item.getIe_uint8()));
      } else {
        Ie ie = instantiateIe(ieClass);

        ie.decode(item.getData());

        ieMap.putRaw(ieClass, ie);
      }
    }
    return ieMap;
  }

  private static Ie instantiateIe(final Class<? extends Ie> ieClass) {
    try {
      Constructor<? extends Ie> constructor = ieClass.getConstructor();
      return constructor.newInstance();
    } catch (ReflectiveOperationException e) {
      throw new AssertionError(e);
    }
  }

  static {
    for (IeType type : IeType.values()) {
      if (type.getC() != null) {
        ieType2Class.put(type.getV_uint8(), type.getC());
      }
    }
  }


}
