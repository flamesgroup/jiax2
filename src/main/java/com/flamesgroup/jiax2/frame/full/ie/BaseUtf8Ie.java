/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.flamesgroup.jiax2.frame.full.ie;

import java.nio.charset.StandardCharsets;
import java.util.Objects;

public abstract class BaseUtf8Ie implements Ie {

  private String v;

  public BaseUtf8Ie(final String v) {
    this.v = v;
  }

  public BaseUtf8Ie() {
    this("");
  }

  public String getV() {
    return v;
  }

  public void setV(final String v) {
    Objects.requireNonNull(v);
    this.v = v;
  }

  @Override
  public byte[] encode() {
    return v.getBytes(StandardCharsets.UTF_8);
  }

  @Override
  public void decode(final byte[] ieData) {
    v = new String(ieData, StandardCharsets.UTF_8);
  }

}
