/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.flamesgroup.jiax2.frame.full;

import com.flamesgroup.jiax2.util.ClassUtils;

import java.util.HashMap;
import java.util.Map;

public final class CauseCode {

  public static final byte UNALLOCATED = (byte) 1;
  public static final byte NO_ROUTE_TO_NETWORK = (byte) 2;
  public static final byte NO_ROUTE = (byte) 3;
  public static final byte CHANNEL_UNACCEPTABLE = (byte) 6;
  public static final byte CALL_DELIVERED = (byte) 7;
  public static final byte _OPERATOR_DETERMINED_BARRING = (byte) 8;
  public static final byte _FDN_MISMATCH = (byte) 9;
  public static final byte NORMAL_CLEARING = (byte) 16;
  public static final byte BUSY = (byte) 17;
  public static final byte NO_RESPONSE = (byte) 18;
  public static final byte NO_ANSWER = (byte) 19;
  public static final byte REJECTED = (byte) 21;
  public static final byte MOVED = (byte) 22;
  public static final byte _PRE_EMPTION = (byte) 25;
  public static final byte _NON_SELECTED_USER_CLEARING = (byte) 26;
  public static final byte OUT_OF_ORDER = (byte) 27;
  public static final byte INVALID_NUMBER = (byte) 28;
  public static final byte FACILITY_REJECTED = (byte) 29;
  public static final byte STATUS_ENQUIRY_RSP = (byte) 30;
  public static final byte NORMAL = (byte) 31;
  public static final byte CHANNEL_CONGESTION = (byte) 34;
  public static final byte NET_OUT_OF_ORDER = (byte) 38;
  public static final byte TEMPORARY_FAILURE = (byte) 41;
  public static final byte SWITCH_CONGESTION = (byte) 42;
  public static final byte ACCESS_INFO_DISCARDED = (byte) 43;
  public static final byte CHANNEL_UNAVAILABLE = (byte) 44;
  public static final byte PREEMPTED = (byte) 45;
  public static final byte NO_RESOURCE = (byte) 47;
  public static final byte _QUALITY_OF_SERVICE_UNAVAILABLE = (byte) 49;
  public static final byte FACILITY_NOT_SUBSCRIBED = (byte) 50;
  public static final byte BARRED_OUT = (byte) 52;
  public static final byte BARRED_IN = (byte) 54;
  public static final byte _INCOMING_CALLS_BARRED_WITHIN_THE_CUG = (byte) 55;
  public static final byte BEARER_CAP_NOT_AUTH = (byte) 57;
  public static final byte BEARER_CAP_NOT_AVAILABLE = (byte) 58;
  public static final byte SERVICE_UNAVAILABLE = (byte) 63;
  public static final byte BEARER_CAP_NOT_IMPLEMENTED = (byte) 65;
  public static final byte CHANNEL_TYPE_NOT_IMPLEMENTED = (byte) 66;
  public static final byte _ACM_EQUAL_OR_GREATER_THAN_ACMMAX = (byte) 68;
  public static final byte FACILITY_NOT_IMPLEMENTED = (byte) 69;
  public static final byte RESTRICT_BEARER_CAP_AVAIL = (byte) 70;
  public static final byte SERVICE_NOT_IMPLEMENTED = (byte) 79;
  public static final byte INVALID_CALL_REFERENCE = (byte) 81;
  public static final byte UNKNOWN_CHANNEL = (byte) 82;
  public static final byte UNKNOWN_CALLID = (byte) 83;
  public static final byte DUPLICATE_CALLID = (byte) 84;
  public static final byte NO_CALL_SUSPENDED = (byte) 85;
  public static final byte SUSPENDED_CALL_CLEARED = (byte) 86;
  public static final byte _USER_NOT_MEMBER_OF_CUG = (byte) 87;
  public static final byte INCOMPATIBLE_DEST = (byte) 88;
  public static final byte INVALID_TRANSIT_NET = (byte) 91;
  public static final byte INVALID_MESSAGE = (byte) 95;
  public static final byte MISSING_MANDATORY_IE = (byte) 96;
  public static final byte UNKNOWN_MESSAGE = (byte) 97;
  public static final byte WRONG_MESSAGE = (byte) 98;
  public static final byte UNKNOWN_IE = (byte) 99;
  public static final byte INVALID_IE = (byte) 100;
  public static final byte WRONG_STATE_MESSAGE = (byte) 101;
  public static final byte TIMEOUT = (byte) 102;
  public static final byte MANDATORY_IE_LEN = (byte) 103;
  public static final byte PROTOCOL_ERROR = (byte) 111;
  public static final byte INTERWORKING = (byte) 127;
  public static final byte _FDN_IS_ACTIVE_AND_NUMBER_IS_NOT_IN_FDN = (byte) 240;
  public static final byte _CALL_OPERATION_NOT_ALLOWED = (byte) 241;
  public static final byte _CALL_BARRING_ON_OUTGOING_CALLS = (byte) 252;
  public static final byte _CALL_BARRING_ON_INCOMING_CALLS = (byte) 253;
  public static final byte _CALL_IMPOSSIBLE = (byte) 254;
  public static final byte _LOWER_LAYER_FAILURE = (byte) 255;

  private static final Map<Byte, String> causeCodeNames;
  private static final Map<Byte, String> causeCodeExplanations;

  private CauseCode() {
  }

  public static String asString(final byte causeCode) {
    String name = causeCodeNames.get(causeCode);
    if (name == null) {
      name = "<Unknown Cause Code " + causeCode + ">";
    }
    return name;
  }

  public static String asExplanationString(final byte causeCode) {
    String explanation = causeCodeExplanations.get(causeCode);
    if (explanation == null) {
      explanation = "<Unknown Cause Code " + causeCode + " Explanation>";
    }
    return explanation;
  }

  static {
    causeCodeNames = ClassUtils.collectByteFieldNames(CauseCode.class);
    causeCodeExplanations = new HashMap<>();
    causeCodeExplanations.put(UNALLOCATED, "Unassigned/unallocated number");
    causeCodeExplanations.put(NO_ROUTE_TO_NETWORK, "No route to specified transit network");
    causeCodeExplanations.put(NO_ROUTE, "No route to destination");
    causeCodeExplanations.put(CHANNEL_UNACCEPTABLE, "Channel unacceptable");
    causeCodeExplanations.put(CALL_DELIVERED, "Call awarded and delivered");
    causeCodeExplanations.put(NORMAL_CLEARING, "Normal call clearing");
    causeCodeExplanations.put(BUSY, "User busy");
    causeCodeExplanations.put(NO_RESPONSE, "No user response");
    causeCodeExplanations.put(NO_ANSWER, "No answer");
    causeCodeExplanations.put(REJECTED, "Call rejected");
    causeCodeExplanations.put(MOVED, "Number changed");
    causeCodeExplanations.put(OUT_OF_ORDER, "Destination out of order");
    causeCodeExplanations.put(INVALID_NUMBER, "Invalid number format/incomplete number");
    causeCodeExplanations.put(FACILITY_REJECTED, "Facility rejected");
    causeCodeExplanations.put(STATUS_ENQUIRY_RSP, "Response to status enquiry");
    causeCodeExplanations.put(NORMAL, "Normal, unspecified");
    causeCodeExplanations.put(CHANNEL_CONGESTION, "No circuit/channel available");
    causeCodeExplanations.put(NET_OUT_OF_ORDER, "Network out of order");
    causeCodeExplanations.put(TEMPORARY_FAILURE, "Temporary failure");
    causeCodeExplanations.put(SWITCH_CONGESTION, "Switch congestion");
    causeCodeExplanations.put(ACCESS_INFO_DISCARDED, "Access information discarded");
    causeCodeExplanations.put(CHANNEL_UNAVAILABLE, "Requested channel not available");
    causeCodeExplanations.put(PREEMPTED, "Preempted (causes.h only)");
    causeCodeExplanations.put(NO_RESOURCE, "Resource unavailable, unspecified (Q.931 only)");
    causeCodeExplanations.put(FACILITY_NOT_SUBSCRIBED, "Facility not subscribed (causes.h only)");
    causeCodeExplanations.put(BARRED_OUT, "Outgoing call barred (causes.h only)");
    causeCodeExplanations.put(BARRED_IN, "Incoming call barred (causes.h only)");
    causeCodeExplanations.put(BEARER_CAP_NOT_AUTH, "Bearer capability not authorized");
    causeCodeExplanations.put(BEARER_CAP_NOT_AVAILABLE, "Bearer capability not available");
    causeCodeExplanations.put(SERVICE_UNAVAILABLE, "Service or option not available (Q.931 only)");
    causeCodeExplanations.put(BEARER_CAP_NOT_IMPLEMENTED, "Bearer capability not implemented");
    causeCodeExplanations.put(CHANNEL_TYPE_NOT_IMPLEMENTED, "Channel type not implemented");
    causeCodeExplanations.put(FACILITY_NOT_IMPLEMENTED, "Facility not implemented");
    causeCodeExplanations.put(RESTRICT_BEARER_CAP_AVAIL, "Only restricted digital information bearer capability is  available (Q.931 only)");
    causeCodeExplanations.put(SERVICE_NOT_IMPLEMENTED, "Service or option not available (Q.931 only)");
    causeCodeExplanations.put(INVALID_CALL_REFERENCE, "Invalid call reference");
    causeCodeExplanations.put(UNKNOWN_CHANNEL, "Identified channel does not exist (Q.931 only)");
    causeCodeExplanations.put(UNKNOWN_CALLID, "A suspended call exists, but this call identity does not (Q.931 only)");
    causeCodeExplanations.put(DUPLICATE_CALLID, "Call identity in use (Q.931 only)");
    causeCodeExplanations.put(NO_CALL_SUSPENDED, "No call suspended (Q.931 only)");
    causeCodeExplanations.put(SUSPENDED_CALL_CLEARED, "Call has been cleared (Q.931 only)");
    causeCodeExplanations.put(INCOMPATIBLE_DEST, "Incompatible destination");
    causeCodeExplanations.put(INVALID_TRANSIT_NET, "Invalid transit network selection (Q.931 only)");
    causeCodeExplanations.put(INVALID_MESSAGE, "Invalid message, unspecified");
    causeCodeExplanations.put(MISSING_MANDATORY_IE, "Mandatory information element missing (Q.931 on");
    causeCodeExplanations.put(UNKNOWN_MESSAGE, "Message type nonexistent/not implemented");
    causeCodeExplanations.put(WRONG_MESSAGE, "Message not compatible with call state");
    causeCodeExplanations.put(UNKNOWN_IE, "Information element nonexistent");
    causeCodeExplanations.put(INVALID_IE, "Invalid information element contents");
    causeCodeExplanations.put(WRONG_STATE_MESSAGE, "Message not compatible with call state");
    causeCodeExplanations.put(TIMEOUT, "Recovery on timer expiration");
    causeCodeExplanations.put(MANDATORY_IE_LEN, "Mandatory information element length error (causes.h only)");
    causeCodeExplanations.put(PROTOCOL_ERROR, "Protocol error, unspecified");
    causeCodeExplanations.put(INTERWORKING, "Internetworking, unspecified");
    causeCodeExplanations.put(_OPERATOR_DETERMINED_BARRING, "Operator determined barring");
    causeCodeExplanations.put(_FDN_MISMATCH, "FDN Mismatch");
    causeCodeExplanations.put(_PRE_EMPTION, "Pre-emption");
    causeCodeExplanations.put(_NON_SELECTED_USER_CLEARING, "Non-selected user clearing");
    causeCodeExplanations.put(_QUALITY_OF_SERVICE_UNAVAILABLE, "Quality of service unavailable");
    causeCodeExplanations.put(_INCOMING_CALLS_BARRED_WITHIN_THE_CUG, "Incoming calls barred within the CUG");
    causeCodeExplanations.put(_ACM_EQUAL_OR_GREATER_THAN_ACMMAX, "ACM equal or greater than ACMmax");
    causeCodeExplanations.put(_USER_NOT_MEMBER_OF_CUG, "User not member of CUG");
    causeCodeExplanations.put(_FDN_IS_ACTIVE_AND_NUMBER_IS_NOT_IN_FDN, "FDN is active and number is not in FDN");
    causeCodeExplanations.put(_CALL_OPERATION_NOT_ALLOWED, "Call operation not allowed");
    causeCodeExplanations.put(_CALL_BARRING_ON_OUTGOING_CALLS, "Call barring on outgoing calls");
    causeCodeExplanations.put(_CALL_BARRING_ON_INCOMING_CALLS, "Call barring on incoming calls");
    causeCodeExplanations.put(_CALL_IMPOSSIBLE, "Call impossible");
    causeCodeExplanations.put(_LOWER_LAYER_FAILURE, "Lower layer failure");
  }

}
