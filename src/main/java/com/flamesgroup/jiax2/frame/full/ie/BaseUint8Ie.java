/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.flamesgroup.jiax2.frame.full.ie;

public abstract class BaseUint8Ie implements Ie {

  private byte v_uint8;

  public BaseUint8Ie(final byte v_uint8) {
    this.v_uint8 = v_uint8;
  }

  public BaseUint8Ie() {
  }

  public byte getV_uint8() {
    return v_uint8;
  }

  public void setV_uint8(final byte v_uint8) {
    this.v_uint8 = v_uint8;
  }

  @Override
  public byte[] encode() {
    return new byte[] {v_uint8};
  }

  @Override
  public void decode(final byte[] ieData) {
    if (ieData.length != 1) {
      throw new IllegalArgumentException("IE Data Length must be 1");
    }

    v_uint8 = ieData[0];
  }

}
