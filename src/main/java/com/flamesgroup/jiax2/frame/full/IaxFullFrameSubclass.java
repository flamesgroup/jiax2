/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.flamesgroup.jiax2.frame.full;

import com.flamesgroup.jiax2.util.ClassUtils;

import java.util.Map;

public final class IaxFullFrameSubclass {

  public static final byte NEW = (byte) 0x01;
  public static final byte PING = (byte) 0x02;
  public static final byte PONG = (byte) 0x03;
  public static final byte ACK = (byte) 0x04;
  public static final byte HANGUP = (byte) 0x05;
  public static final byte REJECT = (byte) 0x06;
  public static final byte ACCEPT = (byte) 0x07;
  public static final byte AUTHREQ = (byte) 0x08;
  public static final byte AUTHREP = (byte) 0x09;
  public static final byte INVAL = (byte) 0x0a;
  public static final byte LAGRQ = (byte) 0x0b;
  public static final byte LAGRP = (byte) 0x0c;
  public static final byte REGREQ = (byte) 0x0d;
  public static final byte REGAUTH = (byte) 0x0e;
  public static final byte REGACK = (byte) 0x0f;
  public static final byte REGREJ = (byte) 0x10;
  public static final byte REGREL = (byte) 0x11;
  public static final byte VNAK = (byte) 0x12;
  public static final byte DPREQ = (byte) 0x13;
  public static final byte DPREP = (byte) 0x14;
  public static final byte DIAL = (byte) 0x15;
  public static final byte TXREQ = (byte) 0x16;
  public static final byte TXCNT = (byte) 0x17;
  public static final byte TXACC = (byte) 0x18;
  public static final byte TXREADY = (byte) 0x19;
  public static final byte TXREL = (byte) 0x1a;
  public static final byte TXREJ = (byte) 0x1b;
  public static final byte QUELCH = (byte) 0x1c;
  public static final byte UNQUELCH = (byte) 0x1d;
  public static final byte POKE = (byte) 0x1e;
  // public static final byte Reserved = (byte) 0x1f;
  public static final byte MWI = (byte) 0x20;
  public static final byte UNSUPPORT = (byte) 0x21;
  public static final byte TRANSFER = (byte) 0x22;
  // public static final byte Reserved = (byte) 0x23;
  // public static final byte Reserved = (byte) 0x24;
  // public static final byte Reserved = (byte) 0x25;
  public static final byte CALLTOKEN = (byte) 0x28;

  private static final Map<Byte, String> subclassNames;

  private IaxFullFrameSubclass() {
  }

  public static String asString(final byte subclass) {
    String name = subclassNames.get(subclass);
    if (name == null) {
      name = "<Unknown IAX Subclass " + subclass + ">";
    }
    return name;
  }

  static {
    subclassNames = ClassUtils.collectByteFieldNames(IaxFullFrameSubclass.class);
  }

}
