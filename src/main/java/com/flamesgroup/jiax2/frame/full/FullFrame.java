/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.flamesgroup.jiax2.frame.full;

import com.flamesgroup.jiax2.frame.SingularFrame;
import com.flamesgroup.jiax2.util.DataUtils;

import java.nio.ByteBuffer;
import java.util.Objects;

public final class FullFrame extends SingularFrame {

  private boolean retransmitted;
  private short destinationCallNumber_uint16;
  private int timestamp_uint32;
  private byte oSeqno_uint8;
  private byte iSeqno_uint8;
  private byte frameType_uint8;
  private byte subclass_uint8;
  private byte[] data = DataUtils.EMPTY_DATA;

  public boolean isRetransmitted() {
    return retransmitted;
  }

  public void setRetransmitted(final boolean retransmitted) {
    this.retransmitted = retransmitted;
  }

  public short getDestinationCallNumber_uint16() {
    return destinationCallNumber_uint16;
  }

  public void setDestinationCallNumber_uint16(final short destinationCallNumber_uint16) {
    if (Integer.compareUnsigned(destinationCallNumber_uint16, 0x7FFF) > 0) {
      throw new IllegalArgumentException("Destination Call Number is overflow 15-bit value");
    }
    this.destinationCallNumber_uint16 = destinationCallNumber_uint16;
  }

  public int getTimestamp_uint32() {
    return timestamp_uint32;
  }

  public void setTimestamp_uint32(final int timestamp_uint32) {
    this.timestamp_uint32 = timestamp_uint32;
  }

  public byte getOSeqno_uint8() {
    return oSeqno_uint8;
  }

  public void setOSeqno_uint8(final byte oSeqno_uint8) {
    this.oSeqno_uint8 = oSeqno_uint8;
  }

  public byte getISeqno_uint8() {
    return iSeqno_uint8;
  }

  public void setISeqno_uint8(final byte iSeqno_uint8) {
    this.iSeqno_uint8 = iSeqno_uint8;
  }

  public byte getFrameType_uint8() {
    return frameType_uint8;
  }

  public void setFrameType_uint8(final byte frameType_uint8) {
    this.frameType_uint8 = frameType_uint8;
  }

  public byte getSubclass_uint8() {
    return subclass_uint8;
  }

  public void setSubclass_uint8(final byte subclass_uint8) {
    this.subclass_uint8 = subclass_uint8;
  }

  public long getUncompressedSubclass_uint64() {
    return FullFrameUtils.uncompressSubclass_uint64(subclass_uint8);
  }

  public void setUncompressedSubclass_uint64(final long uncompressedSubclass_uint64) {
    this.subclass_uint8 = FullFrameUtils.compressSubclass_uint8(uncompressedSubclass_uint64);
  }

  public byte[] getData() {
    return data;
  }

  public void setData(final byte[] data) {
    Objects.requireNonNull(data);
    this.data = data;
  }

  @Override
  public final void encodeTo(final ByteBuffer bb) {
    bb.putShort((short) (getSourceCallNumber_uint16() | 0x8000));
    if (isRetransmitted()) {
      bb.putShort((short) (getDestinationCallNumber_uint16() | 0x8000));
    } else {
      bb.putShort((short) (getDestinationCallNumber_uint16() | 0x0000));
    }
    bb.putInt(getTimestamp_uint32());
    bb.put(getOSeqno_uint8());
    bb.put(getISeqno_uint8());
    bb.put(getFrameType_uint8());
    bb.put(getSubclass_uint8());
    bb.put(data);
  }

  @Override
  public void decodeFrom(final ByteBuffer bb) {
    short sourceCallNumber_uint16;
    boolean retransmitted;
    short destinationCallNumber_uint16;
    int timestamp_uint32;
    byte oSeqno_uint8;
    byte iSeqno_uint8;
    byte frameType_uint8;
    byte subclass_uint8;
    byte data[];

    {
      short uint16 = bb.getShort();
      boolean fullFrame = (uint16 & 0x8000) != 0;
      if (!fullFrame) {
        throw new IllegalArgumentException("Full Frame bit is not set to 1");
      }
      sourceCallNumber_uint16 = (short) (uint16 & 0x7FFF);
    }
    {
      short uint16 = bb.getShort();
      retransmitted = (uint16 & 0x8000) != 0;
      destinationCallNumber_uint16 = (short) (uint16 & 0x7FFF);
    }
    timestamp_uint32 = bb.getInt();
    oSeqno_uint8 = bb.get();
    iSeqno_uint8 = bb.get();
    frameType_uint8 = bb.get();
    subclass_uint8 = bb.get();
    data = DataUtils.remaining2array(bb);

    setSourceCallNumber_uint16(sourceCallNumber_uint16);
    setRetransmitted(retransmitted);
    setDestinationCallNumber_uint16(destinationCallNumber_uint16);
    setTimestamp_uint32(timestamp_uint32);
    setOSeqno_uint8(oSeqno_uint8);
    setISeqno_uint8(iSeqno_uint8);
    setFrameType_uint8(frameType_uint8);
    setSubclass_uint8(subclass_uint8);
    setData(data);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append(getClass().getSimpleName());
    sb.append("@");
    sb.append(Integer.toHexString(hashCode()));
    sb.append("[");
    sb.append(FullFrameType.asString(getFrameType_uint8()));
    sb.append(":");
    switch (frameType_uint8) {
      case FullFrameType.IAX:
        sb.append(IaxFullFrameSubclass.asString(getSubclass_uint8()));
        break;
      case FullFrameType.CONTROL:
        sb.append(ControlFullFrameSubclass.asString(getSubclass_uint8()));
        break;
      case FullFrameType.VOICE:
      case FullFrameType.VIDEO:
        sb.append(MediaFormat.asString(getUncompressedSubclass_uint64()));
        break;
      case FullFrameType.DTMF:
        sb.append((char) getSubclass_uint8());
        break;
    }
    sb.append("]");
    return sb.toString();
  }
}
