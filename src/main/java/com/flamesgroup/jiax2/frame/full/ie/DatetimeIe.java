/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.flamesgroup.jiax2.frame.full.ie;

import java.time.LocalDateTime;

public class DatetimeIe extends BaseUint32Ie {

  public LocalDateTime getV() {
    int v_uint32 = getV_uint32();
    int year = 2000 + ((v_uint32 >> 25) & 0b0111_1111);
    int month = (v_uint32 >> 21) & 0b000_1111;
    int day = (v_uint32 >> 16) & 0b001_1111;
    int hours = (v_uint32 >> 11) & 0b001_1111;
    int minutes = (v_uint32 >> 5) & 0b011_1111;
    int seconds = ((v_uint32 >> 0) & 0b001_1111) << 1;
    return LocalDateTime.of(year, month, day, hours, minutes, seconds);
  }

  public void setV(final LocalDateTime v) {
    int v_uint32 = 0;
    v_uint32 |= (v.getYear() - 2000) << 25;
    v_uint32 |= v.getMonthValue() << 21;
    v_uint32 |= v.getDayOfMonth() << 16;
    v_uint32 |= v.getHour() << 11;
    v_uint32 |= v.getMinute() << 5;
    v_uint32 |= (v.getSecond() >> 1) << 0;
    setV_uint32(v_uint32);
  }

  @Override
  public IeType getType() {
    return IeType.DATETIME;
  }

}
