/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.flamesgroup.jiax2.frame.full;

import com.flamesgroup.jiax2.util.Converter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;

public class FullFrameUtils {

  final static Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

  private FullFrameUtils() {
  }

  public static byte compressSubclass_uint8(final long uncompressedSubclass_uint64) {
    int unsignedCompareResult = Long.compareUnsigned(uncompressedSubclass_uint64, 0b1000_0000L);
    if (unsignedCompareResult < 0) {
      return (byte) uncompressedSubclass_uint64;
    }
    if (unsignedCompareResult == 0) {
      return (byte) (0b1000_0000 | 7);
    }
    // No need to start from zero, we already know it's >= 2^8
    long v = 0x100;
    for (int i = 8; i < 64; i++) {
      if (v == uncompressedSubclass_uint64) {
        return (byte) (i | 0b1000_0000);
      }
      v <<= 1;
    }
    logger.warn("Can't compress uncompressed subclass [{}]", Converter.longToHex(uncompressedSubclass_uint64));
    return 0; // such return value inspired by YATE project IAX implementation
  }

  public static long uncompressSubclass_uint64(byte compressedSubclass_uint8) {
    if ((compressedSubclass_uint8 & 0b1000_0000) == 0) {
      return Byte.toUnsignedLong(compressedSubclass_uint8);
    } else {
      // special case for 'compressed' -1
      if (compressedSubclass_uint8 == (byte) 0xFF) {
        return -1L;
      } else {
        compressedSubclass_uint8 &= ~0b1000_0000;
        if (compressedSubclass_uint8 < 64) {
          return 1L << compressedSubclass_uint8;
        } else {
          logger.warn("Can't uncompress compressed subclass [{}]", Converter.byteToHex(compressedSubclass_uint8));
          return 0L; // such return value inspired by YATE project IAX implementation
        }
      }
    }
  }

}
