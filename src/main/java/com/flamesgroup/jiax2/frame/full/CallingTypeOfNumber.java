/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.flamesgroup.jiax2.frame.full;

import com.flamesgroup.jiax2.util.ClassUtils;

import java.util.HashMap;
import java.util.Map;

public final class CallingTypeOfNumber {

  public static final byte UNKNOWN = (byte) 0x00;
  public static final byte INTERNATIONAL = (byte) 0x10;
  public static final byte NATIONAL = (byte) 0x20;
  public static final byte NETWORK_SPECIFIC = (byte) 0x30;
  public static final byte SUBSCRIBER = (byte) 0x40;
  public static final byte ABBREVIATED = (byte) 0x60;
  public static final byte RESERVED = (byte) 0x70;

  private static final Map<Byte, String> callingTypeOfNumberNames;
  private static final Map<Byte, String> callingTypeOfNumberExplanations;

  private CallingTypeOfNumber() {
  }


  public static String asString(final byte callingTypeOfNumber) {
    String name = callingTypeOfNumberNames.get(callingTypeOfNumber);
    if (name == null) {
      name = "<Unknown Calling Type Of Number " + callingTypeOfNumber + ">";
    }
    return name;
  }

  public static String asExplanationString(final byte callingTypeOfNumber) {
    String explanation = callingTypeOfNumberExplanations.get(callingTypeOfNumber);
    if (explanation == null) {
      explanation = "<Unknown Calling Type Of Number " + callingTypeOfNumber + " Explanation>";
    }
    return explanation;
  }

  static {
    callingTypeOfNumberNames = ClassUtils.collectByteFieldNames(FullFrameType.class);
    callingTypeOfNumberExplanations = new HashMap<>();
    callingTypeOfNumberExplanations.put(UNKNOWN, "Unknown");
    callingTypeOfNumberExplanations.put(INTERNATIONAL, "International Number");
    callingTypeOfNumberExplanations.put(NATIONAL, "National Number");
    callingTypeOfNumberExplanations.put(NETWORK_SPECIFIC, "Network Specific Number");
    callingTypeOfNumberExplanations.put(SUBSCRIBER, "Subscriber Number");
    callingTypeOfNumberExplanations.put(ABBREVIATED, "Abbreviated Number");
    callingTypeOfNumberExplanations.put(RESERVED, "Reserved for extension");
  }

}
