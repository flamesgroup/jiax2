/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.flamesgroup.jiax2.frame.full.ie;

import com.flamesgroup.jiax2.util.DataUtils;

import java.util.Objects;

/**
 * This class implement base raw IE.
 * <p>
 * Notice that current implementation, for performance reason, use reference
 * to IE value and do not perform any value copy (what would be safer in general).
 */
public abstract class BaseRawIe implements Ie {

  private byte[] v = DataUtils.EMPTY_DATA;

  public byte[] getV() {
    return v;
  }

  public void setV(final byte[] v) {
    Objects.nonNull(v);
    this.v = v;
  }

  @Override
  public byte[] encode() {
    return v;
  }

  @Override
  public void decode(final byte[] ieData) {
    v = ieData;
  }

}
