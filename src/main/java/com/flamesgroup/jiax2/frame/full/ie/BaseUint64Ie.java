/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.flamesgroup.jiax2.frame.full.ie;

public abstract class BaseUint64Ie implements Ie {

  private long v_uint64;

  public long getV_uint64() {
    return v_uint64;
  }

  public void setV_uint64(final long v_uint64) {
    this.v_uint64 = v_uint64;
  }

  @Override
  public byte[] encode() {
    return new byte[] {(byte) (v_uint64 >> 56),
        (byte) (v_uint64 >> 48),
        (byte) (v_uint64 >> 40),
        (byte) (v_uint64 >> 32),
        (byte) (v_uint64 >> 24),
        (byte) (v_uint64 >> 16),
        (byte) (v_uint64 >> 8),
        (byte) (v_uint64)
    };
  }

  @Override
  public void decode(final byte[] ieData) {
    if (ieData.length < 1) {
      throw new IllegalArgumentException("IE Data Length mustn't be less than 1");
    }
    if (ieData[0] != 0) { // version 0
      throw new IllegalArgumentException("Unknown version " + ieData[0]);
    }

    v_uint64 = ((long) (ieData[1] & 0xFF) << 56) |
        ((long) (ieData[2] & 0xFF) << 48) |
        ((long) (ieData[3] & 0xFF) << 40) |
        ((long) (ieData[4] & 0xFF) << 32) |
        ((long) (ieData[5] & 0xFF) << 24) |
        ((long) (ieData[6] & 0xFF) << 16) |
        ((long) (ieData[7] & 0xFF) << 8) |
        ((long) (ieData[8] & 0xFF));
  }

}
