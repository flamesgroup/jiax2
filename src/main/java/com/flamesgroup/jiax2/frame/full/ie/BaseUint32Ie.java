/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.flamesgroup.jiax2.frame.full.ie;

public abstract class BaseUint32Ie implements Ie {

  private int v_uint32;

  public BaseUint32Ie(final int v_uint32) {
    this.v_uint32 = v_uint32;
  }

  public BaseUint32Ie() {
  }

  public int getV_uint32() {
    return v_uint32;
  }

  public void setV_uint32(final int v_uint32) {
    this.v_uint32 = v_uint32;
  }

  @Override
  public byte[] encode() {
    return new byte[] {(byte) (v_uint32 >> 24), (byte) (v_uint32 >> 16), (byte) (v_uint32 >> 8), (byte) (v_uint32)};
  }

  @Override
  public void decode(final byte[] ieData) {
    if (ieData.length != 4) {
      throw new IllegalArgumentException("IE Data Length must be 4");
    }

    v_uint32 = (ieData[0] & 0xFF) << 24 | (ieData[1] & 0xFF) << 16 | (ieData[2] & 0xFF) << 8 | (ieData[3] & 0xFF);
  }

}
