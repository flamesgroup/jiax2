/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.flamesgroup.jiax2.frame.full.ie;

import com.flamesgroup.jiax2.util.DataUtils;

import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.Objects;

public class ApparentAddrIe implements Ie {

  private InetSocketAddress v;

  public InetSocketAddress getV() {
    return v;
  }

  public void setV(final InetSocketAddress v) {
    Objects.nonNull(v);
    this.v = v;
  }

  @Override
  public IeType getType() {
    return IeType.APPARENT_ADDR;
  }

  @Override
  public byte[] encode() {
    int port = v.getPort();
    InetAddress inetAddress = v.getAddress();

    if (inetAddress instanceof Inet4Address) {
      Inet4Address inet4Address = (Inet4Address) inetAddress;
      final byte[] ieData = new byte[0x10];
      {
        final ByteBuffer bb = ByteBuffer.wrap(ieData);
        bb.putShort((short) 0x0200);
        bb.putShort((short) port);
        bb.put(inet4Address.getAddress());
      }
      return ieData;
    } else if (inetAddress instanceof Inet6Address) {
      Inet6Address inet6Address = (Inet6Address) inetAddress;
      final byte[] ieData = new byte[0x1C];
      {
        final ByteBuffer bb = ByteBuffer.wrap(ieData);
        bb.putShort((short) 0x0A00);
        bb.putShort((short) port);
        bb.putInt(0);
        bb.put(inet6Address.getAddress());
        bb.putInt(inet6Address.getScopeId());
      }
      return ieData;
    } else {
      throw new UnsupportedOperationException();
    }
  }

  @Override
  public void decode(final byte[] ieData) {
    if (ieData.length == 0x10) { // IPv4 address
      final ByteBuffer bb = ByteBuffer.wrap(ieData);

      bb.getShort(); // skip - this value must be 0x0200 (but on OS X it unfortunately have value 0x0201)
      int port = Short.toUnsignedInt(bb.getShort());
      byte[] ipData = DataUtils.length2array(bb, 4);
      InetAddress inetAddress;
      try {
        inetAddress = Inet4Address.getByAddress(ipData);
      } catch (UnknownHostException e) {
        throw new AssertionError(e);
      }
      v = new InetSocketAddress(inetAddress, port);
    } else if (ieData.length == 0x1C) { // IPv6 address
      final ByteBuffer bb = ByteBuffer.wrap(ieData);
      bb.getShort(); // skip - this value must be 0x0A00
      int port = Short.toUnsignedInt(bb.getShort());
      int flowInformation = bb.getInt();
      byte[] ipData = DataUtils.length2array(bb, 16);
      int scopeId = bb.getInt();
      InetAddress inetAddress;
      try {
        inetAddress = Inet6Address.getByAddress(null, ipData, scopeId);
      } catch (UnknownHostException e) {
        throw new AssertionError(e);
      }
      v = new InetSocketAddress(inetAddress, port);
    } else {
      throw new UnsupportedOperationException();
    }
  }

}
