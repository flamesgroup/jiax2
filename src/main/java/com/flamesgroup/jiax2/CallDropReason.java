/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.flamesgroup.jiax2;

import com.flamesgroup.jiax2.frame.full.CauseCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;
import java.util.HashMap;
import java.util.Map;

public enum CallDropReason {

  UNDEFINED((byte) 0),
  UNALLOCATED(CauseCode.UNALLOCATED),
  NO_ROUTE_TO_NETWORK(CauseCode.NO_ROUTE_TO_NETWORK),
  NO_ROUTE(CauseCode.NO_ROUTE),
  CHANNEL_UNACCEPTABLE(CauseCode.CHANNEL_UNACCEPTABLE),
  CALL_DELIVERED(CauseCode.CALL_DELIVERED),
  NORMAL_CLEARING(CauseCode.NORMAL_CLEARING),
  BUSY(CauseCode.BUSY),
  NO_RESPONSE(CauseCode.NO_RESPONSE),
  NO_ANSWER(CauseCode.NO_ANSWER),
  REJECTED(CauseCode.REJECTED),
  MOVED(CauseCode.MOVED),
  OUT_OF_ORDER(CauseCode.OUT_OF_ORDER),
  INVALID_NUMBER(CauseCode.INVALID_NUMBER),
  FACILITY_REJECTED(CauseCode.FACILITY_REJECTED),
  STATUS_ENQUIRY_RSP(CauseCode.STATUS_ENQUIRY_RSP),
  NORMAL(CauseCode.NORMAL),
  CHANNEL_CONGESTION(CauseCode.CHANNEL_CONGESTION),
  NET_OUT_OF_ORDER(CauseCode.NET_OUT_OF_ORDER),
  TEMPORARY_FAILURE(CauseCode.TEMPORARY_FAILURE),
  SWITCH_CONGESTION(CauseCode.SWITCH_CONGESTION),
  ACCESS_INFO_DISCARDED(CauseCode.ACCESS_INFO_DISCARDED),
  CHANNEL_UNAVAILABLE(CauseCode.CHANNEL_UNAVAILABLE),
  PREEMPTED(CauseCode.PREEMPTED),
  NO_RESOURCE(CauseCode.NO_RESOURCE),
  FACILITY_NOT_SUBSCRIBED(CauseCode.FACILITY_NOT_SUBSCRIBED),
  BARRED_OUT(CauseCode.BARRED_OUT),
  BARRED_IN(CauseCode.BARRED_IN),
  BEARER_CAP_NOT_AUTH(CauseCode.BEARER_CAP_NOT_AUTH),
  BEARER_CAP_NOT_AVAILABLE(CauseCode.BEARER_CAP_NOT_AVAILABLE),
  SERVICE_UNAVAILABLE(CauseCode.SERVICE_UNAVAILABLE),
  BEARER_CAP_NOT_IMPLEMENTED(CauseCode.BEARER_CAP_NOT_IMPLEMENTED),
  CHANNEL_TYPE_NOT_IMPLEMENTED(CauseCode.CHANNEL_TYPE_NOT_IMPLEMENTED),
  FACILITY_NOT_IMPLEMENTED(CauseCode.FACILITY_NOT_IMPLEMENTED),
  RESTRICT_BEARER_CAP_AVAIL(CauseCode.RESTRICT_BEARER_CAP_AVAIL),
  SERVICE_NOT_IMPLEMENTED(CauseCode.SERVICE_NOT_IMPLEMENTED),
  INVALID_CALL_REFERENCE(CauseCode.INVALID_CALL_REFERENCE),
  UNKNOWN_CHANNEL(CauseCode.UNKNOWN_CHANNEL),
  UNKNOWN_CALLID(CauseCode.UNKNOWN_CALLID),
  DUPLICATE_CALLID(CauseCode.DUPLICATE_CALLID),
  NO_CALL_SUSPENDED(CauseCode.NO_CALL_SUSPENDED),
  SUSPENDED_CALL_CLEARED(CauseCode.SUSPENDED_CALL_CLEARED),
  INCOMPATIBLE_DEST(CauseCode.INCOMPATIBLE_DEST),
  INVALID_TRANSIT_NET(CauseCode.INVALID_TRANSIT_NET),
  INVALID_MESSAGE(CauseCode.INVALID_MESSAGE),
  MISSING_MANDATORY_IE(CauseCode.MISSING_MANDATORY_IE),
  UNKNOWN_MESSAGE(CauseCode.UNKNOWN_MESSAGE),
  WRONG_MESSAGE(CauseCode.WRONG_MESSAGE),
  UNKNOWN_IE(CauseCode.UNKNOWN_IE),
  INVALID_IE(CauseCode.INVALID_IE),
  WRONG_STATE_MESSAGE(CauseCode.WRONG_STATE_MESSAGE),
  TIMEOUT(CauseCode.TIMEOUT),
  MANDATORY_IE_LEN(CauseCode.MANDATORY_IE_LEN),
  PROTOCOL_ERROR(CauseCode.PROTOCOL_ERROR),
  INTERWORKING(CauseCode.INTERWORKING),
  _OPERATOR_DETERMINED_BARRING(CauseCode._OPERATOR_DETERMINED_BARRING),
  _FDN_MISMATCH(CauseCode._FDN_MISMATCH),
  _PRE_EMPTION(CauseCode._PRE_EMPTION),
  _NON_SELECTED_USER_CLEARING(CauseCode._NON_SELECTED_USER_CLEARING),
  _QUALITY_OF_SERVICE_UNAVAILABLE(CauseCode._QUALITY_OF_SERVICE_UNAVAILABLE),
  _INCOMING_CALLS_BARRED_WITHIN_THE_CUG(CauseCode._INCOMING_CALLS_BARRED_WITHIN_THE_CUG),
  _ACM_EQUAL_OR_GREATER_THAN_ACMMAX(CauseCode._ACM_EQUAL_OR_GREATER_THAN_ACMMAX),
  _USER_NOT_MEMBER_OF_CUG(CauseCode._USER_NOT_MEMBER_OF_CUG),
  _FDN_IS_ACTIVE_AND_NUMBER_IS_NOT_IN_FDN(CauseCode._FDN_IS_ACTIVE_AND_NUMBER_IS_NOT_IN_FDN),
  _CALL_OPERATION_NOT_ALLOWED(CauseCode._CALL_OPERATION_NOT_ALLOWED),
  _CALL_BARRING_ON_OUTGOING_CALLS(CauseCode._CALL_BARRING_ON_OUTGOING_CALLS),
  _CALL_BARRING_ON_INCOMING_CALLS(CauseCode._CALL_BARRING_ON_INCOMING_CALLS),
  _CALL_IMPOSSIBLE(CauseCode._CALL_IMPOSSIBLE),
  _LOWER_LAYER_FAILURE(CauseCode._LOWER_LAYER_FAILURE);

  final static Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

  private static final Map<Byte, CallDropReason> causeCode2CallDropReason;

  private final byte causeCode;

  CallDropReason(final byte causeCode) {
    this.causeCode = causeCode;
  }

  public byte getCauseCode() {
    return causeCode;
  }

  public static CallDropReason fromCauseCode(final byte causeCode) {
    CallDropReason callDropReason = causeCode2CallDropReason.get(causeCode);
    if (callDropReason != null) {
      return callDropReason;
    } else {
      logger.warn("Can't map cause code [{}] to call drop reason - return [{}]", Byte.toUnsignedInt(causeCode), UNDEFINED);
      return UNDEFINED;
    }
  }

  static {
    causeCode2CallDropReason = new HashMap<>();
    for (CallDropReason callDropReason : values()) {
      causeCode2CallDropReason.put(callDropReason.getCauseCode(), callDropReason);
    }
  }

}
