/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.flamesgroup.jiax2;

import java.io.IOException;
import java.net.SocketAddress;

public interface IIaxPeer {

  boolean isUp();

  void startup(INewTerminatingCallHandler handler) throws IOException, InterruptedException;

  void shutdown() throws IOException, InterruptedException;

  IOriginatingCall newCall(SocketAddress target, String called, String caller, String callerName, IOriginatingCallHandler originationCallHandler);

  IOriginatingCall newCall(SocketAddress target, String called, String calledContext, String caller, String callerName, IOriginatingCallHandler originationCallHandler);

}
