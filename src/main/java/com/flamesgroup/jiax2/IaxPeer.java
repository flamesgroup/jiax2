/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.flamesgroup.jiax2;

import com.flamesgroup.jiax2.engine.ClosedIaxEngineException;
import com.flamesgroup.jiax2.engine.IaxEngine;
import com.flamesgroup.jiax2.engine.SessionId;
import com.flamesgroup.jiax2.event.AcceptControlEvent;
import com.flamesgroup.jiax2.event.AnswerControlEvent;
import com.flamesgroup.jiax2.event.AudioMediaEvent;
import com.flamesgroup.jiax2.event.ControlEvent;
import com.flamesgroup.jiax2.event.DtmfControlEvent;
import com.flamesgroup.jiax2.event.HangupControlEvent;
import com.flamesgroup.jiax2.event.InternalErrorControlEvent;
import com.flamesgroup.jiax2.event.MediaEvent;
import com.flamesgroup.jiax2.event.NewControlEvent;
import com.flamesgroup.jiax2.event.RejectControlEvent;
import com.flamesgroup.jiax2.event.RingingControlEvent;
import com.flamesgroup.jiax2.frame.full.CauseCode;
import com.flamesgroup.media.codec.audio.IAudioCodecFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.net.SocketAddress;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class IaxPeer implements IIaxPeer {

  final static Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

  private final SocketAddress bindSocketAddress;
  private final AudioOptions audioOptions;
  private final IAudioCodecFactory audioCodecFactory;
  private final Map<SocketAddress, TrunkOptions> trunkOptionsMap;

  private final Lock peerLock = new ReentrantLock();

  private INewTerminatingCallHandler newTerminatingCallHandler;
  private IaxEngine iaxEngine;

  private final Thread[] controlThreads = new Thread[1];
  private final Thread[] mediaThreads = new Thread[1];

  public IaxPeer(final SocketAddress bindSocketAddress, final AudioOptions audioOptions, final IAudioCodecFactory audioCodecFactory, final Map<SocketAddress, TrunkOptions> trunkOptionsMap) {
    Objects.requireNonNull(bindSocketAddress, "bindSocketAddress mustn't be null");
    Objects.requireNonNull(audioOptions, "audioOptions mustn't be null");
    Objects.requireNonNull(audioCodecFactory, "audioCodecFactory mustn't be null");
    Objects.requireNonNull(trunkOptionsMap, "trunkOptionsMap mustn't be null");

    this.bindSocketAddress = bindSocketAddress;
    this.audioOptions = audioOptions;
    this.audioCodecFactory = audioCodecFactory;
    this.trunkOptionsMap = trunkOptionsMap;
  }

  @Override
  public boolean isUp() {
    return isUpLocal(getIaxEngineLocal());
  }

  @Override
  public void startup(final INewTerminatingCallHandler handler) throws IOException, InterruptedException {
    Objects.requireNonNull(handler);

    peerLock.lock();
    try {
      if (iaxEngine != null) {
        throw new IllegalStateException("IaxPeer has already started up");
      }

      logger.debug("[{}] - startuping", this);

      this.newTerminatingCallHandler = handler;

      iaxEngine = new IaxEngine(trunkOptionsMap);

      for (int i = 0; i < controlThreads.length; i++) {
        Thread controlThread = new Thread(new ControlWorker(), "IaxPeerControl-" + i);
        controlThread.setDaemon(true);
        controlThread.start();
        controlThreads[i] = controlThread;
      }
      for (int i = 0; i < mediaThreads.length; i++) {
        Thread mediaThread = new Thread(new MediaWorker(), "IaxPeerMedia-" + i);
        mediaThread.setDaemon(true);
        mediaThread.start();
        mediaThreads[i] = mediaThread;
      }

      iaxEngine.startup(bindSocketAddress);

      logger.debug("[{}] - startuped", this);
    } finally {
      peerLock.unlock();
    }
  }

  @Override
  public void shutdown() throws IOException, InterruptedException {
    peerLock.lock();
    try {
      if (iaxEngine == null) {
        logger.info("[{}] - already shutdowned", this);
        return;
      }

      logger.debug("[{}] - shutdowning", this);

      iaxEngine.shutdown();

      for (Thread controlThread : controlThreads) {
        controlThread.join();
      }
      for (Thread mediaThread : mediaThreads) {
        mediaThread.join();
      }

      iaxEngine = null;

      newTerminatingCallHandler = null;

      logger.debug("[{}] - shutdowned", this);
    } finally {
      peerLock.unlock();
    }
  }

  @Override
  public IOriginatingCall newCall(final SocketAddress target, final String called, final String caller, final String callerName, final IOriginatingCallHandler originationCallHandler) {
    return this.newCall(target, called, null, caller, callerName, originationCallHandler);
  }

  @Override
  public IOriginatingCall newCall(final SocketAddress target, final String called, final String calledContext, final String caller, final String callerName, final IOriginatingCallHandler originationCallHandler) {
    Objects.requireNonNull(originationCallHandler);

    peerLock.lock();
    try {
      if (!isUpLocal(iaxEngine)) {
        throw new IllegalStateException("IaxPeer must be started up");
      }

      long mediaFormat = audioOptions.getDefaultAudioMediaFormatOption().getAudioMediaFormat().getV_uint64();
      long capability = 0;
      for (AudioMediaFormatOption mediaFormatOption : audioOptions.getAudioMediaFormatOptions()) {
        capability |= mediaFormatOption.getAudioMediaFormat().getV_uint64();
      }
      NewControlEvent newControlEvent = new NewControlEvent(SessionId.generateSessionId(), target, called, caller, calledContext, mediaFormat, capability);

      putControlEvent(newControlEvent);

      int countOfAudioFrames = audioOptions.getDefaultAudioMediaFormatOption().getAudioMediaFrameCount();
      IaxCall call = new IaxCall(
          IaxPeer.this, newControlEvent.getSessionId(), newControlEvent.getCallingNumber(), newControlEvent.getCalledNumber(), newControlEvent.getCalledContext(), audioCodecFactory, newControlEvent.getFormat(), countOfAudioFrames);

      iaxCallMap.put(call.getSessionId(), call);

      call.initAsNewOriginationCall(originationCallHandler);
      return call;
    } finally {
      peerLock.unlock();
    }
  }

  private final ConcurrentHashMap<SessionId, IaxCall> iaxCallMap = new ConcurrentHashMap<>();

  private void putControlEvent(final ControlEvent controlEvent) {
    IaxEngine iaxEngineLocal = getIaxEngineLocal();
    if (!isUpLocal(iaxEngineLocal)) {
      logger.info("[{}] - can't put [{}] because IaxPeer shutdown", this, controlEvent);
      return;
    }

    try {
      iaxEngineLocal.putControlEvent(controlEvent);
    } catch (InterruptedException e) {
      Thread.currentThread().interrupt();
      logger.debug("[{}] - can't put [{}] because current thread [{}] interrupted", this, controlEvent, Thread.currentThread());
    }
  }

  void processHangup(final IaxCall iaxCall, final byte causeCode) {
    iaxCallMap.remove(iaxCall.getSessionId());

    HangupControlEvent hangupControlEvent = new HangupControlEvent(iaxCall.getSessionId(), null, causeCode);
    putControlEvent(hangupControlEvent);
  }

  void processDtmf(final IaxCall iaxCall, final char dtmf) {
    DtmfControlEvent dtmfControlEvent = new DtmfControlEvent(iaxCall.getSessionId(), dtmf);
    putControlEvent(dtmfControlEvent);
  }

  void processAccept(final IaxCall iaxCall) {
    AcceptControlEvent acceptControlEvent = new AcceptControlEvent(iaxCall.getSessionId(), iaxCall.getMediaFormat());
    putControlEvent(acceptControlEvent);
  }

  void processRinging(final IaxCall iaxCall) {
    RingingControlEvent ringingControlEvent = new RingingControlEvent(iaxCall.getSessionId());
    putControlEvent(ringingControlEvent);
  }

  void processAnswer(final IaxCall iaxCall) {
    AnswerControlEvent answerControlEvent = new AnswerControlEvent(iaxCall.getSessionId());
    putControlEvent(answerControlEvent);
  }

  void processMedia(final IaxCall iaxCall, final int timestamp, final byte[] data) {
    IaxEngine iaxEngineLocal = getIaxEngineLocal();
    if (!isUpLocal(iaxEngineLocal)) {
      logger.info("[{}] - can't process media for [{}] with timestamp [{}] because IaxPeer shutdown", this, iaxCall, timestamp);
      return;
    }

    AudioMediaEvent audioMediaEvent = new AudioMediaEvent(iaxCall.getSessionId(), iaxCall.getMediaFormat(), timestamp, data);
    try {
      iaxEngineLocal.putMediaEvent(audioMediaEvent);
    } catch (InterruptedException e) {
      Thread.currentThread().interrupt();
      logger.debug("[{}] - can't process [{}] because current thread [{}] interrupted", this, audioMediaEvent, Thread.currentThread());
    }
  }

  @Override
  public String toString() {
    return getClass().getSimpleName() + "@" + Integer.toHexString(hashCode());
  }

  private IaxEngine getIaxEngineLocal() {
    peerLock.lock();
    try {
      return iaxEngine;
    } finally {
      peerLock.unlock();
    }
  }

  private boolean isUpLocal(final IaxEngine iaxEngine) {
    return iaxEngine != null;
  }

  private final class ControlWorker implements Runnable {

    @Override
    public void run() {
      logger.debug("[{}] - begin executing", this);

      IaxEngine iaxEngineLocal = getIaxEngineLocal();
      while (true) {
        try {
          ControlEvent controlEvent = iaxEngineLocal.takeControlEvent();

          if (controlEvent instanceof NewControlEvent) {
            NewControlEvent newControlEvent = (NewControlEvent) controlEvent;

            // check on required Information Elements
            if (newControlEvent.getCalledNumber() == null || newControlEvent.getFormat() == null) {
              putControlEvent(new RejectControlEvent(newControlEvent.getSessionId(), "Required IE(s) missed", CauseCode.MISSING_MANDATORY_IE));
              continue;
            }

            // check on conditional Information Elements
            long capability;
            if (newControlEvent.getCapability() == null) {
              capability = newControlEvent.getFormat();
            } else {
              capability = newControlEvent.getCapability();
            }

            long mediaFormat = 0;
            int mediaFrameCount = 0;
            for (AudioMediaFormatOption mediaFormatOption : audioOptions.getAudioMediaFormatOptions()) {
              if ((capability & mediaFormatOption.getAudioMediaFormat().getV_uint64()) == mediaFormatOption.getAudioMediaFormat().getV_uint64()) {
                mediaFormat = mediaFormatOption.getAudioMediaFormat().getV_uint64();
                mediaFrameCount = mediaFormatOption.getAudioMediaFrameCount();
                break;
              }
            }
            if (mediaFormat == 0) {
              putControlEvent(new RejectControlEvent(newControlEvent.getSessionId(), "Can't negotiate codec", CauseCode.BEARER_CAP_NOT_AVAILABLE));
              continue;
            }

            IaxCall call = new IaxCall(
                IaxPeer.this, newControlEvent.getSessionId(), newControlEvent.getCallingNumber(), newControlEvent.getCalledNumber(), newControlEvent.getCalledContext(), audioCodecFactory, mediaFormat, mediaFrameCount);

            iaxCallMap.put(call.getSessionId(), call);

            call.initAsNewTerminationCall(newTerminatingCallHandler);
          } else if (controlEvent instanceof HangupControlEvent) {
            HangupControlEvent hangupControlEvent = (HangupControlEvent) controlEvent;
            IaxCall call = iaxCallMap.remove(hangupControlEvent.getSessionId());
            if (call == null) {
              logger.warn("[{}] - [{}] unprocessed, because call already hangup", this, hangupControlEvent);
            } else {
              call.handleHangup(hangupControlEvent.getCauseCode());
            }
          } else if (controlEvent instanceof InternalErrorControlEvent) {
            InternalErrorControlEvent internalErrorControlEvent = (InternalErrorControlEvent) controlEvent;
            IaxCall call = iaxCallMap.remove(internalErrorControlEvent.getSessionId());
            if (call == null) {
              logger.warn("[{}] - [{}] unprocessed, because call already hangup", this, internalErrorControlEvent);
            } else {
              call.handleHangup(null);
            }
          } else {
            IaxCall call = iaxCallMap.get(controlEvent.getSessionId());
            if (call == null) {
              logger.warn("[{}] - [{}] unprocessed, because call already hangup", this, controlEvent);
            } else {
              if (controlEvent instanceof RingingControlEvent) {
                call.handleRinging();
              } else if (controlEvent instanceof AnswerControlEvent) {
                call.handleAnswer();
              } else if (controlEvent instanceof AcceptControlEvent) {
                AcceptControlEvent acceptControlEvent = (AcceptControlEvent) controlEvent;
                if (acceptControlEvent.getFormat() == null) {
                  putControlEvent(new RejectControlEvent(acceptControlEvent.getSessionId(), "Required IE(s) missed", CauseCode.MISSING_MANDATORY_IE));
                  continue;
                }

                long mediaFormat = 0;
                int mediaFrameCount = 0;
                for (AudioMediaFormatOption mediaFormatOption : audioOptions.getAudioMediaFormatOptions()) {
                  if (acceptControlEvent.getFormat() == mediaFormatOption.getAudioMediaFormat().getV_uint64()) {
                    mediaFormat = mediaFormatOption.getAudioMediaFormat().getV_uint64();
                    mediaFrameCount = mediaFormatOption.getAudioMediaFrameCount();
                    break;
                  }
                }

                if (mediaFormat == 0) {
                  putControlEvent(new RejectControlEvent(acceptControlEvent.getSessionId(), "Can't negotiate codec", CauseCode.BEARER_CAP_NOT_AVAILABLE));
                  continue;
                }

                call.handleAccept(mediaFormat, mediaFrameCount);
              } else if (controlEvent instanceof DtmfControlEvent) {
                DtmfControlEvent dtmfControlEvent = (DtmfControlEvent) controlEvent;
                call.handleDtmf(dtmfControlEvent.getDtmf());
              } else {
                logger.warn("[{}] - [{}] unprocessed", this, controlEvent);
              }
            }
          }
        } catch (ClosedIaxEngineException e) {
          logger.debug("[{}] - end executing", this);
          break;
        } catch (InterruptedException e) {
          logger.error("[{}] - unexpected InterruptedException - end executing", this, e);
          break;
        }
      }
    }

    @Override
    public String toString() {
      return getClass().getEnclosingClass().getSimpleName() + "$" + getClass().getSimpleName() + "@" + Integer.toHexString(hashCode());
    }

  }

  private final class MediaWorker implements Runnable {

    @Override
    public void run() {
      logger.debug("[{}] - begin executing", this);

      IaxEngine iaxEngineLocal = getIaxEngineLocal();
      while (true) {
        try {
          MediaEvent mediaEvent = iaxEngineLocal.takeMediaEvent();

          if (mediaEvent instanceof AudioMediaEvent) {
            AudioMediaEvent audioMediaEvent = (AudioMediaEvent) mediaEvent;

            IaxCall iaxCall = iaxCallMap.get(audioMediaEvent.getSessionId());
            if (iaxCall == null) {
              logger.trace("[{}] - Call with [{}] is not exist - Media Event dropped", this, audioMediaEvent.getSessionId());
              continue;
            }

            if (iaxCall.getMediaFormat() != audioMediaEvent.getAudioMediaFormat_uint64()) {
              logger.warn("[{}] - Call Media format [{}] with [{}] mismatch with Media Event format [{}]", this, iaxCall.getMediaFormat(), iaxCall.getSessionId(),
                  audioMediaEvent.getAudioMediaFormat_uint64());
              continue;
            }

            iaxCall.handleMedia(audioMediaEvent.getTimestamp(), audioMediaEvent.getData());
          }
        } catch (ClosedIaxEngineException e) {
          logger.debug("[{}] - end executing", this);
          break;
        } catch (InterruptedException e) {
          logger.error("[{}] - unexpected InterruptedException - end executing", this, e);
          break;
        }
      }
    }

    @Override
    public String toString() {
      return getClass().getEnclosingClass().getSimpleName() + "$" + getClass().getSimpleName() + "@" + Integer.toHexString(hashCode());
    }

  }

}
