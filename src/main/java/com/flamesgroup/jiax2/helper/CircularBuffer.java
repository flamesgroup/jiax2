/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.flamesgroup.jiax2.helper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;
import java.util.Arrays;

public final class CircularBuffer {

  final static Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

  private final int size;
  private final byte[] buffer;
  private final boolean fillLackDataByValue;
  private final byte fillValue;
  private int usedSize;
  private int wp;
  private int rp;

  public CircularBuffer(final int size, final boolean fillLackDataByValue, final byte fillValue) {
    if (size <= 0) {
      throw new IllegalArgumentException("size mustn't be less or equal than 0");
    }
    this.size = size;
    this.buffer = new byte[size];
    this.fillLackDataByValue = fillLackDataByValue;
    this.fillValue = fillValue;

    usedSize = 0;
    wp = 0;
    rp = 0;
  }

  public CircularBuffer(final int size) {
    this(size, false, (byte) 0);
  }

  public int getSize() {
    return size;
  }

  public int getFreeSize() {
    return size - usedSize;
  }

  public int getUsedSize() {
    return usedSize;
  }

  public boolean isFull() {
    return usedSize == size;
  }

  public boolean isEmpty() {
    return usedSize == 0;
  }

  public void clear() {
    usedSize = 0;
    wp = 0;
    rp = 0;
  }

  public int put(final byte[] b, final int pos, final int len) {
    if (len < 0) {
      throw new IllegalArgumentException("len mustn't be less than 0");
    }

    if (isFull()) {
      logger.trace("[{}] - try put buffer with length [{}] when it is FULL", this, len);
      return 0;
    }

    if (isEmpty()) { // provide some speed optimization by reset read position if buffer empty
      wp = 0;
      rp = 0;
    }

    int freeSize = getFreeSize();
    int putSize;
    if (len > freeSize) {
      logger.trace("[{}] - try put buffer with length [{}] when actual accessible [{}]", this, len, freeSize);
      putSize = freeSize;
    } else {
      putSize = len;
    }

    if (rp > wp) {
      System.arraycopy(b, pos, buffer, wp, putSize);
      wp += putSize;
    } else { // if (m_rp <= m_wp)
      int sizeLeftToEnd = size - wp;
      if (putSize > sizeLeftToEnd) {
        System.arraycopy(b, pos, buffer, wp, sizeLeftToEnd);
        System.arraycopy(b, pos + sizeLeftToEnd, buffer, 0, putSize - sizeLeftToEnd);
        wp = putSize - sizeLeftToEnd;
      } else if (putSize == sizeLeftToEnd) {
        System.arraycopy(b, pos, buffer, wp, putSize);
        wp = 0;
      } else {
        System.arraycopy(b, pos, buffer, wp, putSize);
        wp += putSize;
      }
    }
    usedSize += putSize;
    return putSize;
  }

  public int put(final byte[] b) {
    return put(b, 0, b.length);
  }

  public int get(final byte[] b, int pos, final int len) {
    if (len < 0) {
      throw new IllegalArgumentException("len mustn't be less than 0");
    }

    if (!fillLackDataByValue && isEmpty()) {
      logger.trace("[{}] - try get buffer with length [{}] when it is EMPTY", this, len);
      return 0;
    }

    int getSize;
    if (len > usedSize) {
      if (fillLackDataByValue) {
        int fillToPos = pos + len - usedSize;
        Arrays.fill(b, pos, fillToPos, fillValue);
        pos = fillToPos;
      } else {
        logger.trace("[{}] - try get buffer with length [{}] when actual accessible [{}]", this, len, usedSize);
      }
      getSize = usedSize;
    } else {
      getSize = len;
    }

    if (rp >= wp) {
      int sizeLeftToEnd = size - rp;
      if (getSize > sizeLeftToEnd) {
        System.arraycopy(buffer, rp, b, pos, sizeLeftToEnd);
        System.arraycopy(buffer, 0, b, pos + sizeLeftToEnd, getSize - sizeLeftToEnd);
        rp = getSize - sizeLeftToEnd;
      } else if (getSize == sizeLeftToEnd) {
        System.arraycopy(buffer, rp, b, pos, getSize);
        rp = 0;
      } else {
        System.arraycopy(buffer, rp, b, pos, getSize);
        rp += getSize;
      }
    } else { // if (m_rp < m_wp)
      System.arraycopy(buffer, rp, b, pos, getSize);
      rp += getSize;
    }
    usedSize -= getSize;
    if (fillLackDataByValue) {
      return len;
    } else {
      return getSize;
    }

  }

  public int get(final byte[] b) {
    return get(b, 0, b.length);
  }

}
