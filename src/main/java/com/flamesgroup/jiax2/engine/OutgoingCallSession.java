/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.flamesgroup.jiax2.engine;

import com.flamesgroup.jiax2.event.AcceptControlEvent;
import com.flamesgroup.jiax2.event.AnswerControlEvent;
import com.flamesgroup.jiax2.event.ControlEvent;
import com.flamesgroup.jiax2.event.NewControlEvent;
import com.flamesgroup.jiax2.event.RingingControlEvent;
import com.flamesgroup.jiax2.frame.full.ControlFullFrameSubclass;
import com.flamesgroup.jiax2.frame.full.FullFrame;
import com.flamesgroup.jiax2.frame.full.FullFrameType;
import com.flamesgroup.jiax2.frame.full.IaxFullFrameSubclass;
import com.flamesgroup.jiax2.frame.full.IeUtils;
import com.flamesgroup.jiax2.frame.full.ie.CalledNumberIe;
import com.flamesgroup.jiax2.frame.full.ie.CallingNumberIe;
import com.flamesgroup.jiax2.frame.full.ie.CallingpresIe;
import com.flamesgroup.jiax2.frame.full.ie.CallingtnsIe;
import com.flamesgroup.jiax2.frame.full.ie.CallingtonIe;
import com.flamesgroup.jiax2.frame.full.ie.CalltokenIe;
import com.flamesgroup.jiax2.frame.full.ie.CapabilityIe;
import com.flamesgroup.jiax2.frame.full.ie.CodecPrefsIe;
import com.flamesgroup.jiax2.frame.full.ie.FormatIe;
import com.flamesgroup.jiax2.frame.full.ie.IeMap;
import com.flamesgroup.jiax2.frame.full.ie.UsernameIe;
import com.flamesgroup.jiax2.frame.full.ie.VersionIe;

import java.net.SocketAddress;

public class OutgoingCallSession extends CallSession {

  public OutgoingCallSession(final IaxEngine iaxEngine, final SocketAddress target, final SessionId sessionId) {
    super(iaxEngine, target, sessionId);
  }

  @Override
  protected boolean processControlEventInternal(final ControlEvent controlEvent) {
    switch (getSessionState()) {
      //////////////////////////////////////////////////////////////////////////////////////////////////
      case UNINITIALIZED: {
        if (controlEvent instanceof NewControlEvent) {
          NewControlEvent newControlEvent = (NewControlEvent) controlEvent;

          reliableSendIaxNew(newControlEvent);

          setSessionState(SessionState.INITIALIZED);
          return true;
        }
        break;
      }        //////////////////////////////////////////////////////////////////////////////////////////////////
      case INITIALIZED: {
        break;
      }
      //////////////////////////////////////////////////////////////////////////////////////////////////
      case LINKED: {
        break;
      }
      //////////////////////////////////////////////////////////////////////////////////////////////////
      case UP: {
        break;
      }
      //////////////////////////////////////////////////////////////////////////////////////////////////
      case DEINITIALIZING_WITH_ACK: {
        break;
      }
      //////////////////////////////////////////////////////////////////////////////////////////////////
      case DEINITIALIZING_BY_GUARD_TIMEOUT: {
        break;
      }
      //////////////////////////////////////////////////////////////////////////////////////////////////
      case DEINITIALIZED: {
        break;
      }
      //////////////////////////////////////////////////////////////////////////////////////////////////
    }

    return super.processControlEventInternal(controlEvent);
  }

  @Override
  protected boolean processFullFrameInternal(final FullFrame fullFrame) {
    switch (getSessionState()) {
      //////////////////////////////////////////////////////////////////////////////////////////////////
      case INITIALIZED: {
        switch (fullFrame.getFrameType_uint8()) {
          case FullFrameType.IAX:
            switch (fullFrame.getSubclass_uint8()) {
              case IaxFullFrameSubclass.ACCEPT:
                setDestinationCallNumber_uint16(fullFrame.getSourceCallNumber_uint16());

                sendAck(fullFrame.getTimestamp_uint32(), fullFrame.getISeqno_uint8());

                offerControlEventAccept(fullFrame);

                setSessionState(SessionState.LINKED);
                return true;
            }
            break;
        }
        break;
      }
      //////////////////////////////////////////////////////////////////////////////////////////////////
      case LINKED: {
        switch (fullFrame.getFrameType_uint8()) {
          case FullFrameType.CONTROL:
            switch (fullFrame.getSubclass_uint8()) {
              case ControlFullFrameSubclass.RINGING:
                sendAck(fullFrame.getTimestamp_uint32(), fullFrame.getISeqno_uint8());

                offerControlEvent(new RingingControlEvent(getSessionId()));

                setSessionState(SessionState.LINKED);
                return true;
              case ControlFullFrameSubclass.ANSWER:
                sendAck(fullFrame.getTimestamp_uint32(), fullFrame.getISeqno_uint8());

                offerControlEvent(new AnswerControlEvent(getSessionId()));

                setSessionState(SessionState.UP);
                return true;
            }
            break;
        }
        break;
      }
      //////////////////////////////////////////////////////////////////////////////////////////////////
      case UP: {
        break;
      }
      //////////////////////////////////////////////////////////////////////////////////////////////////
      case DEINITIALIZING_WITH_ACK: {
        break;
      }
      //////////////////////////////////////////////////////////////////////////////////////////////////
      case DEINITIALIZING_BY_GUARD_TIMEOUT: {
        break;
      }
      //////////////////////////////////////////////////////////////////////////////////////////////////
      case DEINITIALIZED: {
        break;
      }
      //////////////////////////////////////////////////////////////////////////////////////////////////
    }

    return super.processFullFrameInternal(fullFrame);
  }

  protected void reliableSendIaxNew(final NewControlEvent newControlEvent) {
    IeMap ieMap = new IeMap();
    ieMap.add(new VersionIe(IAX_PROTOCOL_VERSION));

    UsernameIe usernameIe = new UsernameIe();
    usernameIe.setV(newControlEvent.getCalledNumber());
    ieMap.add(usernameIe);

    CallingNumberIe callingNumberIe = new CallingNumberIe();
    callingNumberIe.setV(newControlEvent.getCallingNumber());
    ieMap.add(callingNumberIe);

    CallingtnsIe callingtnsIe = new CallingtnsIe();
    callingtnsIe.setV_uint16((short) 0x00);
    ieMap.add(callingtnsIe);

    CallingtonIe callingtonIe = new CallingtonIe();
    callingtonIe.setV_uint8((byte) 0x00);
    ieMap.add(callingtonIe);

    CallingpresIe callingpresIe = new CallingpresIe();
    callingpresIe.setV_uint8((byte) 0x00);
    ieMap.add(callingpresIe);

    CalledNumberIe calledNumberIe = new CalledNumberIe();
    calledNumberIe.setV(newControlEvent.getCalledNumber());
    ieMap.add(calledNumberIe);

    ieMap.add(new FormatIe(newControlEvent.getFormat().intValue()));

    CapabilityIe capabilityIe = new CapabilityIe();
    capabilityIe.setV_uint32(newControlEvent.getCapability().intValue());
    ieMap.add(capabilityIe);

    ieMap.add(new CodecPrefsIe());

    CalltokenIe calltokenIe = new CalltokenIe();
    ieMap.add(calltokenIe);

    reliableSendIaxFullFrame(IaxFullFrameSubclass.NEW, ieMap);
  }

  protected void offerControlEventAccept(final FullFrame fullFrame) {
    assert fullFrame.getFrameType_uint8() == FullFrameType.IAX && fullFrame.getSubclass_uint8() == IaxFullFrameSubclass.ACCEPT;

    IeMap ieMap = IeUtils.decode(fullFrame.getData());
    Long format = Long.valueOf(ieMap.getUint32(FormatIe.class));

    offerControlEvent(new AcceptControlEvent(getSessionId(), format));
  }

}
