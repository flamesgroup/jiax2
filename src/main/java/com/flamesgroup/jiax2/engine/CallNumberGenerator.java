/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.flamesgroup.jiax2.engine;

import java.util.NoSuchElementException;

public class CallNumberGenerator {

  public static final int IAX2_MIN_CALL_NUMBER = 2;              // Minimum value for local call numbers
  public static final int IAX2_MAX_CALL_NUMBER = 32767;          // Max call number value

  private final int minCallNumber;
  private final int maxCallNumber;
  private final boolean[] usedCallNumbers;
  private int nextPossibleCallNumber;

  public CallNumberGenerator(final int minCallNumber, final int maxCallNumber) {
    if (minCallNumber < IAX2_MIN_CALL_NUMBER) {
      throw new IllegalArgumentException("Min Call Number mustn't be less than " + IAX2_MIN_CALL_NUMBER);
    }
    if (maxCallNumber > IAX2_MAX_CALL_NUMBER) {
      throw new IllegalArgumentException("Max Call Number mustn't be above than " + IAX2_MAX_CALL_NUMBER);
    }

    this.minCallNumber = minCallNumber;
    this.maxCallNumber = maxCallNumber;
    usedCallNumbers = new boolean[maxCallNumber + 1];
    nextPossibleCallNumber = minCallNumber;
  }


  public CallNumberGenerator() {
    this(IAX2_MIN_CALL_NUMBER, IAX2_MAX_CALL_NUMBER);
  }

  public short generateCallNumber_uint16() {
    int i = 0;
    try {
      for (i = nextPossibleCallNumber; i <= maxCallNumber; i++) {
        if (!usedCallNumbers[i]) {
          usedCallNumbers[i] = true;
          return (short) i;
        }
      }
      for (i = minCallNumber; i < nextPossibleCallNumber; i++) {
        if (!usedCallNumbers[i]) {
          usedCallNumbers[i] = true;
          return (short) i;
        }
      }
    } finally {
      nextPossibleCallNumber = i + 1;
      if (nextPossibleCallNumber > maxCallNumber) {
        nextPossibleCallNumber = minCallNumber;
      }
    }
    throw new NoSuchElementException();
  }

  public void releaseCallNumber_uint16(final short callNumber_uint16) {
    int callNumber = Short.toUnsignedInt(callNumber_uint16);
    if (callNumber < minCallNumber || callNumber > maxCallNumber) {
      throw new IndexOutOfBoundsException("Call Number can't be less than " + minCallNumber + " and above than " + maxCallNumber);
    }
    if (usedCallNumbers[callNumber]) {
      usedCallNumbers[callNumber] = false;
    } else {
      throw new IllegalStateException("Try to release unused Call Number");
    }
  }

}
