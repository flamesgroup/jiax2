/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.flamesgroup.jiax2.engine;

import java.util.concurrent.atomic.AtomicLong;

public final class SessionId {

  private final static AtomicLong lastValue = new AtomicLong();

  public static SessionId generateSessionId() {
    return new SessionId(lastValue.getAndIncrement());
  }

  private final long value;

  private SessionId(final long value) {
    this.value = value;
  }

  @Override
  public boolean equals(final Object o) {
    // In current implementation assumed that each instance of this class will be unique
    return super.equals(o);
  }

  @Override
  public int hashCode() {
    return super.hashCode();
  }

  @Override
  public String toString() {
    return "SessionId#" + value;
  }

}
