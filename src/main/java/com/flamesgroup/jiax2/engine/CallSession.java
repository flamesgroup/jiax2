/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.flamesgroup.jiax2.engine;

import com.flamesgroup.jiax2.event.AcceptControlEvent;
import com.flamesgroup.jiax2.event.AnswerControlEvent;
import com.flamesgroup.jiax2.event.AudioMediaEvent;
import com.flamesgroup.jiax2.event.ControlEvent;
import com.flamesgroup.jiax2.event.DtmfControlEvent;
import com.flamesgroup.jiax2.event.HangupControlEvent;
import com.flamesgroup.jiax2.event.InternalErrorControlEvent;
import com.flamesgroup.jiax2.event.MediaEvent;
import com.flamesgroup.jiax2.event.ProceedingControlEvent;
import com.flamesgroup.jiax2.event.RejectControlEvent;
import com.flamesgroup.jiax2.event.RingingControlEvent;
import com.flamesgroup.jiax2.frame.Frame;
import com.flamesgroup.jiax2.frame.full.CauseCode;
import com.flamesgroup.jiax2.frame.full.ControlFullFrameSubclass;
import com.flamesgroup.jiax2.frame.full.FullFrame;
import com.flamesgroup.jiax2.frame.full.FullFrameType;
import com.flamesgroup.jiax2.frame.full.IaxFullFrameSubclass;
import com.flamesgroup.jiax2.frame.full.IeUtils;
import com.flamesgroup.jiax2.frame.full.MediaFormat;
import com.flamesgroup.jiax2.frame.full.ie.CauseIe;
import com.flamesgroup.jiax2.frame.full.ie.CausecodeIe;
import com.flamesgroup.jiax2.frame.full.ie.FormatIe;
import com.flamesgroup.jiax2.frame.full.ie.IeMap;
import com.flamesgroup.jiax2.frame.meta.MetaTrunkMiniFrame;
import com.flamesgroup.jiax2.frame.mini.MiniFrame;

import java.net.SocketAddress;

public abstract class CallSession extends Session {

  protected static final short IAX_PROTOCOL_VERSION = 0x0002;

  private static final int PING_INTERVAL_MINIMAL = 10000;
  private static final int PING_INTERVAL_DEFAULT = 20000;

  private static final int ADJUST_TIMESTAMP_OUT_THRESHOLD = 120;
  private static final int ADJUST_TIMESTAMP_OUT_OVERRUN = 120;
  private static final int ADJUST_TIMESTAMP_OUT_UNDERRUN = 60;


  private long nextPingTimestamp;

  private int deinitializingAckTimestamp_uint32;
  private int deinitializingGuardTimeoutTimestamp_uint32;


  private long audioMediaFormat;

  private long receivedAudioMediaFormat;

  private boolean audioOutStart = false;
  private int audioOutTimestampStart;
  private int audioOutTimestampLast;
  private int audioMediaEventOutTimestampFirst;

  private int audioSend;
  private int audioSendBytes;
  private int audioDropSendBytes;

  private boolean audioInStart = false;
  private int audioInTimestampStart;
  private int audioInTimestampLast;

  private int audioReceived;
  private int audioReceivedBytes;
  private int audioDropReceivedBytes;

  private int audioInRequestVnak;

  public CallSession(final IaxEngine iaxEngine, final SocketAddress target, final SessionId sessionId) {
    super(iaxEngine, target, sessionId);
  }

  @Override
  protected boolean processControlEventInternal(final ControlEvent controlEvent) {
    switch (getSessionState()) {
      //////////////////////////////////////////////////////////////////////////////////////////////////
      case INITIALIZED: {
        if (controlEvent instanceof RejectControlEvent) {
          deinitializingAckTimestamp_uint32 = reliableSendIaxReject((RejectControlEvent) controlEvent);

          setSessionState(SessionState.DEINITIALIZING_WITH_ACK);
          return true;
        }
        if (controlEvent instanceof HangupControlEvent) {
          deinitializingAckTimestamp_uint32 = reliableSendIaxHangup((HangupControlEvent) controlEvent);

          setSessionState(SessionState.DEINITIALIZING_WITH_ACK);
          return true;
        }
        break;
      }
      //////////////////////////////////////////////////////////////////////////////////////////////////
      case LINKED: {
        if (controlEvent instanceof HangupControlEvent) {
          deinitializingAckTimestamp_uint32 = reliableSendIaxHangup((HangupControlEvent) controlEvent);

          setSessionState(SessionState.DEINITIALIZING_WITH_ACK);
          return true;
        } else if (controlEvent instanceof DtmfControlEvent) {
          reliableSendDtmf((DtmfControlEvent) controlEvent);
          return true;
        }
        break;
      }
      //////////////////////////////////////////////////////////////////////////////////////////////////
      case UP: {
        if (controlEvent instanceof HangupControlEvent) {
          deinitializingAckTimestamp_uint32 = reliableSendIaxHangup((HangupControlEvent) controlEvent);

          setSessionState(SessionState.DEINITIALIZING_WITH_ACK);
          return true;
        } else if (controlEvent instanceof DtmfControlEvent) {
          reliableSendDtmf((DtmfControlEvent) controlEvent);
          return true;
        }
        break;
      }
    }

    return false;
  }

  @Override
  protected boolean processMediaEventInternal(final MediaEvent mediaEvent) {
    switch (getSessionState()) {
      //////////////////////////////////////////////////////////////////////////////////////////////////
      case LINKED: {
        if (mediaEvent instanceof AudioMediaEvent) {
          sendAudioMedia((AudioMediaEvent) mediaEvent);
          return true;
        }
        break;
      }
      //////////////////////////////////////////////////////////////////////////////////////////////////
      case UP: {
        if (mediaEvent instanceof AudioMediaEvent) {
          sendAudioMedia((AudioMediaEvent) mediaEvent);
          return true;
        }
        break;
      }
    }

    return false;
  }

  @Override
  protected boolean processFullFrameInternal(final FullFrame fullFrame) {
    switch (getSessionState()) {
      //////////////////////////////////////////////////////////////////////////////////////////////////
      case INITIALIZED: {
        switch (fullFrame.getFrameType_uint8()) {
          //////////////////////////////////////////////////////////////////////////////////////////
          case FullFrameType.IAX: {
            switch (fullFrame.getSubclass_uint8()) {
              case IaxFullFrameSubclass.HANGUP: {
                sendAck(fullFrame.getTimestamp_uint32(), fullFrame.getISeqno_uint8());

                offerControlEventHangup(fullFrame);

                setSessionState(SessionState.DEINITIALIZING_BY_GUARD_TIMEOUT);
                return true;
              }
            }
            break;
          }
          //////////////////////////////////////////////////////////////////////////////////////////
          case FullFrameType.VOICE: {
            InternalErrorControlEvent internalErrorControlEvent = new InternalErrorControlEvent(getSessionId(), "Received full Voice before Accept");
            offerControlEvent(internalErrorControlEvent);

            IeMap ieMap = new IeMap();
            ieMap.setUtf8(CauseIe.class, "Received full Voice before Accept");
            ieMap.setUint8(CausecodeIe.class, CauseCode.WRONG_STATE_MESSAGE);

            reliableSendIaxFullFrame(IaxFullFrameSubclass.HANGUP, ieMap);

            setSessionState(SessionState.DEINITIALIZING_WITH_ACK);
            return true;
          }
          //////////////////////////////////////////////////////////////////////////////////////////
          case FullFrameType.DTMF: {
            break;
          }
          //////////////////////////////////////////////////////////////////////////////////////////
        }
        break;
      }
      //////////////////////////////////////////////////////////////////////////////////////////////////
      case LINKED: {
        switch (fullFrame.getFrameType_uint8()) {
          //////////////////////////////////////////////////////////////////////////////////////////
          case FullFrameType.IAX: {
            switch (fullFrame.getSubclass_uint8()) {
              case IaxFullFrameSubclass.HANGUP: {
                sendAck(fullFrame.getTimestamp_uint32(), fullFrame.getISeqno_uint8());

                offerControlEventHangup(fullFrame);

                setSessionState(SessionState.DEINITIALIZING_BY_GUARD_TIMEOUT);
                return true;
              }
            }
            break;
          }
          //////////////////////////////////////////////////////////////////////////////////////////
          case FullFrameType.CONTROL: {
            break;
          }
          //////////////////////////////////////////////////////////////////////////////////////////
          case FullFrameType.VOICE: {
            sendAck(fullFrame.getTimestamp_uint32(), fullFrame.getISeqno_uint8());

            offerMediaEventAudio(fullFrame);
            return true;
          }
          //////////////////////////////////////////////////////////////////////////////////////////
          case FullFrameType.DTMF: {
            sendAck(fullFrame.getTimestamp_uint32(), fullFrame.getISeqno_uint8());

            offerControlEventDtmf(fullFrame);
            return true;
          }
          //////////////////////////////////////////////////////////////////////////////////////////
        }
        break;
      }
      //////////////////////////////////////////////////////////////////////////////////////////////////
      case UP: {
        switch (fullFrame.getFrameType_uint8()) {
          //////////////////////////////////////////////////////////////////////////////////////////
          case FullFrameType.IAX: {
            switch (fullFrame.getSubclass_uint8()) {
              case IaxFullFrameSubclass.REJECT:
              case IaxFullFrameSubclass.HANGUP: {
                sendAck(fullFrame.getTimestamp_uint32(), fullFrame.getISeqno_uint8());

                offerControlEventHangup(fullFrame);

                setSessionState(SessionState.DEINITIALIZING_BY_GUARD_TIMEOUT);
                return true;
              }
            }
            break;
          }
          //////////////////////////////////////////////////////////////////////////////////////////
          case FullFrameType.VOICE: {
            sendAck(fullFrame.getTimestamp_uint32(), fullFrame.getISeqno_uint8());

            offerMediaEventAudio(fullFrame);
            return true;
          }
          //////////////////////////////////////////////////////////////////////////////////////////
          case FullFrameType.DTMF: {
            sendAck(fullFrame.getTimestamp_uint32(), fullFrame.getISeqno_uint8());

            offerControlEventDtmf(fullFrame);
            return true;
          }
          //////////////////////////////////////////////////////////////////////////////////////////
        }
        break;
      }
      //////////////////////////////////////////////////////////////////////////////////////////////////
      case DEINITIALIZING_WITH_ACK: {
        switch (fullFrame.getFrameType_uint8()) {
          //////////////////////////////////////////////////////////////////////////////////////////
          case FullFrameType.IAX: {
            switch (fullFrame.getSubclass_uint8()) {
              case IaxFullFrameSubclass.ACK: {
                if (fullFrame.getTimestamp_uint32() == deinitializingAckTimestamp_uint32) {
                  setSessionState(SessionState.DEINITIALIZED);
                  return true;
                }
                break;
              }
            }
            break;
          }
          //////////////////////////////////////////////////////////////////////////////////////////
        }
        break;
      }
    }
    return processPingAndLagrqRequest(fullFrame) || processPongAndLagrpResponse(fullFrame) || processAck(fullFrame);
  }

  @Override
  protected boolean processMetaTrunkFrameMediaItemInternal(final MetaTrunkMiniFrame.MediaItem mediaItem) {
    offerMediaEventAudio(mediaItem);
    return true;
  }

  @Override
  protected boolean processMiniFrameInternal(final MiniFrame miniFrame) {
    offerMediaEventAudio(miniFrame);
    return true;
  }

  @Override
  protected void processRoutineInternal(final int timestamp_uint32) {
    switch (getSessionState()) {
      //////////////////////////////////////////////////////////////////////////////////////////////////
      case INITIALIZED: {
        break;
      }
      //////////////////////////////////////////////////////////////////////////////////////////////////
      case LINKED: {
        break;
      }
      //////////////////////////////////////////////////////////////////////////////////////////////////
      case UP: {
        break;
      }
      //////////////////////////////////////////////////////////////////////////////////////////////////
      case DEINITIALIZING_WITH_ACK: {
        break;
      }
      //////////////////////////////////////////////////////////////////////////////////////////////////
      case DEINITIALIZING_BY_GUARD_TIMEOUT: {
        if (Integer.compareUnsigned(timestamp_uint32, deinitializingGuardTimeoutTimestamp_uint32) > 0) {
          setSessionState(SessionState.DEINITIALIZED);
        }
        break;
      }
      //////////////////////////////////////////////////////////////////////////////////////////////////
    }

    if (getSessionState() == SessionState.DEINITIALIZING_WITH_ACK || getSessionState() == SessionState.DEINITIALIZING_BY_GUARD_TIMEOUT || getSessionState() == SessionState.DEINITIALIZED) {
      return;
    }

    if (nextPingTimestamp == 0L || timestamp_uint32 > nextPingTimestamp) {
      if (nextPingTimestamp != 0L) {
        reliableSendPing();
      }
      nextPingTimestamp = timestamp_uint32 + PING_INTERVAL_DEFAULT;
    }
  }



  protected boolean processPingAndLagrqRequest(final FullFrame fullFrame) {
    if (fullFrame.getFrameType_uint8() == FullFrameType.IAX) {
      switch (fullFrame.getSubclass_uint8()) {
        case IaxFullFrameSubclass.PING:
          reliableSendPong(fullFrame.getTimestamp_uint32());
          return true;
        case IaxFullFrameSubclass.LAGRQ:
          reliableSendLagRp(fullFrame.getTimestamp_uint32());
          return true;
      }
    }
    return false;
  }

  protected boolean processPongAndLagrpResponse(final FullFrame fullFrame) {
    if (fullFrame.getFrameType_uint8() == FullFrameType.IAX) {
      switch (fullFrame.getSubclass_uint8()) {
        case IaxFullFrameSubclass.PONG:
        case IaxFullFrameSubclass.LAGRP:
          sendAck(fullFrame.getTimestamp_uint32(), fullFrame.getISeqno_uint8());
          return true;
      }
    }
    return false;
  }

  protected boolean processAck(final FullFrame fullFrame) {
    switch (fullFrame.getFrameType_uint8()) {
      case FullFrameType.IAX:
        switch (fullFrame.getSubclass_uint8()) {
          case IaxFullFrameSubclass.ACK:
            return true;
        }
        break;
    }
    return false;
  }



  protected void reliableSendIaxAccept(final AcceptControlEvent acceptControlEvent) {
    IeMap ieMap = new IeMap();
    ieMap.add(new FormatIe(acceptControlEvent.getFormat().intValue()));

    reliableSendIaxFullFrame(IaxFullFrameSubclass.ACCEPT, ieMap);
  }

  protected int reliableSendIaxHangup(final HangupControlEvent hangupControlEvent) {
    IeMap ieMap = new IeMap();
    ieMap.setUtf8(CauseIe.class, hangupControlEvent.getCause());
    ieMap.setUint8(CausecodeIe.class, hangupControlEvent.getCauseCode());

    return reliableSendIaxFullFrame(IaxFullFrameSubclass.HANGUP, ieMap);
  }

  protected int reliableSendIaxReject(final RejectControlEvent rejectControlEvent) {
    IeMap ieMap = new IeMap();
    ieMap.setUtf8(CauseIe.class, rejectControlEvent.getCause());
    ieMap.setUint8(CausecodeIe.class, rejectControlEvent.getCauseCode());

    return reliableSendIaxFullFrame(IaxFullFrameSubclass.REJECT, ieMap);
  }

  protected void reliableSendControlAnswer(final AnswerControlEvent answerControlEvent) {
    reliableSendControlFullFrame(ControlFullFrameSubclass.ANSWER);
  }

  protected void reliableSendControlRinging(final RingingControlEvent ringingControlEvent) {
    reliableSendControlFullFrame(ControlFullFrameSubclass.RINGING);
  }

  protected void reliableSendControlProceeding(final ProceedingControlEvent proceedingControlEvent) {
    reliableSendControlFullFrame(ControlFullFrameSubclass.CALL_PROCEEDING);
  }

  protected void reliableSendDtmf(final DtmfControlEvent controlEvent) {
    reliableSendDtmfFullFrame(controlEvent.getDtmf());
  }

  protected void offerControlEventHangup(final FullFrame fullFrame) {
    assert fullFrame.getFrameType_uint8() == FullFrameType.IAX && fullFrame.getSubclass_uint8() == IaxFullFrameSubclass.HANGUP;

    IeMap ieMap = IeUtils.decode(fullFrame.getData());
    String cause = ieMap.getUtf8(CauseIe.class);
    Byte causeCode = ieMap.getUint8(CausecodeIe.class);

    offerControlEvent(new HangupControlEvent(getSessionId(), cause, causeCode));

    deinitializingGuardTimeoutTimestamp_uint32 = currentTimestamp_uint32() + calculateOverallTimeout_uint32();
  }

  protected void offerControlEventDtmf(final FullFrame fullFrame) {
    assert fullFrame.getFrameType_uint8() == FullFrameType.DTMF;

    offerControlEvent(new DtmfControlEvent(getSessionId(), (char) fullFrame.getSubclass_uint8()));
  }



  private void sendAudioMedia(final AudioMediaEvent audioMediaEvent) {
    boolean audioMediaFormatChange = audioMediaFormat != audioMediaEvent.getAudioMediaFormat_uint64();
    if (audioMediaFormatChange) {
      logger.debug("[{}] - outgoing format changed from [{}] to [{}]", this, MediaFormat.asString(audioMediaFormat), MediaFormat.asString(audioMediaEvent.getAudioMediaFormat_uint64()));
      audioMediaFormat = audioMediaEvent.getAudioMediaFormat_uint64();
    }

    int currentTimestamp = currentTimestamp_uint32();

    int audioTimestampNext;

    if (!audioOutStart) {
      logger.trace("[{}] - started outgoing audio media in format [{}]", this, MediaFormat.asString(audioMediaFormat));
      audioOutStart = true;
      audioOutTimestampStart = currentTimestamp;
      audioMediaEventOutTimestampFirst = audioMediaEvent.getTimestamp();
      audioTimestampNext = audioOutTimestampStart;
    } else {
      if (audioMediaEventOutTimestampFirst > audioMediaEvent.getTimestamp()) {
        logger.warn("[{}] - dropping outdated Audio Media Event with timestamp [{}]", this, audioMediaEvent.getTimestamp());
        audioDropSendBytes += audioMediaEvent.getData().length;
        return;
      } else {
        int audioMediaEventTimestampDelta = audioMediaEvent.getTimestamp() - audioMediaEventOutTimestampFirst;
        audioTimestampNext = audioOutTimestampStart + audioMediaEventTimestampDelta;
        if (audioTimestampNext > currentTimestamp) {
          // Voice timestamp is past transaction timestamp
          // Packets arrived on intervals shorter then expected
          // Data overrun: decrease timestamp
          int delta = audioTimestampNext - currentTimestamp;
          if (delta >= ADJUST_TIMESTAMP_OUT_THRESHOLD) {
            logger.warn("[{}] - voice overrun audio timestamp [{}] when current timestamp [{}]", this, audioTimestampNext, currentTimestamp);
            audioDropSendBytes += audioMediaEvent.getData().length;
            audioOutTimestampStart -= ADJUST_TIMESTAMP_OUT_OVERRUN;
            return;
          }
        } else if (audioTimestampNext < currentTimestamp) {
          // Voice timestamp is behind transaction timestamp
          // Packets arrived on intervals longer then expected
          // Data underrun: increase timestamp
          int delta = currentTimestamp - audioTimestampNext;
          if (delta >= ADJUST_TIMESTAMP_OUT_THRESHOLD) {
            logger.warn("[{}] - voice underrun audio timestamp [{}] when current timestamp [{}]", this, audioTimestampNext, currentTimestamp);
            audioDropSendBytes += audioMediaEvent.getData().length;
            audioOutTimestampStart += ADJUST_TIMESTAMP_OUT_UNDERRUN;
            return;
          }
        }

        if (audioTimestampNext == audioOutTimestampLast) {
          audioTimestampNext++;
        }
      }
    }

    if (audioTimestampNext < audioOutTimestampLast) {
      audioDropSendBytes += audioMediaEvent.getData().length;
      logger.warn("[{}] - audio media timestamp [{}] less then last send [{}] - drop it", this, audioTimestampNext, audioOutTimestampLast);
      return;
    }

    boolean fullFrame = audioMediaFormatChange || (audioOutTimestampLast == 0);
    if (!fullFrame) {
      // Voice: timestamp is lowest 16 bits
      int mask = 0xFFFF;
      // Timestamp wraparound if mini timestamp is less then last one or
      // we had a media gap greater then mask
      fullFrame = ((audioTimestampNext & mask) < (audioOutTimestampLast & mask)) || ((audioTimestampNext - audioOutTimestampLast) > mask); // this magic inspired by YATE project IAX implementation
    }

    audioOutTimestampLast = audioTimestampNext;

    if (fullFrame) {
      if (isTrunkOutLinkExist()) {
        flushTrunkOutLink(); // Send trunked frame before full frame to keep the media order
      }
      reliableSendVoiceFullFrame(audioMediaEvent.getAudioMediaFormat_uint64(), audioTimestampNext, audioMediaEvent.getData());
    } else {
      if (isTrunkOutLinkExist()) {
        addMetaTrunkFrameMediaItemToTrunkOutLink((short) audioTimestampNext, audioMediaEvent.getData());
      } else {
        sendMiniFrame((short) audioTimestampNext, audioMediaEvent.getData());
      }
    }

    audioSend++;
    audioSendBytes += audioMediaEvent.getData().length;
  }

  protected void offerMediaEventAudio(final FullFrame fullFrame) {
    assert fullFrame.getFrameType_uint8() == FullFrameType.VOICE;

    if (receivedAudioMediaFormat != fullFrame.getUncompressedSubclass_uint64()) {
      logger.debug("[{}] - incoming format changed from [{}] to [{}]", this, MediaFormat.asString(receivedAudioMediaFormat), MediaFormat.asString(fullFrame.getUncompressedSubclass_uint64()));
      receivedAudioMediaFormat = fullFrame.getUncompressedSubclass_uint64();
    }

    if (!audioInStart) {
      logger.trace("[{}] - started incoming audio media in format [{}]", this, MediaFormat.asString(receivedAudioMediaFormat));
      audioInStart = true;
    }

    processMediaEventAudio(fullFrame.getTimestamp_uint32(), true, fullFrame.getData());
  }

  protected void offerMediaEventAudio(final MetaTrunkMiniFrame.MediaItem mediaItem) {
    if (isReceivedBeforeFullFrameAudio()) {
      sendVnakOnReceivedBeforeFullFrameAudio(mediaItem);
      return;
    }

    processMediaEventAudio(mediaItem.getTimestamp_uint16(), false, mediaItem.getData());
  }

  protected void offerMediaEventAudio(final MiniFrame miniFrame) {
    if (isReceivedBeforeFullFrameAudio()) {
      sendVnakOnReceivedBeforeFullFrameAudio(miniFrame);
    }

    processMediaEventAudio(miniFrame.getTimestamp_uint16(), false, miniFrame.getData());
  }

  private boolean isReceivedBeforeFullFrameAudio() {
    return !audioInStart;
  }

  private void sendVnakOnReceivedBeforeFullFrameAudio(final Frame frame) {
    if (audioInRequestVnak > 15) { // ???
      return;
    }
    audioInRequestVnak++;
    if (audioInRequestVnak == 3) {
      logger.warn("[{}] - received [{}] before Voice Full Frame", this, frame);
    }
    if (audioInRequestVnak % 3 == 0) {
      sendVnak();
    }
  }

  private void processMediaEventAudio(final int timestamp, final boolean timestampFull, final byte[] data) {
    int audioTimestampNext;

    if (timestampFull) {
      audioTimestampNext = timestamp;
    } else {
      // Voice: timestamp is lowest 16 bits
      int mask = 0xFFFF;

      audioTimestampNext = timestamp & mask;

      // Interval between received timestamp and last one:
      // Negative: wraparound if less then half mask
      int delta = audioTimestampNext - (audioInTimestampLast & mask);
      if (delta < 0 && (Math.abs(delta) < mask / 2)) {
        logger.warn("[{}] - dropping Media Event Audio with timestamp [{}]", this, timestamp);
        audioDropReceivedBytes += data.length;
        return;
      } else {
        // Add upper bits from last frame, adjust timestamp if wrapped around
        audioTimestampNext |= audioInTimestampLast & ~mask;
        if (delta < 0) {
          logger.debug("[{}] - wraparound Media Event Audio timestamp [{}]", this, audioTimestampNext);
          audioTimestampNext += mask + 1;
        }
      }
    }

    boolean forward = audioTimestampNext >= audioInTimestampLast;
    if (forward) {
      // New frame is newer then the last one
      logger.trace("[{}] - forwarding Media Event Audio [{}] with timestamp [{}]", this, data.length, audioTimestampNext);
      audioInTimestampLast = audioTimestampNext;
      offerMediaEvent(new AudioMediaEvent(getSessionId(), receivedAudioMediaFormat, audioTimestampNext, data));
    } else {
      logger.warn("[{}] - dropping Media Event Audio [{}] with timestamp [{}]", this, data.length, audioTimestampNext);
      audioDropReceivedBytes += data.length;
    }

    audioReceived++;
    audioReceivedBytes += data.length;
  }

}
