/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.flamesgroup.jiax2.engine;

import com.flamesgroup.jiax2.CallDropReason;
import com.flamesgroup.jiax2.event.AcceptControlEvent;
import com.flamesgroup.jiax2.event.AnswerControlEvent;
import com.flamesgroup.jiax2.event.ControlEvent;
import com.flamesgroup.jiax2.event.HangupControlEvent;
import com.flamesgroup.jiax2.event.NewControlEvent;
import com.flamesgroup.jiax2.event.ProceedingControlEvent;
import com.flamesgroup.jiax2.event.RingingControlEvent;
import com.flamesgroup.jiax2.frame.full.FullFrame;
import com.flamesgroup.jiax2.frame.full.FullFrameType;
import com.flamesgroup.jiax2.frame.full.IaxFullFrameSubclass;
import com.flamesgroup.jiax2.frame.full.IeUtils;
import com.flamesgroup.jiax2.frame.full.ie.CalledContextIe;
import com.flamesgroup.jiax2.frame.full.ie.CalledNumberIe;
import com.flamesgroup.jiax2.frame.full.ie.CallingNumberIe;
import com.flamesgroup.jiax2.frame.full.ie.CapabilityIe;
import com.flamesgroup.jiax2.frame.full.ie.FormatIe;
import com.flamesgroup.jiax2.frame.full.ie.IeMap;
import com.flamesgroup.jiax2.frame.full.ie.VersionIe;

import java.net.SocketAddress;

public class IncomingCallSession extends CallSession {

  public IncomingCallSession(final IaxEngine iaxEngine, final SocketAddress target, final SessionId sessionId) {
    super(iaxEngine, target, sessionId);
  }

  @Override
  protected boolean processControlEventInternal(final ControlEvent controlEvent) {
    switch (getSessionState()) {
      //////////////////////////////////////////////////////////////////////////////////////////////////
      case INITIALIZED: {
        if (controlEvent instanceof AcceptControlEvent) {
          reliableSendIaxAccept((AcceptControlEvent) controlEvent);

          setSessionState(SessionState.LINKED);
          return true;
        }
        break;
      }
      //////////////////////////////////////////////////////////////////////////////////////////////////
      case LINKED: {
        if (controlEvent instanceof AnswerControlEvent) {
          reliableSendControlAnswer((AnswerControlEvent) controlEvent);

          setSessionState(SessionState.UP);
          return true;
        }
        if (controlEvent instanceof RingingControlEvent) {
          reliableSendControlRinging((RingingControlEvent) controlEvent);

          setSessionState(SessionState.LINKED);
          return true;
        }
        if (controlEvent instanceof ProceedingControlEvent) {
          reliableSendControlProceeding((ProceedingControlEvent) controlEvent);

          setSessionState(SessionState.LINKED);
          return true;
        }
        break;
      }
      //////////////////////////////////////////////////////////////////////////////////////////////////
    }

    return super.processControlEventInternal(controlEvent);
  }

  @Override
  protected boolean processFullFrameInternal(final FullFrame fullFrame) {
    switch (getSessionState()) {
      //////////////////////////////////////////////////////////////////////////////////////////////////
      case UNINITIALIZED: {
        switch (fullFrame.getFrameType_uint8()) {
          //////////////////////////////////////////////////////////////////////////////////////////
          case FullFrameType.IAX: {
            switch (fullFrame.getSubclass_uint8()) {
              case IaxFullFrameSubclass.NEW: {
                setDestinationCallNumber_uint16(fullFrame.getSourceCallNumber_uint16());

                sendAck(fullFrame.getTimestamp_uint32(), fullFrame.getISeqno_uint8());

                offerControlEventNew(fullFrame);

                setSessionState(SessionState.INITIALIZED);
                return true;
              }
            }
            break;
          }
          //////////////////////////////////////////////////////////////////////////////////////////
        }
        break;
      }
      //////////////////////////////////////////////////////////////////////////////////////////////////
      case INITIALIZED: {
        switch (fullFrame.getFrameType_uint8()) {
          //////////////////////////////////////////////////////////////////////////////////////////
          case FullFrameType.IAX: {
            break;
          }
          //////////////////////////////////////////////////////////////////////////////////////////
          case FullFrameType.CONTROL: {
            break;
          }
          //////////////////////////////////////////////////////////////////////////////////////////
          case FullFrameType.VOICE: {
            break;
          }
          //////////////////////////////////////////////////////////////////////////////////////////
          case FullFrameType.DTMF: {
            break;
          }
          //////////////////////////////////////////////////////////////////////////////////////////
        }
        break;
      }
      //////////////////////////////////////////////////////////////////////////////////////////////////
      case LINKED: {
        switch (fullFrame.getFrameType_uint8()) {
          //////////////////////////////////////////////////////////////////////////////////////////
          case FullFrameType.IAX: {
            switch (fullFrame.getSubclass_uint8()) {
              case IaxFullFrameSubclass.INVAL: {
                offerControlEvent(new HangupControlEvent(getSessionId(), CallDropReason.SWITCH_CONGESTION.getCauseCode()));

                setSessionState(SessionState.DEINITIALIZED);
                return true;
              }
            }
            break;
          }
          //////////////////////////////////////////////////////////////////////////////////////////
          case FullFrameType.CONTROL: {
            break;
          }
          //////////////////////////////////////////////////////////////////////////////////////////
          case FullFrameType.VOICE: {
            break;
          }
          //////////////////////////////////////////////////////////////////////////////////////////
          case FullFrameType.DTMF: {
            break;
          }
          //////////////////////////////////////////////////////////////////////////////////////////
        }
        break;
      }
      //////////////////////////////////////////////////////////////////////////////////////////////////
      case UP: {
        switch (fullFrame.getFrameType_uint8()) {
          //////////////////////////////////////////////////////////////////////////////////////////
          case FullFrameType.IAX: {
            switch (fullFrame.getSubclass_uint8()) {
              case IaxFullFrameSubclass.INVAL: {
                offerControlEvent(new HangupControlEvent(getSessionId(), CallDropReason.SWITCH_CONGESTION.getCauseCode()));

                setSessionState(SessionState.DEINITIALIZED);
                return true;
              }
            }
            break;
          }
          //////////////////////////////////////////////////////////////////////////////////////////
          case FullFrameType.CONTROL: {
            break;
          }
          //////////////////////////////////////////////////////////////////////////////////////////
          case FullFrameType.VOICE: {
            break;
          }
          //////////////////////////////////////////////////////////////////////////////////////////
          case FullFrameType.DTMF: {
            break;
          }
          //////////////////////////////////////////////////////////////////////////////////////////
        }
        break;
      }
      //////////////////////////////////////////////////////////////////////////////////////////////////
      case DEINITIALIZING_WITH_ACK: {
        switch (fullFrame.getFrameType_uint8()) {
          //////////////////////////////////////////////////////////////////////////////////////////
          case FullFrameType.IAX: {
            break;
          }
          //////////////////////////////////////////////////////////////////////////////////////////
        }
        break;
      }
      //////////////////////////////////////////////////////////////////////////////////////////////////
      case DEINITIALIZING_BY_GUARD_TIMEOUT: {
        break;
      }
      //////////////////////////////////////////////////////////////////////////////////////////////////
      case DEINITIALIZED: {
        break;
      }
      //////////////////////////////////////////////////////////////////////////////////////////////////
    }

    return super.processFullFrameInternal(fullFrame);
  }

  protected void offerControlEventNew(final FullFrame fullFrame) {
    assert fullFrame.getFrameType_uint8() == FullFrameType.IAX && fullFrame.getSubclass_uint8() == IaxFullFrameSubclass.NEW;

    IeMap ieMap = IeUtils.decode(fullFrame.getData());
    Short version = ieMap.getUint16(VersionIe.class);
    String calledNumber = ieMap.getUtf8(CalledNumberIe.class);
    String callingNumber = ieMap.getUtf8(CallingNumberIe.class);
    String calledContext = ieMap.getUtf8(CalledContextIe.class);
    Long format = Long.valueOf(ieMap.getUint32(FormatIe.class));
    Long capability = Long.valueOf(ieMap.getUint32(CapabilityIe.class));

    offerControlEvent(new NewControlEvent(getSessionId(), getTarget(), calledNumber, callingNumber, calledContext, format, capability));
  }

}
