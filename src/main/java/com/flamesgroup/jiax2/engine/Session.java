/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.flamesgroup.jiax2.engine;

import com.flamesgroup.jiax2.event.ControlEvent;
import com.flamesgroup.jiax2.event.InternalErrorControlEvent;
import com.flamesgroup.jiax2.event.MediaEvent;
import com.flamesgroup.jiax2.frame.full.FullFrame;
import com.flamesgroup.jiax2.frame.full.FullFrameType;
import com.flamesgroup.jiax2.frame.full.IaxFullFrameSubclass;
import com.flamesgroup.jiax2.frame.full.IeUtils;
import com.flamesgroup.jiax2.frame.full.ie.IaxUnknownIe;
import com.flamesgroup.jiax2.frame.full.ie.IeMap;
import com.flamesgroup.jiax2.frame.meta.MetaTrunkMiniFrame;
import com.flamesgroup.jiax2.frame.mini.MiniFrame;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;
import java.net.SocketAddress;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

public abstract class Session {

  final static Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

  private SessionState sessionState = SessionState.UNINITIALIZED;

  private final long creationTimeMillis = System.currentTimeMillis();

  private final IaxEngine iaxEngine;
  private final SocketAddress target;
  private final SessionId sessionId;

  private final short callNumber_uint16;
  private short destinationCallNumber_uint16;

  private byte oSeqno_uint8;
  private byte iSeqno_uint8;

  private int lastTimestamp_uint32 = 0;
  private int lastProcessedFullFrameTimestamp_uint32 = 0;

  private final List<ReliableFullFrame> reliableFullFrames = new LinkedList<>();

  // Statistics
  private int inTotalFrames;
  private int inOutOfOrderFrames;
  private int inDuplicateFrames;

  public Session(final IaxEngine iaxEngine, final SocketAddress target, final SessionId sessionId) {
    this.iaxEngine = iaxEngine;
    this.target = target;
    this.sessionId = sessionId;

    callNumber_uint16 = iaxEngine.generateCallNumber_uint16();
  }

  void free() {
    iaxEngine.releaseCallNumber_uint16(callNumber_uint16);
  }


  public void processControlEvent(final ControlEvent controlEvent) {
    if (!processControlEventInternal(controlEvent)) {
      logger.warn("[{}] - [{}] unprocessed in session state [{}]", this, controlEvent, getSessionState());
    }
  }

  public void processMediaEvent(final MediaEvent mediaEvent) {
    if (!processMediaEventInternal(mediaEvent)) {
      logger.warn("[{}] - [{}] unprocessed in session state [{}]", this, mediaEvent, getSessionState());
    }
  }

  public void processFullFrame(final FullFrame fullFrame) {
    inTotalFrames++;

    // TODO add INVAL processing and correct session terminating

    if (fullFrame.getFrameType_uint8() == FullFrameType.IAX && fullFrame.getSubclass_uint8() == IaxFullFrameSubclass.VNAK) {
      retransmitReliableFullFramesWithVnakISeqno(fullFrame.getISeqno_uint8());
    } else {
      int seqnoDelta = fullFrame.getOSeqno_uint8() - iSeqno_uint8;
      if (seqnoDelta != 0) {
        if (isAckFullFrame(fullFrame)) {
          logger.info("[{}] - received [{}] out of order OSeqno [{}] and timestamp [{}] when expected OSeqno [{}] and timestamp greater [{}]", this, fullFrame,
              Byte.toUnsignedInt(fullFrame.getOSeqno_uint8()), Integer.toUnsignedLong(fullFrame.getTimestamp_uint32()), Byte.toUnsignedInt(iSeqno_uint8),
              Integer.toUnsignedLong(lastProcessedFullFrameTimestamp_uint32));
          updateReliableFullFramesWithProcessingFullFrameISeqno(fullFrame.getISeqno_uint8());
          return;
        }

        if (seqnoDelta > 0) {
          inOutOfOrderFrames++;
          if (isAckMustBeSentAccordingToFullFrame(fullFrame)) {
            logger.info("[{}] - received [{}] out of order with OSeqno [{}] and timestamp [{}] when expected OSeqno [{}] and timestamp greater [{}] - send VNAK", this, fullFrame,
                Byte.toUnsignedInt(fullFrame.getOSeqno_uint8()), Integer.toUnsignedLong(fullFrame.getTimestamp_uint32()), Byte.toUnsignedInt(iSeqno_uint8),
                Integer.toUnsignedLong(lastProcessedFullFrameTimestamp_uint32));
            sendVnak();
          } else {
            logger.info("[{}] - received [{}] out of order with OSeqno [{}] and timestamp [{}] when expected OSeqno [{}] and timestamp greater [{}] - dropped", this, fullFrame,
                Byte.toUnsignedInt(fullFrame.getOSeqno_uint8()), Integer.toUnsignedLong(fullFrame.getTimestamp_uint32()), Byte.toUnsignedInt(iSeqno_uint8),
                Integer.toUnsignedLong(lastProcessedFullFrameTimestamp_uint32));
          }
        } else {
          inDuplicateFrames++;
          if (isAckMustBeSentAccordingToFullFrame(fullFrame)) {
            logger.debug("[{}] - received [{}] late with OSeqno [{}] and timestamp [{}] when expected OSeqno [{}] and timestamp greater [{}] - send ACK", this, fullFrame,
                Byte.toUnsignedInt(fullFrame.getOSeqno_uint8()), Integer.toUnsignedLong(fullFrame.getTimestamp_uint32()), Byte.toUnsignedInt(iSeqno_uint8),
                Integer.toUnsignedLong(lastProcessedFullFrameTimestamp_uint32));
            sendAck(fullFrame.getTimestamp_uint32(), fullFrame.getISeqno_uint8());
          } else {
            logger.debug("[{}] - received [{}] late with OSeqno [{}] and timestamp [{}] when expected OSeqno [{}] and timestamp greater [{}] - dropped", this, fullFrame,
                Byte.toUnsignedInt(fullFrame.getOSeqno_uint8()), Integer.toUnsignedLong(fullFrame.getTimestamp_uint32()), Byte.toUnsignedInt(iSeqno_uint8),
                Integer.toUnsignedLong(lastProcessedFullFrameTimestamp_uint32));
          }
        }
      } else {
        if (isTimestampMustBeUpdatedAccordingToFullFrame(fullFrame)) {
          lastProcessedFullFrameTimestamp_uint32 = fullFrame.getTimestamp_uint32();
        }

        if (isSeqnoMustBeChangeAccordingToFullFrame(fullFrame)) {
          iSeqno_uint8++;
        }

        updateReliableFullFramesWithProcessingFullFrameISeqno(fullFrame.getISeqno_uint8());

        if (!processFullFrameInternal(fullFrame)) {
          logger.warn("[{}] - received [{}] unprocessed in session state [{}] - dropped", this, fullFrame, getSessionState());
        }
      }
    }
  }

  public void processMetaTrunkFrameMediaItem(final MetaTrunkMiniFrame.MediaItem mediaItem) {
    if (!processMetaTrunkFrameMediaItemInternal(mediaItem)) {
      logger.warn("[{}] - received [{}] unprocessed in session state [{}] - dropped", this, mediaItem, getSessionState());
    }
  }

  public void processMiniFrame(final MiniFrame miniFrame) {
    if (!processMiniFrameInternal(miniFrame)) {
      logger.warn("[{}] - received [{}] unprocessed in session state [{}] - dropped", this, miniFrame, getSessionState());
    }
  }

  public void processRoutine(final long currentTimeMillis) {
    int timestamp_uint32 = timestamp_uint32(currentTimeMillis);

    // Process outgoing frames
    Iterator<ReliableFullFrame> i = reliableFullFrames.iterator();
    while (i.hasNext()) {
      ReliableFullFrame reliableFullFrame = i.next();
      if (reliableFullFrame.isTimeForRetransmit(timestamp_uint32)) {
        if (reliableFullFrame.getRetransmitCount() > 0) {
          sendFullFrame(reliableFullFrame.getFullFrame());
          reliableFullFrame.transmitted();
        } else {
          i.remove();

          logger.warn("[{}] - deliver [{}] timeout", this, reliableFullFrame.getFullFrame());
          InternalErrorControlEvent internalErrorControlEvent = new InternalErrorControlEvent(getSessionId(), "Deliver timeout");
          offerControlEvent(internalErrorControlEvent);

          setSessionState(SessionState.DEINITIALIZED);
        }
      }
    }

    processRoutineInternal(timestamp_uint32);
  }



  protected abstract boolean processControlEventInternal(final ControlEvent controlEvent);

  protected abstract boolean processMediaEventInternal(final MediaEvent mediaEvent);

  protected abstract boolean processFullFrameInternal(FullFrame fullFrame);

  protected abstract boolean processMetaTrunkFrameMediaItemInternal(final MetaTrunkMiniFrame.MediaItem mediaItem);

  protected abstract boolean processMiniFrameInternal(MiniFrame miniFrame);

  protected abstract void processRoutineInternal(final int timestamp_uint32);



  private void updateReliableFullFramesWithProcessingFullFrameISeqno(final byte iSeqno_uint8) {
    ListIterator<ReliableFullFrame> i = reliableFullFrames.listIterator();
    while (i.hasNext()) {
      FullFrame reliableFullFrame = i.next().getFullFrame();

      if (reliableFullFrame.getOSeqno_uint8() == (byte) (iSeqno_uint8 - 1)) {
        i.remove();

        while (i.hasPrevious()) {
          i.previous();

          i.remove();
        }

        break;
      }
    }
  }

  private void retransmitReliableFullFramesWithVnakISeqno(final byte iSeqno_uint8) {
    int retransmitCounter = 0;

    ListIterator<ReliableFullFrame> i = reliableFullFrames.listIterator();
    while (i.hasNext()) {
      FullFrame reliableFullFrame = i.next().getFullFrame();

      if (reliableFullFrame.getOSeqno_uint8() == iSeqno_uint8) {
        retransmitCounter++;
        sendFullFrame(reliableFullFrame);

        while (i.hasNext()) {
          reliableFullFrame = i.next().getFullFrame();

          retransmitCounter++;
          sendFullFrame(reliableFullFrame);
        }

        break;
      }
    }

    logger.info("[{}] - retransmitted [{}] frames on VNAK with ISeqno [{}]", this, retransmitCounter, Byte.toUnsignedInt(iSeqno_uint8));
  }


  protected void reliableSendFullFrame(final FullFrame fullFrame) {
    if (isSeqnoMustBeChangeAccordingToFullFrame(fullFrame)) {
      oSeqno_uint8++;
    }

    sendFullFrame(fullFrame);

    reliableFullFrames.add(new ReliableFullFrame(fullFrame, currentTimestamp_uint32()));
  }

  protected void sendFullFrame(final FullFrame fullFrame) {
    iaxEngine.sendFrame(fullFrame, getTarget());
  }

  protected boolean isTrunkOutLinkExist() {
    return iaxEngine.isTrunkOutLinkExist(getTarget());
  }

  protected void flushTrunkOutLink() {
    iaxEngine.flushTrunkOutLink(getTarget());
  }

  protected void addMetaTrunkFrameMediaItemToTrunkOutLink(final MetaTrunkMiniFrame.MediaItem mediaItem) {
    iaxEngine.addMetaTrunkFrameMediaItemToTrunkOutLink(mediaItem, getTarget());
  }

  protected void sendMiniFrame(final MiniFrame miniFrame) {
    iaxEngine.sendFrame(miniFrame, getTarget());
  }



  protected void offerControlEvent(final ControlEvent controlEvent) {
    iaxEngine.offerControlEvent(controlEvent);
  }

  protected void offerMediaEvent(final MediaEvent mediaEvent) {
    iaxEngine.offerMediaEvent(mediaEvent);
  }


  public boolean isFinished() {
    return getSessionState() == SessionState.DEINITIALIZED;
  }

  public SessionState getSessionState() {
    return sessionState;
  }

  protected void setSessionState(final SessionState sessionState) {
    logger.trace("[{}] - session state changed from [{}] to [{}]", this, this.sessionState, sessionState);
    this.sessionState = sessionState;
  }

  public SocketAddress getTarget() {
    return target;
  }

  public SessionId getSessionId() {
    return sessionId;
  }

  public short getCallNumber_uint16() {
    return callNumber_uint16;
  }

  public short getDestinationCallNumber_uint16() {
    return destinationCallNumber_uint16;
  }

  protected void setDestinationCallNumber_uint16(final short destinationCallNumber_uint16) {
    this.destinationCallNumber_uint16 = destinationCallNumber_uint16;
  }

  protected int timestamp_uint32(final long currentTimeMillis) {
    long timestamp = currentTimeMillis - creationTimeMillis;
    if (timestamp < 0xFFFFFFFFL) {
      return (int) (timestamp);
    } else {
      throw new AssertionError();
    }
  }

  protected int currentTimestamp_uint32() {
    return timestamp_uint32(System.currentTimeMillis());
  }

  protected int currentSessionTimestamp_uint32() {
    int timestamp_uint32 = currentTimestamp_uint32();
    if (Integer.compareUnsigned(timestamp_uint32, lastTimestamp_uint32) <= 0) {
      timestamp_uint32 = lastTimestamp_uint32 + 1;
    }
    lastTimestamp_uint32 = timestamp_uint32;
    return timestamp_uint32;
  }

  protected void reliableSendVoiceFullFrame(final long audioMediaFormat_uint64, final int timestamp, final byte[] data) {
    FullFrame fullFrame = new FullFrame();
    fullFrame.setSourceCallNumber_uint16(getCallNumber_uint16());
    fullFrame.setDestinationCallNumber_uint16(getDestinationCallNumber_uint16());
    fullFrame.setTimestamp_uint32(timestamp);
    fullFrame.setOSeqno_uint8(oSeqno_uint8);
    fullFrame.setISeqno_uint8(iSeqno_uint8);
    fullFrame.setFrameType_uint8(FullFrameType.VOICE);
    fullFrame.setUncompressedSubclass_uint64(audioMediaFormat_uint64);
    fullFrame.setData(data);

    reliableSendFullFrame(fullFrame);
  }

  protected int reliableSendIaxFullFrame(final byte subclass, final IeMap ieMap) {
    int timestamp_uint32 = currentSessionTimestamp_uint32();

    FullFrame fullFrame = new FullFrame();
    fullFrame.setSourceCallNumber_uint16(getCallNumber_uint16());
    fullFrame.setDestinationCallNumber_uint16(getDestinationCallNumber_uint16());
    fullFrame.setTimestamp_uint32(timestamp_uint32);
    fullFrame.setOSeqno_uint8(oSeqno_uint8);
    fullFrame.setISeqno_uint8(iSeqno_uint8);
    fullFrame.setFrameType_uint8(FullFrameType.IAX);
    fullFrame.setSubclass_uint8(subclass);
    fullFrame.setData(IeUtils.encode(ieMap));

    reliableSendFullFrame(fullFrame);

    return timestamp_uint32;
  }

  protected int reliableSendControlFullFrame(final byte subclass) {
    int timestamp_uint32 = currentSessionTimestamp_uint32();

    FullFrame fullFrame = new FullFrame();
    fullFrame.setSourceCallNumber_uint16(getCallNumber_uint16());
    fullFrame.setDestinationCallNumber_uint16(getDestinationCallNumber_uint16());
    fullFrame.setTimestamp_uint32(timestamp_uint32);
    fullFrame.setOSeqno_uint8(oSeqno_uint8);
    fullFrame.setISeqno_uint8(iSeqno_uint8);
    fullFrame.setFrameType_uint8(FullFrameType.CONTROL);
    fullFrame.setSubclass_uint8(subclass);

    reliableSendFullFrame(fullFrame);

    return timestamp_uint32;
  }

  protected int reliableSendDtmfFullFrame(final char dtmf) {
    int timestamp_uint32 = currentSessionTimestamp_uint32();

    FullFrame ackFullFrame = new FullFrame();
    ackFullFrame.setSourceCallNumber_uint16(getCallNumber_uint16());
    ackFullFrame.setDestinationCallNumber_uint16(getDestinationCallNumber_uint16());
    ackFullFrame.setTimestamp_uint32(timestamp_uint32);
    ackFullFrame.setOSeqno_uint8(oSeqno_uint8);
    ackFullFrame.setISeqno_uint8(iSeqno_uint8);
    ackFullFrame.setFrameType_uint8(FullFrameType.DTMF);
    ackFullFrame.setSubclass_uint8((byte) dtmf);

    reliableSendFullFrame(ackFullFrame);

    return timestamp_uint32;
  }

  protected void reliableSendPing() {
    FullFrame ackFullFrame = new FullFrame();
    ackFullFrame.setSourceCallNumber_uint16(getCallNumber_uint16());
    ackFullFrame.setDestinationCallNumber_uint16(getDestinationCallNumber_uint16());
    ackFullFrame.setTimestamp_uint32(currentSessionTimestamp_uint32());
    ackFullFrame.setOSeqno_uint8(oSeqno_uint8);
    ackFullFrame.setISeqno_uint8(iSeqno_uint8);
    ackFullFrame.setFrameType_uint8(FullFrameType.IAX);
    ackFullFrame.setSubclass_uint8(IaxFullFrameSubclass.PING);

    reliableSendFullFrame(ackFullFrame);
  }

  protected void reliableSendPong(final int timestamp_uint32) {
    FullFrame ackFullFrame = new FullFrame();
    ackFullFrame.setSourceCallNumber_uint16(getCallNumber_uint16());
    ackFullFrame.setDestinationCallNumber_uint16(getDestinationCallNumber_uint16());
    ackFullFrame.setTimestamp_uint32(timestamp_uint32);
    ackFullFrame.setOSeqno_uint8(oSeqno_uint8);
    ackFullFrame.setISeqno_uint8(iSeqno_uint8);
    ackFullFrame.setFrameType_uint8(FullFrameType.IAX);
    ackFullFrame.setSubclass_uint8(IaxFullFrameSubclass.PONG);

    reliableSendFullFrame(ackFullFrame);
  }

  protected void reliableSendLagRp(final int timestamp_uint32) {
    FullFrame ackFullFrame = new FullFrame();
    ackFullFrame.setSourceCallNumber_uint16(getCallNumber_uint16());
    ackFullFrame.setDestinationCallNumber_uint16(getDestinationCallNumber_uint16());
    ackFullFrame.setTimestamp_uint32(timestamp_uint32);
    ackFullFrame.setOSeqno_uint8(oSeqno_uint8);
    ackFullFrame.setISeqno_uint8(iSeqno_uint8);
    ackFullFrame.setFrameType_uint8(FullFrameType.IAX);
    ackFullFrame.setSubclass_uint8(IaxFullFrameSubclass.LAGRP);

    reliableSendFullFrame(ackFullFrame);
  }

  protected void reliableSendUnsupport(final byte subclass_uint8) {
    IeMap unsupportedFullFrameIeMap = new IeMap();
    unsupportedFullFrameIeMap.add(new IaxUnknownIe(subclass_uint8));

    FullFrame unsupportedFullFrame = new FullFrame();
    unsupportedFullFrame.setSourceCallNumber_uint16(getCallNumber_uint16());
    unsupportedFullFrame.setDestinationCallNumber_uint16(getDestinationCallNumber_uint16());
    unsupportedFullFrame.setTimestamp_uint32(currentSessionTimestamp_uint32());
    unsupportedFullFrame.setOSeqno_uint8(oSeqno_uint8);
    unsupportedFullFrame.setISeqno_uint8(iSeqno_uint8);
    unsupportedFullFrame.setFrameType_uint8(FullFrameType.IAX);
    unsupportedFullFrame.setSubclass_uint8(IaxFullFrameSubclass.UNSUPPORT);
    unsupportedFullFrame.setData(IeUtils.encode(unsupportedFullFrameIeMap));

    reliableSendFullFrame(unsupportedFullFrame);
  }

  protected void sendAck(final int timestamp_uint32, final byte oSeqno_uint8) {
    FullFrame ackFullFrame = new FullFrame();
    ackFullFrame.setSourceCallNumber_uint16(getCallNumber_uint16());
    ackFullFrame.setDestinationCallNumber_uint16(getDestinationCallNumber_uint16());
    ackFullFrame.setTimestamp_uint32(timestamp_uint32);
    ackFullFrame.setOSeqno_uint8(oSeqno_uint8);
    ackFullFrame.setISeqno_uint8(iSeqno_uint8);
    ackFullFrame.setFrameType_uint8(FullFrameType.IAX);
    ackFullFrame.setSubclass_uint8(IaxFullFrameSubclass.ACK);

    sendFullFrame(ackFullFrame);
  }

  protected boolean isAckFullFrame(final FullFrame fullFrame) {
    return fullFrame.getFrameType_uint8() == FullFrameType.IAX && fullFrame.getSubclass_uint8() == IaxFullFrameSubclass.ACK;
  }

  protected void sendVnak() {
    FullFrame vnakFullFrame = new FullFrame();
    vnakFullFrame.setSourceCallNumber_uint16(getCallNumber_uint16());
    vnakFullFrame.setDestinationCallNumber_uint16(getDestinationCallNumber_uint16());
    vnakFullFrame.setTimestamp_uint32(currentSessionTimestamp_uint32());
    vnakFullFrame.setOSeqno_uint8(oSeqno_uint8);
    vnakFullFrame.setISeqno_uint8(iSeqno_uint8);
    vnakFullFrame.setFrameType_uint8(FullFrameType.IAX);
    vnakFullFrame.setSubclass_uint8(IaxFullFrameSubclass.VNAK);

    sendFullFrame(vnakFullFrame);
  }

  protected void addMetaTrunkFrameMediaItemToTrunkOutLink(final short timestamp_uint16, final byte[] data) {
    MetaTrunkMiniFrame.MediaItem mi = new MetaTrunkMiniFrame.MediaItem();
    mi.setSourceCallNumber_uint16(getCallNumber_uint16());
    mi.setTimestamp_uint16(timestamp_uint16);
    mi.setData(data);

    addMetaTrunkFrameMediaItemToTrunkOutLink(mi);
  }

  protected void sendMiniFrame(final short timestamp_uint16, final byte[] data) {
    MiniFrame mf = new MiniFrame();
    mf.setSourceCallNumber_uint16(getCallNumber_uint16());
    mf.setTimestamp_uint16(timestamp_uint16);
    mf.setData(data);

    sendMiniFrame(mf);
  }

  private boolean isTimestampMustBeUpdatedAccordingToFullFrame(final FullFrame fullFrame) {
    if (fullFrame.getFrameType_uint8() == FullFrameType.IAX) {
      byte iaxSubclass_uint8 = fullFrame.getSubclass_uint8();
      if (iaxSubclass_uint8 != IaxFullFrameSubclass.ACK &&
          iaxSubclass_uint8 != IaxFullFrameSubclass.PONG &&
          iaxSubclass_uint8 != IaxFullFrameSubclass.LAGRP) {
        return true;
      } else {
        return false;
      }
    } else {
      return true;
    }
  }

  private boolean isSeqnoMustBeChangeAccordingToFullFrame(final FullFrame fullFrame) {
    if (fullFrame.getFrameType_uint8() == FullFrameType.IAX) {
      byte iaxSubclass_uint8 = fullFrame.getSubclass_uint8();
      if (iaxSubclass_uint8 != IaxFullFrameSubclass.ACK &&
          iaxSubclass_uint8 != IaxFullFrameSubclass.INVAL &&
          iaxSubclass_uint8 != IaxFullFrameSubclass.VNAK &&
          iaxSubclass_uint8 != IaxFullFrameSubclass.TXCNT &&
          iaxSubclass_uint8 != IaxFullFrameSubclass.TXACC) {
        return true;
      } else {
        return false;
      }
    } else {
      return true;
    }
  }

  private boolean isAckMustBeSentAccordingToFullFrame(final FullFrame fullFrame) {
    if (fullFrame.getFrameType_uint8() == FullFrameType.IAX) {
      byte iaxSubclass_uint8 = fullFrame.getSubclass_uint8();
      if (iaxSubclass_uint8 != IaxFullFrameSubclass.ACK &&
          iaxSubclass_uint8 != IaxFullFrameSubclass.INVAL &&
          iaxSubclass_uint8 != IaxFullFrameSubclass.VNAK &&
          iaxSubclass_uint8 != IaxFullFrameSubclass.TXCNT &&
          iaxSubclass_uint8 != IaxFullFrameSubclass.TXACC &&
          iaxSubclass_uint8 != IaxFullFrameSubclass.PING &&
          iaxSubclass_uint8 != IaxFullFrameSubclass.LAGRQ) {
        return true;
      } else {
        return false;
      }
    } else {
      return true;
    }
  }

  @Override
  public String toString() {
    return getClass().getSimpleName() + "@" + Integer.toHexString(hashCode());
  }

  private static final class ReliableFullFrame {

    private final FullFrame fullFrame;
    private int retransmitCount;
    private int retransmitInterval;
    private int nextRetransmitTime_uint32;

    public ReliableFullFrame(final FullFrame fullFrame, final int timestamp_uint32) {
      this.fullFrame = fullFrame;
      fullFrame.setRetransmitted(true);

      retransmitCount = 5;
      retransmitInterval = 500;
      nextRetransmitTime_uint32 = timestamp_uint32 + retransmitInterval;
    }

    public FullFrame getFullFrame() {
      return fullFrame;
    }

    public void transmitted() {
      retransmitCount--;
      retransmitInterval *= 2;
      nextRetransmitTime_uint32 += retransmitInterval;
    }

    public int getRetransmitCount() {
      return retransmitCount;
    }

    public boolean isTimeForRetransmit(final int timestamp_uint32) {
      return Integer.compareUnsigned(nextRetransmitTime_uint32, timestamp_uint32) < 0;
    }

  }

  protected int calculateOverallTimeout_uint32() {
    int r = 0;
    for (int i = 0; i < getRetransmitCount(); i++) {
      r += getRetransmitIntervalForRetransmitAttempt(i);
    }
    return r;
  }

  protected int getRetransmitCount() {
    return 5;
  }

  protected int getRetransmitInterval() {
    return 500;
  }

  protected int getRetransmitIntervalForRetransmitAttempt(final int attempt) {
    return getRetransmitInterval() * (1 << attempt);
  }


}
