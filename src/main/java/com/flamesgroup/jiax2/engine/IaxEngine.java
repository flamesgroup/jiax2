/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.flamesgroup.jiax2.engine;

import com.flamesgroup.jiax2.TrunkOptions;
import com.flamesgroup.jiax2.event.ControlEvent;
import com.flamesgroup.jiax2.event.InternalErrorControlEvent;
import com.flamesgroup.jiax2.event.MediaEvent;
import com.flamesgroup.jiax2.event.NewControlEvent;
import com.flamesgroup.jiax2.frame.Frame;
import com.flamesgroup.jiax2.frame.full.FullFrame;
import com.flamesgroup.jiax2.frame.full.FullFrameType;
import com.flamesgroup.jiax2.frame.full.IaxFullFrameSubclass;
import com.flamesgroup.jiax2.frame.full.IeUtils;
import com.flamesgroup.jiax2.frame.meta.MetaTrunkMiniFrame;
import com.flamesgroup.jiax2.frame.meta.MetaTrunkSuperMiniFrame;
import com.flamesgroup.jiax2.frame.meta.MetaVideoFrame;
import com.flamesgroup.jiax2.frame.mini.MiniFrame;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.net.SocketAddress;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.ClosedSelectorException;
import java.nio.channels.DatagramChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

public class IaxEngine {

  final static Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

  public static final short IAX2_POKE_PONG_CALL_NUMBER = 1;        // Local call number to set when sending call token message
  public static final short IAX2_CALLTOKEN_CALL_NUMBER = 1;        // Local call number to set when sending call token message
  public static final short IAX2_CALLTOKEN_REJ_CALL_NUMBER = 1;    // Local call number to set when rejecting calls with missing call token

  private static final SessionId zeroSessionId = SessionId.generateSessionId();
  private static final ControlEvent shutdownControlEvent = new ControlEvent(zeroSessionId) {
  };
  private static final MediaEvent shutdownMediaEvent = new MediaEvent(zeroSessionId) {
  };

  private final List<TrunkOutLink> trunkOutLinks = new ArrayList<>();

  private final Selector iaxSelector;
  private final DatagramChannel iaxDatagramChannel;
  private final SelectionKey iaxDatagramChannelSelectionKey;

  private final List<SendFrame> sendFrames = new LinkedList<>();

  private Thread engineThread;

  private final BlockingQueue<ControlEvent> outControlEvents = new LinkedBlockingDeque<>();
  private final BlockingQueue<MediaEvent> outMediaEvents = new LinkedBlockingDeque<>();
  private final BlockingQueue<ControlEvent> inControlEvents = new LinkedBlockingDeque<>();
  private final BlockingQueue<MediaEvent> inMediaEvents = new LinkedBlockingDeque<>();

  private final CallNumberGenerator callNumberGenerator = new CallNumberGenerator();

  private final List<Session> sessions = new LinkedList<>();

  public IaxEngine(final Map<SocketAddress, TrunkOptions> trunkOptionsMap) throws IOException {
    Objects.requireNonNull(trunkOptionsMap, "trunkOptionsMap mustn't be null");

    for (Map.Entry<SocketAddress, TrunkOptions> trunkOptionsEntry : trunkOptionsMap.entrySet()) {
      trunkOutLinks.add(new TrunkOutLink(trunkOptionsEntry.getValue(), trunkOptionsEntry.getKey()));
    }

    iaxSelector = Selector.open();

    iaxDatagramChannel = DatagramChannel.open();
    iaxDatagramChannel.configureBlocking(false);

    iaxDatagramChannelSelectionKey = iaxDatagramChannel.register(iaxSelector, SelectionKey.OP_READ);
  }

  public IaxEngine() throws IOException {
    this(Collections.emptyMap());
  }

  @Override
  protected void finalize() throws Throwable {
    super.finalize();
    if (iaxDatagramChannel.isOpen()) {
      iaxDatagramChannel.close();
    }
    if (iaxSelector.isOpen()) {
      iaxSelector.close();
    }
  }

  public void startup(final SocketAddress bindSocketAddress) throws IOException, InterruptedException {
    try {
      iaxDatagramChannel.bind(bindSocketAddress);
    } catch (IOException e) {
      outControlEvents.put(shutdownControlEvent);
      outMediaEvents.put(shutdownMediaEvent);
      throw e;
    }

    engineThread = new Thread(new Worker(), "IaxEngine(" + bindSocketAddress + ")");
    engineThread.setDaemon(true);
    engineThread.start();
  }

  public void shutdown() throws IOException, InterruptedException {
    iaxDatagramChannel.close();

    iaxSelector.close();

    engineThread.join();
    engineThread = null;

    outControlEvents.put(shutdownControlEvent);
    outMediaEvents.put(shutdownMediaEvent);
  }

  public ControlEvent takeControlEvent() throws ClosedIaxEngineException, InterruptedException {
    ControlEvent controlEvent = outControlEvents.take();
    if (controlEvent != shutdownControlEvent) {
      return controlEvent;
    } else {
      outControlEvents.put(controlEvent);
      throw new ClosedIaxEngineException();
    }
  }

  public MediaEvent takeMediaEvent() throws ClosedIaxEngineException, InterruptedException {
    MediaEvent mediaEvent = outMediaEvents.take();
    if (mediaEvent != shutdownMediaEvent) {
      return mediaEvent;
    } else {
      outMediaEvents.put(mediaEvent);
      throw new ClosedIaxEngineException();
    }
  }

  public void putControlEvent(final ControlEvent controlEvent) throws InterruptedException {
    inControlEvents.put(controlEvent);
    iaxSelector.wakeup();
  }

  public void putMediaEvent(final MediaEvent mediaEvent) throws InterruptedException {
    inMediaEvents.put(mediaEvent);
    iaxSelector.wakeup();
  }

  protected void offerControlEvent(final ControlEvent controlEvent) {
    if (!outControlEvents.offer(controlEvent)) {
      logger.warn("[{}] - offered [{}] wasn't added to control events queue - dropped", this, controlEvent);
    }
  }

  protected void offerMediaEvent(final MediaEvent mediaEvent) {
    if (!outMediaEvents.offer(mediaEvent)) {
      logger.warn("[{}] - offered [{}] wasn't added to media events queue - dropped", this, mediaEvent);
    }
  }

  protected ControlEvent pollControlEvent() {
    return inControlEvents.poll();
  }

  protected MediaEvent pollMediaEvent() {
    return inMediaEvents.poll();
  }

  private Session findSessionBySessionId(final SessionId sessionId) {
    for (final Session session : sessions) {
      if (session.getSessionId().equals(sessionId)) {
        return session;
      }
    }
    return null;
  }

  private Session findSessionBySourceCallNumber(final SocketAddress source, final short sourceCallNumber_uint16) {
    for (final Session session : sessions) {
      if (session.getDestinationCallNumber_uint16() == sourceCallNumber_uint16 && session.getTarget().equals(source)) {
        return session;
      }
    }
    return null;
  }

  private Session findSessionByDestinationCallNumber(final SocketAddress source, final short destinationCallNumber_uint16) {
    for (final Session session : sessions) {
      if (session.getCallNumber_uint16() == destinationCallNumber_uint16 && session.getTarget().equals(source)) {
        return session;
      }
    }
    return null;
  }

  private TrunkOutLink findTrunkOutLink(final SocketAddress target) {
    for (TrunkOutLink trunkOutLink : trunkOutLinks) {
      if (trunkOutLink.getTarget().equals(target)) {
        return trunkOutLink;
      }
    }
    return null;
  }

  private void processTrunksRoutine() {
    long currentTimeMillis = System.currentTimeMillis();
    for (final TrunkOutLink trunkOutLink : trunkOutLinks) {
      trunkOutLink.sendOnTime(currentTimeMillis);
    }
  }

  private void processSessionsRoutine() {
    long currentTimeMillis = System.currentTimeMillis();
    for (final Session session : sessions) {
      session.processRoutine(currentTimeMillis);
    }
  }

  private void removeFinishedSessions() {
    Iterator<Session> i = sessions.iterator();
    while (i.hasNext()) {
      Session session = i.next();
      if (session.isFinished()) {
        session.free();
        i.remove();
      }
    }
  }

  private void processControlEvents() {
    ControlEvent controlEvent;
    while ((controlEvent = pollControlEvent()) != null) {
      processControlEvent(controlEvent);
    }
  }

  private void processMediaEvents() {
    MediaEvent mediaEvent;
    while ((mediaEvent = pollMediaEvent()) != null) {
      processMediaEvent(mediaEvent);
    }
  }


  private void processControlEvent(final ControlEvent controlEvent) {
    // First check for initial control event and if not then sent it to session
    if (controlEvent instanceof NewControlEvent) {
      NewControlEvent newControlEvent = (NewControlEvent) controlEvent;

      Session session = new OutgoingCallSession(this, newControlEvent.getAddress(), newControlEvent.getSessionId());
      sessions.add(session);

      session.processControlEvent(controlEvent);
    } else {
      Session session = findSessionBySessionId(controlEvent.getSessionId());
      if (session != null) {
        session.processControlEvent(controlEvent);
      } else {
        InternalErrorControlEvent internalErrorControlEvent = new InternalErrorControlEvent(controlEvent.getSessionId(), "Session is not exist");
        offerControlEvent(internalErrorControlEvent);
      }
    }
  }

  private void processMediaEvent(final MediaEvent mediaEvent) {
    Session session = findSessionBySessionId(mediaEvent.getSessionId());
    if (session != null) {
      session.processMediaEvent(mediaEvent);
    } else {
      logger.warn("[{}] - Session with id [{}] is not exist", this, mediaEvent.getSessionId());
    }
  }

  private void processFullFrame(final SocketAddress source, final FullFrame fullFrame) {

    // this POKE processing logic grab from asterisk
    {
      // Deal with POKE/PONG without allocating a callno
      if (fullFrame.getDestinationCallNumber_uint16() == 0 &&
          fullFrame.getFrameType_uint8() == FullFrameType.IAX &&
          fullFrame.getSubclass_uint8() == IaxFullFrameSubclass.POKE) {
        // Reply back with a PONG, but don't care about the result.
        sendPong(fullFrame, source);
        return;
      }
      if (fullFrame.getDestinationCallNumber_uint16() == IAX2_POKE_PONG_CALL_NUMBER &&
          fullFrame.getFrameType_uint8() == FullFrameType.IAX &&
          fullFrame.getSubclass_uint8() == IaxFullFrameSubclass.ACK) {
        // Ignore
        return;
      }
    }


    if (fullFrame.getDestinationCallNumber_uint16() == 0) {
      if (fullFrame.getFrameType_uint8() != FullFrameType.IAX) {
        sendInval(fullFrame, source);
      } else {
        // TODO implement CallToken check

        Session session = findSessionBySourceCallNumber(source, fullFrame.getSourceCallNumber_uint16());

        if (session != null) {
          session.processFullFrame(fullFrame);
        } else {
          switch (fullFrame.getSubclass_uint8()) {
            case IaxFullFrameSubclass.NEW:
              session = new IncomingCallSession(this, source, SessionId.generateSessionId());
              break;
            case IaxFullFrameSubclass.REGREQ:
              // TODO Registration Request session
              session = null;
              break;
            case IaxFullFrameSubclass.REGREL:
              // TODO Registration Release session
              session = null;
              break;
            default:
              session = null;
              break;
          }

          if (session != null) {
            sessions.add(session);
            session.processFullFrame(fullFrame);
          } else {
            logger.info("[{}] - unsupported start incoming session by [{}] with source call number [{}]", this, fullFrame, Short.toUnsignedInt(fullFrame.getSourceCallNumber_uint16()));
            sendInval(fullFrame, source);
          }
        }
      }
    } else {
      Session session = findSessionByDestinationCallNumber(source, fullFrame.getDestinationCallNumber_uint16());
      if (session != null) {
        session.processFullFrame(fullFrame);
      } else {
        logger.info("[{}] - unmatched [{}] with source call number [{}] and destination call number [{}] from [{}]", this, fullFrame, Short.toUnsignedInt(fullFrame.getSourceCallNumber_uint16()),
            Short.toUnsignedInt(fullFrame.getDestinationCallNumber_uint16()), source);
        sendInval(fullFrame, source);
      }
    }
  }

  private void processMetaVideoFrame(final SocketAddress source, final MetaVideoFrame metaVideoFrame) {
    logger.warn("Processing of Meta Video Frame unsupported for now"); // TODO
  }

  private void processMetaTrunkSuperMiniFrame(final SocketAddress source, final MetaTrunkSuperMiniFrame metaTrunkSuperMiniFrame) {
    logger.warn("Processing of Meta Trunk Super Mini Frame unsupported for now"); // TODO
  }

  private void processMetaTrunkMiniFrame(final SocketAddress source, final MetaTrunkMiniFrame metaTrunkMiniFrame) {
    for (MetaTrunkMiniFrame.MediaItem mediaItem : metaTrunkMiniFrame.getMediaItems()) {
      Session session = findSessionBySourceCallNumber(source, mediaItem.getSourceCallNumber_uint16());
      if (session != null) {
        session.processMetaTrunkFrameMediaItem(mediaItem);
      } else {
        logger.info("[{}] - unmatched [{}] with source call number [{}] from [{}]", this, mediaItem, Short.toUnsignedInt(mediaItem.getSourceCallNumber_uint16()), source);
      }
    }
  }

  private void processMiniFrame(final SocketAddress source, final MiniFrame miniFrame) {
    Session session = findSessionBySourceCallNumber(source, miniFrame.getSourceCallNumber_uint16());
    if (session != null) {
      session.processMiniFrame(miniFrame);
    } else {
      logger.info("[{}] - unmatched [{}] with source call number [{}] from [{}]", this, miniFrame, Short.toUnsignedInt(miniFrame.getSourceCallNumber_uint16()), source);
    }
  }

  private void processFrameData(final SocketAddress source, final ByteBuffer bb) {
    if (bb.remaining() < 4) {
      logger.warn("[{}] - data size [{}] too short for Frame", this, bb.remaining());
    } else {
      short uint16_0 = bb.getShort(0);
      if ((uint16_0 & 0x8000) != 0) { // Full Frame ?
        if (bb.remaining() < 12) {
          logger.warn("[{}] - data size [{}] too short for Full Frame", this, bb.remaining());
        } else {
          FullFrame fullFrame = new FullFrame();
          fullFrame.decodeFrom(bb);

          // check data for integrity depending on the Full Frame type
          switch (fullFrame.getFrameType_uint8()) {
            case FullFrameType.IAX: {
              try {
                IeUtils.decode(fullFrame.getData());
              } catch (BufferUnderflowException | IllegalArgumentException e) {
                logger.warn("[{}] - received [{}] with malformed data - dropped", this, fullFrame, e);
                return;
              }
              break;
            }
          }

          processFullFrame(source, fullFrame);
        }
      } else if (uint16_0 == 0) { // Meta Frame ?
        byte uint8_2 = bb.get(2);
        if ((uint8_2 & 0x80) != 0) { // Meta Video Frame
          if (bb.remaining() < 6) {
            logger.warn("[{}] - data size [{}] too short for Meta Video Frame", this, bb.remaining());
          } else {
            MetaVideoFrame metaVideoFrame = new MetaVideoFrame();
            metaVideoFrame.decodeFrom(bb);

            processMetaVideoFrame(source, metaVideoFrame);
          }
        } else {
          if (bb.remaining() < 8) {
            logger.warn("[{}] - data size [{}] too short for Meta Trunk Frame", this, bb.remaining());
          } else {
            if (uint8_2 != 1) {
              logger.warn("[{}] - unknown Meta Command [{}]", this, uint8_2);
            } else {
              byte uint8_3 = bb.get(3);
              if (uint8_3 == 0) {
                MetaTrunkSuperMiniFrame metaTrunkSuperMiniFrame = new MetaTrunkSuperMiniFrame();
                metaTrunkSuperMiniFrame.decodeFrom(bb);

                processMetaTrunkSuperMiniFrame(source, metaTrunkSuperMiniFrame);
              } else if (uint8_3 == 1) {
                MetaTrunkMiniFrame metaTrunkMiniFrame = new MetaTrunkMiniFrame();
                metaTrunkMiniFrame.decodeFrom(bb);

                processMetaTrunkMiniFrame(source, metaTrunkMiniFrame);
              } else {
                logger.warn("[{}] - unknown Meta Command Data [{}]", this, uint8_3);
              }
            }
          }
        }
      } else {
        MiniFrame miniFrame = new MiniFrame();
        miniFrame.decodeFrom(bb);

        processMiniFrame(source, miniFrame);
      }
    }
  }

  private void sendInval(final FullFrame unmatchedFullFrame, final SocketAddress target) {
    // Check for frames that should not receive INVAL
    if (unmatchedFullFrame.getFrameType_uint8() == FullFrameType.IAX && unmatchedFullFrame.getSubclass_uint8() == IaxFullFrameSubclass.INVAL) {
      return;
    }

    logger.debug("[{}] - send INVAL for unmatched [{}]", this, unmatchedFullFrame);

    FullFrame invalFullFrame = new FullFrame();
    invalFullFrame.setSourceCallNumber_uint16(unmatchedFullFrame.getDestinationCallNumber_uint16());
    invalFullFrame.setRetransmitted(false);
    invalFullFrame.setDestinationCallNumber_uint16(unmatchedFullFrame.getSourceCallNumber_uint16());
    invalFullFrame.setTimestamp_uint32(unmatchedFullFrame.getTimestamp_uint32());
    invalFullFrame.setOSeqno_uint8(unmatchedFullFrame.getISeqno_uint8());
    invalFullFrame.setISeqno_uint8(unmatchedFullFrame.getOSeqno_uint8());
    invalFullFrame.setFrameType_uint8(FullFrameType.IAX);
    invalFullFrame.setSubclass_uint8(IaxFullFrameSubclass.INVAL);

    sendFrame(invalFullFrame, target);
  }

  private void sendPong(final FullFrame pokeFullFrame, final SocketAddress target) {
    FullFrame pongFullFrame = new FullFrame();
    pongFullFrame.setSourceCallNumber_uint16(IAX2_POKE_PONG_CALL_NUMBER);
    pongFullFrame.setRetransmitted(false);
    pongFullFrame.setDestinationCallNumber_uint16(pokeFullFrame.getSourceCallNumber_uint16());
    pongFullFrame.setTimestamp_uint32(pokeFullFrame.getTimestamp_uint32());
    pongFullFrame.setOSeqno_uint8((byte) (pokeFullFrame.getISeqno_uint8() + 1));
    pongFullFrame.setISeqno_uint8(pokeFullFrame.getOSeqno_uint8());
    pongFullFrame.setFrameType_uint8(FullFrameType.IAX);
    pongFullFrame.setSubclass_uint8(IaxFullFrameSubclass.PONG);

    sendFrame(pongFullFrame, target);
  }

  public void sendFrame(final Frame frame, final SocketAddress target) {
    sendFrames.add(new SendFrame(frame, target));
    iaxDatagramChannelSelectionKey.interestOps(SelectionKey.OP_READ | SelectionKey.OP_WRITE);
  }

  public boolean isTrunkOutLinkExist(final SocketAddress target) {
    return findTrunkOutLink(target) != null;
  }

  public void flushTrunkOutLink(final SocketAddress target) {
    final TrunkOutLink trunkOutLink = findTrunkOutLink(target);
    if (trunkOutLink != null) {
      trunkOutLink.flush();
    } else {
      throw new AssertionError();
    }
  }

  public void addMetaTrunkFrameMediaItemToTrunkOutLink(final MetaTrunkMiniFrame.MediaItem mediaItem, final SocketAddress target) {
    final TrunkOutLink trunkOutLink = findTrunkOutLink(target);
    if (trunkOutLink != null) {
      trunkOutLink.add(mediaItem);
    } else {
      throw new AssertionError();
    }
  }

  public short generateCallNumber_uint16() {
    return callNumberGenerator.generateCallNumber_uint16();
  }

  public void releaseCallNumber_uint16(final short callNumber_uint16) {
    callNumberGenerator.releaseCallNumber_uint16(callNumber_uint16);
  }

  @Override
  public String toString() {
    return getClass().getSimpleName() + "@" + Integer.toHexString(hashCode());
  }

  private final class Worker implements Runnable {

    private static final int LIMIT_IO_EXCEPTIONS = 3;

    @Override
    public void run() {
      logger.debug("[{}] - begin executing", this);

      final ByteBuffer bb = ByteBuffer.allocateDirect(1500).order(ByteOrder.BIG_ENDIAN);

      int ioExceptions = 0;

      SendFrame sendFrame = null;
      while (true) {
        try {
          int numberOfKeys = iaxSelector.select(5);
          if (numberOfKeys == 0) {
            processControlEvents();
            processMediaEvents();
          } else {
            Set<SelectionKey> iaxSelectionKeys = iaxSelector.selectedKeys();
            Iterator<SelectionKey> iaxSelectionKeyIterator = iaxSelectionKeys.iterator();
            while (iaxSelectionKeyIterator.hasNext()) {
              SelectionKey selectionKey = iaxSelectionKeyIterator.next();
              if (selectionKey != iaxDatagramChannelSelectionKey) {
                throw new AssertionError("Currently only one DatagramChannel supported");
              }

              if (selectionKey.isReadable()) {
                bb.clear();
                SocketAddress source = ((DatagramChannel) selectionKey.channel()).receive(bb);
                bb.flip();

                processFrameData(source, bb);
              }
              if (selectionKey.isWritable()) {
                Iterator<SendFrame> i = sendFrames.iterator();
                while (i.hasNext()) {
                  sendFrame = i.next();

                  bb.clear();
                  sendFrame.getFrame().encodeTo(bb);
                  bb.flip();

                  if (((DatagramChannel) selectionKey.channel()).send(bb, sendFrame.getTarget()) == 0) {
                    logger.warn("[{}] - insufficient room for the datagram in the underlying output buffer - try next time", this);
                    break;
                  } else {
                    i.remove();
                  }
                }
                if (sendFrames.size() == 0) {
                  iaxDatagramChannelSelectionKey.interestOps(SelectionKey.OP_READ);
                }
              }

              iaxSelectionKeyIterator.remove();
            }
            if (ioExceptions > 0) {
              ioExceptions = 0;
            }
          }

          processTrunksRoutine();

          processSessionsRoutine();

          removeFinishedSessions();

        } catch (ClosedChannelException | ClosedSelectorException e) {
          logger.warn("[{}] - end executing", this, e);
          break;
        } catch (IOException e) {
          StringBuilder builder = new StringBuilder("unexpected IOException - ");
          if (sendFrame == null) {
            builder.append("end executing");
          } else {
            builder.append("[").append(System.lineSeparator());
            builder.append("target: [").append(sendFrame.getTarget()).append("]").append(System.lineSeparator());
            builder.append("frame: [").append(sendFrame.getFrame()).append("]").append(System.lineSeparator());
            builder.append("bb: [").append(bb).append("]").append("]");
          }

          logger.warn("[{}] - {}", this, builder, e);
          if (++ioExceptions >= LIMIT_IO_EXCEPTIONS) {
            logger.error("[{}] - limit of IO exceptions exceeded, so terminate engine", this);
            throw new IllegalStateException(e);
          }
        }
      }
    }

    @Override
    public String toString() {
      return getClass().getEnclosingClass().getSimpleName() + "$" + getClass().getSimpleName() + "@" + Integer.toHexString(hashCode());
    }

  }

  private static final class SendFrame {

    private final Frame frame;
    private final SocketAddress target;

    public SendFrame(final Frame frame, final SocketAddress target) {
      assert frame != null;
      assert target != null;

      this.frame = frame;
      this.target = target;
    }

    public Frame getFrame() {
      return frame;
    }

    public SocketAddress getTarget() {
      return target;
    }

  }

  private final class TrunkOutLink {

    private final long creationTimeMillis = System.currentTimeMillis();

    private final TrunkOptions trunkOptions;
    private final SocketAddress target;

    private MetaTrunkMiniFrame metaTrunkMiniFrame;
    private long sendMetaTrunkMiniFrameTimeMillis;

    public TrunkOutLink(final TrunkOptions trunkOptions, final SocketAddress target) {
      this.trunkOptions = trunkOptions;
      this.target = target;

      createMetaTrunkMiniFrame(creationTimeMillis);
    }

    public SocketAddress getTarget() {
      return target;
    }

    public void flush() {
      if (!isMetaTrunkMiniFrameEmpty()) {
        final long currentTimeMillis = System.currentTimeMillis();
        sendMetaTrunkMiniFrame(currentTimeMillis);
        createMetaTrunkMiniFrame(currentTimeMillis);
      }
    }

    public void add(final MetaTrunkMiniFrame.MediaItem mediaItem) {
      if (!isMetaTrunkMiniFrameEmpty() && getMetaTrunkMiniFrameLength() + mediaItem.getLength() > trunkOptions.getMaxFrameLength()) {
        final long currentTimeMillis = System.currentTimeMillis();
        sendMetaTrunkMiniFrame(currentTimeMillis);
        createMetaTrunkMiniFrame(currentTimeMillis);
      }
      addMetaTrunkMiniFrameLength(mediaItem);
    }

    public void sendOnTime(final long currentTimeMillis) {
      if (!isMetaTrunkMiniFrameEmpty() && currentTimeMillis > sendMetaTrunkMiniFrameTimeMillis) {
        sendMetaTrunkMiniFrame(currentTimeMillis);
        createMetaTrunkMiniFrame(currentTimeMillis);
      }
    }

    private boolean isMetaTrunkMiniFrameEmpty() {
      return metaTrunkMiniFrame == null || metaTrunkMiniFrame.getMediaItems().isEmpty();
    }

    private int getMetaTrunkMiniFrameLength() {
      return metaTrunkMiniFrame.getLength();
    }

    private void addMetaTrunkMiniFrameLength(final MetaTrunkMiniFrame.MediaItem mediaItem) {
      metaTrunkMiniFrame.getMediaItems().add(mediaItem);
    }

    private void sendMetaTrunkMiniFrame(final long currentTimeMillis) {
      metaTrunkMiniFrame.setTimestamp_uint32((int) (currentTimeMillis - creationTimeMillis));
      sendFrame(metaTrunkMiniFrame, target);
      metaTrunkMiniFrame = null;
    }

    private void createMetaTrunkMiniFrame(final long currentTimeMillis) {
      metaTrunkMiniFrame = new MetaTrunkMiniFrame();
      sendMetaTrunkMiniFrameTimeMillis = currentTimeMillis + trunkOptions.getSendIntervalMillis();
    }

  }

}
