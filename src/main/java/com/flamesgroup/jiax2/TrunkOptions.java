/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.flamesgroup.jiax2;

public final class TrunkOptions {

  public static final int MIN_SEND_INTERVAL_MILLIS = 5;
  public static final int MIN_MAX_FRAME_LENGTH = 20;

  /**
   * Interval, in milliseconds, to send trunked audio data.
   * The interval is measured from the first packet put in a trunk.
   * Trunked data is sent when this interval elapses or the buffer is full.
   */
  private final int sendIntervalMillis;
  /**
   * Maximum value for trunked data frames.
   * This value includes the length of trunk frame header (8 bytes).
   * Trunked data is sent when the send interval elapses or the buffer is full.
   */
  private final int maxFrameLength;

  public TrunkOptions(final int sendIntervalMillis, final int maxFrameLength) {
    if (sendIntervalMillis < MIN_SEND_INTERVAL_MILLIS) {
      throw new IllegalArgumentException("sendIntervalMillis mustn't be lover then " + MIN_SEND_INTERVAL_MILLIS);
    }
    if (maxFrameLength < MIN_MAX_FRAME_LENGTH) {
      throw new IllegalArgumentException("maxFrameLength mustn't be lover then " + MIN_MAX_FRAME_LENGTH);
    }

    this.sendIntervalMillis = sendIntervalMillis;
    this.maxFrameLength = maxFrameLength;
  }

  public TrunkOptions() {
    this(20, 1400);
  }

  public int getSendIntervalMillis() {
    return sendIntervalMillis;
  }

  public int getMaxFrameLength() {
    return maxFrameLength;
  }

}
